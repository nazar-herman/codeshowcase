﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    Camera camera;

	void Update ()
    {
        if(camera == null)
        {
            camera = Camera.main;
        }

        if (camera != null)
        {
            transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward,
                camera.transform.rotation * Vector3.up);
        }
    }
}

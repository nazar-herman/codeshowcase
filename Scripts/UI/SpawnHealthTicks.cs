﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnHealthTicks : MonoBehaviour
{
    UIResourceControl uiResCtrl;
    public RectTransform parentRect;
    public int numToSpawn;
    public float radius;
    public float yOffset = 0f;
    public GameObject healthImg;

    void Start()
    {
        uiResCtrl = GetComponentInParent<UIResourceControl>();
        float angle;
        float angleStep = 180f / (numToSpawn - 1);
        
        for (int i = 0; i < numToSpawn; i++)
        {

            angle = (i * angleStep) * Mathf.Deg2Rad;

            Vector2 pos = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;
            
            GameObject health = Instantiate(healthImg, parentRect.anchoredPosition + pos + (Vector2.up * yOffset), Quaternion.identity);
            health.transform.SetParent(parentRect, false);
            RectTransform rect = health.GetComponent<RectTransform>();

            Vector2 dirVec = rect.anchoredPosition - parentRect.anchoredPosition;
            float rotAngle = Vector2.Angle(parentRect.up, dirVec);
            if( (i+1) <= numToSpawn/2)
            {
                rotAngle = -rotAngle;
            }
            health.transform.Rotate(Vector3.forward, rotAngle);

            health.transform.localScale = Vector3.one;
        }

        uiResCtrl.SetupHealthTicks();
    }
}
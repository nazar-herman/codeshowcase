﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    public float inputDelay = 0.0f;
    public float startTime = 0.0f;

    public MainMenuManager mainMenuManager;
    public GameObject MainMenuCanvas;

    float inputTimer = 0.0f;
    float startTimer = 0.0f;
    bool input = false;


    void Update()
    {
        if (!input)
        {
            inputTimer += Time.deltaTime;
            if(inputTimer >= inputDelay)
            {
                input = true;
            }
        }
        else
        {
            if (Input.GetButtonDown(Buttons.A))
            {
                MainMenuCanvas.SetActive(true);
                mainMenuManager.SetFirstSelected();
                this.gameObject.SetActive(false);
            }
        }
	}
}

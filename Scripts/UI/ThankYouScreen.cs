﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ThankYouScreen : MonoBehaviour
{
    public float inputDelay = 0.0f;
    float inputTimer = 0.0f;
    bool input = false;


    void Update()
    {
        if (!input)
        {
            inputTimer += Time.deltaTime;
            if(inputTimer >= inputDelay)
            {
                input = true;
            }
        }
        else
        {
            if (Input.GetButtonDown(Buttons.A))
            {
                SceneManager.LoadScene("TitleScene");

                if (AudioManager.Instance != null)
                {
                    Destroy(AudioManager.Instance.gameObject);
                }
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnButtonHighlight : MonoBehaviour {

    [HideInInspector]
    public bool onEnter;
    [HideInInspector]
    public bool onExit;

    Image buttonImage;
    public Image panelImage;

    RectTransform rectTransform;
    Text text;
    Vector2 originalPos;
    public AudioClip highLightSFX;

    public void Awake()
    {
        text = GetComponentInChildren<Text>();
        if(text != null)
        {
            rectTransform = text.GetComponent<RectTransform>();
            originalPos = rectTransform.localPosition;
        }
        buttonImage = GetComponent<Image>();
    }

    void Update()
    {
        if (onEnter)
        {
            rectTransform.localPosition = Vector2.Lerp(rectTransform.localPosition, new Vector2(originalPos.x + 35, originalPos.y), 0.3f);
            buttonImage.color = new Color(buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, 1.0f);
        }

        if (onExit)
        {
            rectTransform.localPosition = Vector2.Lerp(rectTransform.localPosition, new Vector2(originalPos.x, originalPos.y), 0.3f);
            buttonImage.color = new Color(buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, 0.0f);
        }
    }

    public void Highlight()
    {
        panelImage.color = new Color(panelImage.color.r, panelImage.color.g, panelImage.color.b, 0.8f);
    }

    public void UnHighlight()
    {
        panelImage.color = new Color(panelImage.color.r, panelImage.color.g, panelImage.color.b, 0.0f);
    }
}

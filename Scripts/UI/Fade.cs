﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade: MonoBehaviour
{
    public enum FadeTarget
    {
        CanvasGroup,
        CanvasRenderer,
        Image,
        Renderer,
        Text,
    }

    [Tooltip("The Component you want to target.")]
    public FadeTarget fadeTarget;
    [Tooltip("The time it takes to fade in.")]
    public float fadeInTime;
    [Tooltip("The time it takes to fade out.")]
    public float fadeOutTime;
    [Range(0,1)]
    public float minValue;
    [Range(0,1)]
    public float maxValue;
    [Tooltip("How long to delay before fading again.")]
    public float fadeDelayTime;
    public float initialDelay;
    public bool pulse;
    [HideInInspector]
    public bool fadeIn;
    [HideInInspector]
    public bool fadeOut;

    public bool disableAfterFadeIn;


    protected bool startFadeDelay; //start the delay timer
    protected bool interrupt; //fadeIn has interrupted fadeOut
    protected bool active; //the target is fully opaque
    protected float fadeDelayTimer;
    protected float time; //the incremented time after each frame

    protected float initialDelayTimer;
    [HideInInspector]
    public bool start;
    [HideInInspector]
    public bool stop;

    void Start()
    {
        if(initialDelay > 0.0f || pulse)
        {
            fadeIn = true;
        }
    }

    bool InitialDelay()
    {
        initialDelayTimer += Time.deltaTime;
        if(initialDelayTimer >= initialDelay)
        {
            initialDelayTimer = 0.0f;
            start = true;
            return true;
        }
        return false;
    }

    void Update()
    {
        if (!stop)
        {
            if (!InitialDelay() && !start)
            {
                return;
            }

            //if the target should fade in and is not already active
            if (fadeIn && !active)
            {
                //choose the fade target and set the alpha value accordingly
                switch (fadeTarget)
                {
                    case FadeTarget.CanvasGroup:
                        CanvasGroup group = GetComponent<CanvasGroup>();
                        group.alpha = FadeIn(group.alpha);
                        break;
                    case FadeTarget.CanvasRenderer:
                        CanvasRenderer renderer = GetComponent<CanvasRenderer>();
                        renderer.SetAlpha(FadeIn(renderer.GetAlpha()));
                        break;
                }
            }

            if (stop)
            {
                return;
            }
            //reset delay if fadeIn is true and already faded in
            else if (fadeIn && active)
            {
                fadeIn = false;
                fadeDelayTimer = 0.0f;
            }

            //start the delay timer to fade out
            if (startFadeDelay)
            {
                fadeDelayTimer += Time.deltaTime;
                if (fadeDelayTimer >= fadeDelayTime)
                {
                    fadeDelayTimer = 0.0f;
                    startFadeDelay = false;
                    fadeOut = true;
                }
            }

            //if should fade out
            if (fadeOut)
            {
                //select fade target and set alpha value accordingly
                switch (fadeTarget)
                {
                    case FadeTarget.CanvasGroup:
                        CanvasGroup group = GetComponent<CanvasGroup>();
                        group.alpha = FadeOut();
                        break;
                    case FadeTarget.CanvasRenderer:
                        CanvasRenderer renderer = GetComponent<CanvasRenderer>();
                        renderer.SetAlpha(FadeOut());
                        break;
                }
            }
        }
    }

    static float Lerp(float v0, float v1, float t)
    {
        return (1 - t) * v0 + t * v1;
    }

    protected float FadeIn(float val)
    {
        //reset timers and bools
        Reset();

        //the float value to return
        float alphaVal = 0.0f;

        //if fadeOut was not interrupted by fadeIn
        //Lerp from 0, otherwise lerp from current alpha value
        if (!interrupt)
        {
            alphaVal = Lerp(minValue, maxValue, time / fadeInTime);
        }
        else
        {
            alphaVal = Lerp(val, maxValue, time / fadeInTime);
        }

        time += Time.deltaTime;

        //if target is fully opaque, reset values
        if (alphaVal > maxValue - 0.01f)
        {
            interrupt = false;
            startFadeDelay = true;
            active = true;
            fadeIn = false;
            time = 0.0f;

            if(disableAfterFadeIn)
            {
                stop = true;
            }
        }

        return alphaVal;
    }

    protected float FadeOut()
    {
        //target is no longer active
        active = false;
        float alphaVal;

        //lerp from 1 to 0
        alphaVal = Lerp(maxValue, minValue, time / fadeOutTime);
        time += Time.deltaTime;

        //if the target is fully transparent, reset values
        if (alphaVal < minValue + 0.001f)
        {
            fadeOut = false;
            time = 0.0f;

            if (pulse)
            {
                fadeIn = true;
            }
        }
        return alphaVal;
    }

    protected void Reset()
    {
        //if fadeOut and fadeIn are both true, fadeIn will interrupt the fadeOut
        //and continue from the current alpha value
        if (fadeOut)
        {
            interrupt = true;
            fadeOut = false;
            time = 0.0f;
        }

        //reset the delay timer
        if (startFadeDelay)
        {
            startFadeDelay = false;
            fadeDelayTimer = 0.0f;
        }

    }
}

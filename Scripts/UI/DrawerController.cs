﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerController : MonoBehaviour
{
    RectTransform thisRect;
    public RectTransform mainMenuRect;
    private Vector3 closedPosition;
    private Vector3 openedPosition;

    private bool open;
    private bool close;
    public float offsetOpen;
    public float offsetClosed;

    private bool Opened
    {
        set
        {

        }
    }

    void Awake()
    {
        thisRect = GetComponent<RectTransform>();
    }

    public void Open()
    {
        open = true;
        close = false;
    }

    public void Close()
    {
        close = true;
        open = false;
    }

    void Update()
    {
        closedPosition = new Vector3(mainMenuRect.position.x + (Screen.width * (offsetClosed/1920f)), mainMenuRect.position.y, mainMenuRect.position.z);
        openedPosition = new Vector3(Screen.width * (offsetOpen/1920f), mainMenuRect.position.y, mainMenuRect.position.z);

        if(open)
        {
            thisRect.position = Vector3.Lerp(thisRect.position, openedPosition, 0.2f);

            if (Vector2.Distance(thisRect.position, openedPosition) <= 0.001f)
            {
                open = false;
            }
        }

        if(close)
        {
            thisRect.position = Vector3.Lerp(thisRect.position, closedPosition, 0.2f);

            if (Vector2.Distance(thisRect.position, closedPosition) <= 0.001f)
            {
                close = false;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HighLightEvent : EventTrigger {

    OnButtonHighlight buttonHighlight;

    public void Awake()
    {
        buttonHighlight = GetComponent<OnButtonHighlight>();
    }

    public override void OnSelect(BaseEventData eventData)
    {
        buttonHighlight.onEnter = true;
        buttonHighlight.onExit = false;

        if (buttonHighlight.highLightSFX != null)
        {
            AudioManager.Instance.playSFX(buttonHighlight.highLightSFX);
        }
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        buttonHighlight.onExit = true;
        buttonHighlight.onEnter = false;
    }
}

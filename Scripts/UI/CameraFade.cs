﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraFade : MonoBehaviour
{
    public Image fadeImage;

    public void Awake()
    {
        fadeImage.canvasRenderer.SetAlpha(0.0f);
    }

    public void FadeToBlack()
    {
        fadeImage.canvasRenderer.SetAlpha(0.0f);
        fadeImage.CrossFadeAlpha(1.0f, 3.0f, false);
    }

    public void FadeFromBlack()
    {
        fadeImage.canvasRenderer.SetAlpha(1.0f);
        fadeImage.CrossFadeAlpha(0.0f, 3.0f, false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class UIManager : MonoBehaviour
{
    public GameObject audioManager;

    [Header("Tutorial Elements")]
    public GameObject mainTutorialPanel;
    public TextMeshProUGUI mainTutorialText;
    public CanvasGroup tutorialPanel;
    public TextMeshProUGUI tutorialText;
    public TextMeshProUGUI acceptText;
    //public TextMeshProUGUI GIFtext;
    //public GameObject animatedGIF;

    private Animator anim;
    [Space(10)]
    [Header("Main UI Elements")]
    public Canvas hudCanvas;
    public GameObject MenuCanvas;
    public GameObject DeathCanvas;
    public UIMenu mainMenu;
    public DrawerController[] drawers;
    public Text promptText;
    public Button promptConfirm;
    public GameObject DefaultsImage;

    [HideInInspector]
    public UIMenu currentMenu;
    [HideInInspector]
    public bool inMenu;
    [HideInInspector]
    public bool inTutorial;
    [HideInInspector]
    public bool inContext;
    [HideInInspector]
    public bool inDeathPrompt;

    [Space(5)]
    [Header("SFX")]
    public AudioClip drawerOpen_SFX;
    public AudioClip pausePressed_SFX;

    void Awake()
    {
        //anim = animatedGIF.GetComponent<Animator>();

        if(FindObjectOfType<AudioManager>() == null)
        {
            Instantiate(audioManager);
        }
    }

    void Update()
    {
        if (!inDeathPrompt && !Managers.Instance.Level.loading)
        {
            if (Input.GetButtonDown(Buttons.Start))
            {
                if (!inMenu && !inTutorial)
                {
                    AudioListener.pause = true;
                    AudioManager.Instance.playSFX(pausePressed_SFX);
                    inMenu = true;
                    Time.timeScale = 0.0f;
                    hudCanvas.gameObject.SetActive(false);
                    MenuCanvas.gameObject.SetActive(true);
                    currentMenu = mainMenu;
                    EventSystem.current.SetSelectedGameObject(null);
                    currentMenu.firstSelected.Select();
                }
                else
                {
                    if (currentMenu == mainMenu)
                    {
                        AudioListener.pause = false;
                        OnCancel();
                    }
                }
            }

            if (inMenu)
            {
                if (Input.GetButtonDown(Buttons.B))
                {
                    OnCancel();
                }
            }
        }
    }

    public void OnSubMenu(int i)
    {
        currentMenu.lastSelected = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        currentMenu.DisableMenu();
        currentMenu = currentMenu.getSubMenu(i);
        currentMenu.gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        if (currentMenu.firstSelected != null)
        {
            currentMenu.firstSelected.Select();
        }
    }

    public void OnCancel()
    {
        foreach (UnityEvent ev in currentMenu.onCloseEvents)
        {
            ev.Invoke();
        }

        if (currentMenu.parent != null)
        {
            currentMenu.parent.EnableMenu();
            if (currentMenu.parent.lastSelected != null)
            {
                currentMenu.parent.lastSelected.Select();
            }
            else
            {
                currentMenu.parent.firstSelected.Select();
            }
            currentMenu.gameObject.SetActive(false);

            currentMenu = currentMenu.parent;
        }
        else
        {
            AudioListener.pause = false;
            inMenu = false;
            Time.timeScale = 1.0f;
            MenuCanvas.gameObject.SetActive(false);
            hudCanvas.gameObject.SetActive(true);
        }
    }

    public void CloseDrawer(int i)
    {
        drawers[i].Close();
        AudioManager.Instance.playSFX(drawerOpen_SFX);

        if (i == 1)
        {
            DefaultsImage.SetActive(false);
        }
    }

    public void OpenDrawer(int i)
    {
        drawers[i].Open();
        AudioManager.Instance.playSFX(drawerOpen_SFX);

        if (i == 1)
        {
            DefaultsImage.SetActive(true);
        }

    }


    public void OnPromptConfirm(string option)
    {
        switch (option)
        {
            case "Exit":
                //TODO: save prompt some time in the future
                Application.Quit();
                break;
            case "Save":
                //do things
                break;
            case "Title":
                Time.timeScale = 1.0f;
                AudioListener.pause = false;
                Destroy(AudioManager.Instance.gameObject);
                SceneManager.LoadScene("TitleScene");
                break;
        }
    }

    public void SetupPrompt(string option)
    {
        switch (option)
        {
            case "Exit":
                promptText.text = "Are you sure you want to quit?";
                promptConfirm.onClick.RemoveAllListeners();
                promptConfirm.onClick.AddListener(() => OnPromptConfirm(option));
                break;
            case "Save":
                promptText.text = "Save Changes?";
                promptConfirm.onClick.RemoveAllListeners();
                promptConfirm.onClick.AddListener(() => OnPromptConfirm(option));
                break;
            case "Title":
                promptText.text = "Return to Main Menu?";
                promptConfirm.onClick.RemoveAllListeners();
                promptConfirm.onClick.AddListener(() => OnPromptConfirm(option));
                break;
        }
    }

    public void SetupTutorialCanvas(string tutText, string accept)
    {
        acceptText.text = accept;
        //animatedGIF.SetActive(showGIF);
        //if (showGIF)
        //{
        //    GIFtext.text = tutText;
        //    if (clip)
        //    {
        //        AnimatorOverrideController aoc = new AnimatorOverrideController(anim.runtimeAnimatorController);
        //        List<KeyValuePair<AnimationClip, AnimationClip>> anims = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        //        foreach (AnimationClip a in aoc.animationClips)
        //            anims.Add(new KeyValuePair<AnimationClip, AnimationClip>(a, clip));
        //        aoc.ApplyOverrides(anims);
        //        anim.runtimeAnimatorController = aoc;
        //    }
        //}
        //else
        //{
        //    tutorialText.text = tutText;
        //}

        mainTutorialText.text = tutText;
    }

    public void DoTutorialText(string tutText)
    {
        tutorialText.text = tutText;
    }

    public void ResetTutorialText()
    {
        mainTutorialText.text = "";
        tutorialText.text = "";
        //GIFtext.text = "";
    }

    public void TutorialFadeIn()
    {
        GroupFade group = tutorialPanel.GetComponent<GroupFade>();
        group.fadeIn = true;
    }

    public void TutorialFadeOut()
    {
        GroupFade group = tutorialPanel.GetComponent<GroupFade>();
        group.fadeOut = true;
    }

    public void DisplayTutorial(bool val)
    {
        mainTutorialPanel.gameObject.SetActive(val);
        tutorialPanel.GetComponent<GroupFade>().fadeOut = true;
        inTutorial = val;
    }

    public void DisplayDeathPrompt(bool val)
    {
        DeathCanvas.gameObject.SetActive(val);
        hudCanvas.gameObject.SetActive(!val);
        inDeathPrompt = val;

        EventSystem.current.SetSelectedGameObject(null);
        UIMenu deathMenu = DeathCanvas.GetComponentInChildren<UIMenu>();
        deathMenu.firstSelected.Select();
        EventSystem.current.SetSelectedGameObject(deathMenu.firstSelected.gameObject);
        if (!val)
        {
            Time.timeScale = 1.0f;
        }
    }

    public IEnumerator DeathPromptDelay()
    {
        yield return new WaitForSeconds(3);
        DisplayDeathPrompt(true);
        Time.timeScale = 0.0f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager: MonoBehaviour
{
    public GameObject audioManager;
    public GameObject mainMenuCanvas;
    public UIMenu mainMenu;
    public DrawerController[] drawers;
    public UIMenu currentMenu;

    public GameObject DefaultsImage;

    public Text promptText;
    public Button promptConfirm;

    public string sceneToLoad;

    [Space(5)]
    [Header("SFX")]
    public AudioClip drawerOpen_SFX;
    public AudioClip startGame_SFX;

    void Awake()
    {
        if(FindObjectOfType<AudioManager>() == null)
        {
            Instantiate(audioManager);
        }
    }

    void Start()
    {
        currentMenu = mainMenu;
        EventSystem.current.SetSelectedGameObject(null);
        currentMenu.firstSelected.Select();
    }

    public void SetFirstSelected()
    {
        EventSystem.current.SetSelectedGameObject(null);
        currentMenu.firstSelected.Select();
    }
    
    void Update()
    {
        if (mainMenuCanvas.activeInHierarchy)
        {
            if (Input.GetButtonDown(Buttons.B))
            {
                OnCancel();
            }
        }
    }

    public void OnSubMenu(int i)
    {
        currentMenu.lastSelected = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        currentMenu.DisableMenu();
        currentMenu = currentMenu.getSubMenu(i);
        currentMenu.gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        if (currentMenu.firstSelected != null)
        {
            currentMenu.firstSelected.Select();
        }
    }

    public void OnCancel()
    {
        foreach(UnityEvent ev in currentMenu.onCloseEvents)
        {
            ev.Invoke();
        }

        if (currentMenu.parent != null)
        {
            currentMenu.parent.EnableMenu();
            if (currentMenu.parent.lastSelected != null)
            {
                currentMenu.parent.lastSelected.Select();
            }
            else
            {
                currentMenu.parent.firstSelected.Select();
            }
            currentMenu.gameObject.SetActive(false);

            currentMenu = currentMenu.parent;
        }
    }

    public void CloseDrawer(int i)
    {
        drawers[i].Close();
        AudioManager.Instance.playSFX(drawerOpen_SFX);

        if(i == 1)
        {
            DefaultsImage.SetActive(false);
        }
    }
    
    public void OpenDrawer(int i)
    {
        drawers[i].Open();
        AudioManager.Instance.playSFX(drawerOpen_SFX);

        if (i == 1)
        {
            DefaultsImage.SetActive(true);
        }

    }

    public void OnPromptConfirm(string option)
    {
        switch (option)
        {
            case "Exit":
                //TODO: save prompt some time in the future
                Application.Quit();
                break;
            case "Save":
                //do things
                break;
        }
    }

    public void SetupPrompt(string option)
    {
        switch(option)
        {
            case "Exit":
                promptText.text = "Are you sure you want to quit?";
                promptConfirm.onClick.RemoveAllListeners();
                promptConfirm.onClick.AddListener(() => OnPromptConfirm("Exit"));

                break;
            case "Save":
                promptText.text = "Save Changes?";
                promptConfirm.onClick.RemoveAllListeners();
                promptConfirm.onClick.AddListener(() => OnPromptConfirm("Save"));
                break;
        }
    }

    public void Play()
    {
        AudioManager.Instance.sfxSource.PlayOneShot(startGame_SFX);
        //SceneManager.LoadScene(sceneToLoad);
        StartCoroutine(PlayGame());
        //AudioManager.Instance.playSFX(playGameSFX);
    }

    IEnumerator PlayGame()
    {
        currentMenu.DisableMenu();

        AsyncOperation sync = SceneManager.LoadSceneAsync("LoadingScene", LoadSceneMode.Additive);
        while (!sync.isDone) { yield return null; }

        SceneLoad sceneLoad= FindObjectOfType<SceneLoad>();

        sceneLoad.BeginSceneTranstition(sceneToLoad, new Vector3(0, 0, 0));
    }
}

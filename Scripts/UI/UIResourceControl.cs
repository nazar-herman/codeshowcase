﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;


public class UIResourceControl : MonoBehaviour
{
    [Header("LightBar & Health")]
    public HealthCtrlBase health;
    public LightController lightCtrl;
    public RectTransform lightBar;
    public GameObject[] swordImages;

    [Space(10)]
    [Header("Abilities")]
    public GameObject[] AbilityImages;
    public GameObject[] AbilitySelectors;
    public RectTransform Blue_Fade;
    public RectTransform Red_Fade;
    public RectTransform Yellow_Fade;

    private List<HealthTick> healthTicks;
    private int healthPerTick;

    GameObject currentImage;

    GameObject currentSelector;

    StunBallAbility yellowAbility;
    DashAbility redAbility;
    BombAbility blueAbility;

    void Awake()
    {
        healthTicks = new List<HealthTick>();
        health = FindObjectOfType<CharacterControl>().GetComponent<HealthCtrlBase>();
        lightCtrl = FindObjectOfType<CharacterControl>().GetComponent<LightController>();
        yellowAbility = FindObjectOfType<StunBallAbility>();
        redAbility = FindObjectOfType<DashAbility>();
        blueAbility = FindObjectOfType<BombAbility>();
    }

    public void SetupHealthTicks()
    {
        healthTicks = GetComponentsInChildren<HealthTick>().ToList();
        healthPerTick = health.maxhealth.Data / healthTicks.Count;

        for (int i = 0; i < healthTicks.Count; i++)
        {
            healthTicks[i].min = ((healthTicks.Count - i - 1) * healthPerTick) + 1;
            healthTicks[i].max = ((healthTicks.Count - i) * healthPerTick);
        }
    }

    public void DoLightBar()
    {
        lightBar.localScale = Vector3.Lerp(lightBar.localScale, new Vector3(1, lightCtrl.LightPercent, 1), 0.1f);
    }

    public void DoCooldowns()
    {
        if(yellowAbility.OnCooldown)
        {
            if(!Yellow_Fade.gameObject.activeInHierarchy)
            {
                Yellow_Fade.gameObject.SetActive(true);
            }

            Yellow_Fade.localScale = Vector3.Lerp(Yellow_Fade.localScale, new Vector3(1, yellowAbility.CooldownTimer/yellowAbility.cooldown, 1), 1.0f);
        }
        else
        {
            if (Yellow_Fade.gameObject.activeInHierarchy)
            {
                Yellow_Fade.gameObject.SetActive(false);
            }
        }

        if (redAbility.OnCooldown)
        {
            if (!Red_Fade.gameObject.activeInHierarchy)
            {
                Red_Fade.gameObject.SetActive(true);
            }

            Red_Fade.localScale = Vector3.Lerp(Red_Fade.localScale, new Vector3(1, redAbility.CooldownTimer / redAbility.cooldown, 1), 1.0f);
        }
        else
        {
            if (Red_Fade.gameObject.activeInHierarchy)
            {
                Red_Fade.gameObject.SetActive(false);
            }
        }

        
        if (blueAbility.OnCooldown)
        {
            if (!Blue_Fade.gameObject.activeInHierarchy)
            {
                Blue_Fade.gameObject.SetActive(true);
            }

            Blue_Fade.localScale = Vector3.Lerp(Yellow_Fade.localScale, new Vector3(1, blueAbility.CooldownTimer / blueAbility.cooldown, 1), 1.0f);
        }
        else
        {
            if(blueAbility.currentStatue != null)
            {
                Blue_Fade.localScale = new Vector3(1, 1, 1);
                Blue_Fade.gameObject.SetActive(true);
            }
            else if (Blue_Fade.gameObject.activeInHierarchy)
            {
                Blue_Fade.gameObject.SetActive(false);
            }
        }
    }

    void Update()
    {
        DoLightBar();
        DoCooldowns();

        if (lightCtrl != null)
        {
            if (!swordImages[(int)lightCtrl.lightColor].activeInHierarchy)
            {
                if (currentImage != null)
                {
                    currentImage.SetActive(false);
                }

                currentImage = swordImages[(int)lightCtrl.lightColor];
                currentImage.SetActive(true);
            }

            if ((int)lightCtrl.lightColor > 0)
            {
                if (!AbilitySelectors[(int)lightCtrl.lightColor - 1].activeInHierarchy)
                {
                    if (currentSelector != null)
                    {
                        currentSelector.SetActive(false);
                    }

                    currentSelector = AbilitySelectors[(int)lightCtrl.lightColor - 1];
                    currentSelector.SetActive(true);
                }
            }
            else
            {
                if (currentSelector != null)
                {
                    currentSelector.SetActive(false);
                    currentSelector = null;
                }
            }
        }
    }
}

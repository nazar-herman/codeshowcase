﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthTick : MonoBehaviour
{
    public int min;
    public int max;

    public Image tickImage;
    public IntData healthData;

    int tickHealthMax;

    public int currentTickHealth;
    public float currentTickPercent;

    void Start()
    {
        healthData = GetComponentInParent<UIResourceControl>().health.healthData;
        tickHealthMax = max - min;
    }

    void Update()
    {

        currentTickHealth = healthData.Data - min;
        currentTickPercent = (float)currentTickHealth / tickHealthMax;
        tickImage.color = Color.Lerp(tickImage.color, new Color(tickImage.color.r, tickImage.color.g, tickImage.color.b, (1f*currentTickPercent)), 0.2f);

    }
}

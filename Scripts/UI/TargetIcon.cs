﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetIcon : MonoBehaviour
{
    public GameObject camera;
    public GameObject target;
    public Image targetImage;
    public GameObject head;

    void Update()
    {
        if (target != null)
        {
            if (head != null)
            {
                transform.position = head.transform.position + Vector3.up;
            }
            else
            {
                Collider col = target.GetComponent<BoxCollider>();
                transform.position = new Vector3(col.transform.position.x, col.bounds.center.y + col.bounds.extents.y, col.transform.position.z) + (Vector3.up * 0.5f);
            }
        }

        if(gameObject.activeInHierarchy)
        {
            LookAtCamera();
        }
    }

    void LookAtCamera()
    {
        transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward,
            camera.transform.rotation * Vector3.up);
    }

    public void SetTarget(GameObject targ)
    {
        target = targ;
        if (targ.GetComponentInChildren<Head>() != null)
        {
            head = target.GetComponentInChildren<Head>().gameObject;
        }
    }
}

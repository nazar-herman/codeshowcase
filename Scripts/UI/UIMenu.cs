﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIMenu : MonoBehaviour
{
    public UIMenu parent;
    public List<UIMenu> subMenus;
    public Selectable firstSelected;
    public Selectable lastSelected;

    public UnityEvent[] onCloseEvents;

    public UIMenu getSubMenu(int i)
    {
        return subMenus[i];
    }

    public void DisableMenu()
    {
        Selectable[] buttons = GetComponentsInChildren<Button>();

        foreach(Selectable button in buttons)
        {
            button.interactable = false;
        }
    }

    public void EnableMenu()
    {
        Selectable[] buttons = GetComponentsInChildren<Button>();

        foreach (Selectable button in buttons)
        {
            button.interactable = true;
        }
    }
}

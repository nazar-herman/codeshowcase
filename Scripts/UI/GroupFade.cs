﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GroupFade : MonoBehaviour
{
    CanvasGroup canvasGroup;

    public float fadeSpeed;

    public float minValue;
    public float maxValue;

    public bool fadeIn;
    public bool fadeOut;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Update()
    {
        if(fadeIn)
        {
            fadeOut = false;
            FadeIn();
        }

        if(fadeOut)
        {
            FadeOut();
        }
    }

    public void FadeOut()
    {
        canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, minValue, Time.deltaTime * fadeSpeed);

        if(canvasGroup.alpha < 0.01f)
        {
            fadeOut = false;
        }
    }

    public void FadeIn()
    {
        canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, maxValue, Time.deltaTime * fadeSpeed);

        if (canvasGroup.alpha > 0.99f)
        {
            fadeIn = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingsManager : MonoBehaviour
{
    [Header("Audio")]
    public AudioMixer audioMixer;

    public Slider MasterVol;
    public Slider MusicVol;
    public Slider SFXVol;
    public Slider AmbientVol;

    [Space(5)]
    [Header("Video")]
    public Toggle Fullscreen;
    public Text fullscreenText;
    public Dropdown Resolution;
    public Slider Brightness;

    void Start()
    {
        MasterVol.onValueChanged.AddListener(delegate { OnMasterVol(); });
        MusicVol.onValueChanged.AddListener(delegate { OnMusicVol(); });
        SFXVol.onValueChanged.AddListener(delegate { OnSFXVol(); });
        AmbientVol.onValueChanged.AddListener(delegate { OnAmbientVol(); });

        Fullscreen.onValueChanged.AddListener(delegate { OnFullscreenToggle(); });
        Brightness.onValueChanged.AddListener(delegate { OnBrightness(); });

        Load();
    }

    float convertRange(float val)
    {
        float retVal = val * 80/100 + -80;

        return retVal;
    }

    public void OnMasterVol()
    {
        audioMixer.SetFloat("masterVol", convertRange(MasterVol.value));
        Settings.masterVol = MasterVol.value;
    }

    public void OnMusicVol()
    {
        audioMixer.SetFloat("musicVol", convertRange(MusicVol.value));
        Settings.musicVol = MusicVol.value;
    }

    public void OnSFXVol()
    {
        audioMixer.SetFloat("sfxVol", convertRange(SFXVol.value));
        Settings.sfxVol = SFXVol.value;
    }

    public void OnAmbientVol()
    {
        audioMixer.SetFloat("ambientVol", convertRange(AmbientVol.value));
        Settings.ambientVol = AmbientVol.value;
    }

    public void OnFullscreenToggle()
    {
        Screen.fullScreen = Settings.fullscreen = Fullscreen.isOn;

        if(Fullscreen.isOn)
        {
            fullscreenText.text = " - ON";
        }
        else
        {
            fullscreenText.text = " - OFF";
        }
    }

    public void OnBrightness()
    {
        //RenderSettings.ambientLight = new Color(Brightness.value, Brightness.value, Brightness.value, 1.0f);
        Settings.brightness = Brightness.value;
    }

    void Load()
    {
        MasterVol.value = Settings.masterVol;
        MusicVol.value = Settings.musicVol;
        SFXVol.value = Settings.sfxVol;
        AmbientVol.value = Settings.ambientVol;

        Fullscreen.isOn = Settings.fullscreen;
        Brightness.value = Settings.brightness;
    }
}

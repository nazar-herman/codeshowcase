﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsWeb : MonoBehaviour {
    private CharacterControl player;
    public float lifeTime;
    public int webSpeed;
    int lastSpeed;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player = other.gameObject.GetComponent<CharacterControl>();
            player.movementSpeed = webSpeed;
        }
    }

    private void Update()
    {
        if (player != null && player.GetComponent<HealthCtrlBase>() != null && player.GetComponent<HealthCtrlBase>().dead)
        {
            player.movementSpeed = player.maxMovementSpeed;
            player = null;
        }
        if (lifeTime < 0.0f)
        {
            Destroy(gameObject);
        }
        else
        {
            lifeTime -= Time.deltaTime;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player.movementSpeed = player.maxMovementSpeed;
            player = null;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player = other.gameObject.GetComponent<CharacterControl>();
            player.movementSpeed = webSpeed;
        }
    }

    public void OnDestroy()
    {
        if(player != null)
        {
            player.movementSpeed = player.maxMovementSpeed;
            player = null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlane : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<CharacterControl>() != null)
        {
            CharacterControl player = other.GetComponent<CharacterControl>();

            player.health.healthData.Data = 0;
        }
    }
}

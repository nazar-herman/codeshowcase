﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HealthPickup : MonoBehaviour
{
    public int healthPerParticle;
    public int lightPerParticle;
    [Range(0,1)]
    public float gravSpeed;
    bool pickup;
    ParticleSystem ps;

    PlayerHealthCtrl phc;
    LightController light;

    AudioSource a_source;

    bool canPickup = false;
    float timer;
    public float pickupAfterTime = 0.5f;

    [Space(10)]
    [Header("SFX")]
    public AudioClip spawnSFX;
    public AudioClip pickUpSFX;

    List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();

    int count;

    void Awake()
    {
        a_source = GetComponent<AudioSource>();
        a_source.PlayOneShot(spawnSFX);
    }
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        phc = FindObjectOfType<PlayerHealthCtrl>();
        light = phc.GetComponent<LightController>();
        ps.trigger.SetCollider(0, phc.GetComponent<Collider>());

    }

    //void OnTriggerEnter(Collider other)
    //{
    //    phc = other.GetComponent<PlayerHealthCtrl>();

    //    if(phc != null)
    //    {
    //        light = phc.GetComponent<LightController>();
    //        ps.trigger.SetCollider(0, phc.GetComponent<Collider>());
    //        pickup = true;
    //    }
    //}

    void Update()
    {
        Destroy(gameObject, ps.main.startLifetime.constant);

        timer += Time.deltaTime;

        if(timer >= pickupAfterTime)
        {
            canPickup = true;
        }

        if (pickup)
        {
            ParticleSystem.Particle[] par = new ParticleSystem.Particle[ps.main.maxParticles];
            int numAlive = ps.GetParticles(par);

            for (int i = 0; i < numAlive; i++)
            {
                par[i].position = Vector3.Slerp(par[i].position, phc.transform.position + (Vector3.up * 2), gravSpeed);
            }

            ps.SetParticles(par, numAlive);
        }
    }

    void OnParticleTrigger()
    {
        if (canPickup)
        {
            int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);

            if (numEnter > 0)
            {
                pickup = true;
            }

            for (int i = 0; i < numEnter; i++)
            {
                a_source.PlayOneShot(pickUpSFX);

                ParticleSystem.Particle p = enter[i];
                p.remainingLifetime = 0;
                enter[i] = p;

                phc.healthData.Data += healthPerParticle;
                light.lightPool.Data += lightPerParticle;
            }

            ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(LightData))]
public class LightDataEditor : Editor {
    LightController lightCtrl;
    private void OnEnable()
    {
        lightCtrl = ((MonoBehaviour)target).gameObject.GetComponent<LightController>();
    }
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        var iterator = serializedObject.GetIterator();
        for (bool enterChildren = true; iterator.NextVisible(enterChildren); enterChildren = false)
        {
            if (iterator.name != "Name")
            {
               EditorGUILayout.PropertyField(iterator, true);
            }
        }
        EditorGUI.EndChangeCheck();
        serializedObject.ApplyModifiedProperties();
    }
    
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(AbilityCtrl))]
public class AbilityCtrlEditor : Editor
{
    AbilityCtrl abilityCtrl;
    AbilityBase[] abilities;
    // Use this for initialization
    void OnEnable()
    {
        abilityCtrl = ((MonoBehaviour)target).gameObject.GetComponent<AbilityCtrl>();
        abilities = ((MonoBehaviour)target).gameObject.GetComponents<AbilityBase>();
    }

    // Update is called once per frame
    public override void OnInspectorGUI()
    {
        if (abilityCtrl != null)
        {
            foreach (AbilityBase ability in abilities)
            {
                if (abilityCtrl.HideAbilities)
                {
                    ability.hideFlags = HideFlags.HideInInspector;
                }
                else
                {
                    ability.hideFlags = HideFlags.None;
                }
            }
        }
        base.OnInspectorGUI();
    }
}

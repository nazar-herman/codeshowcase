﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(LightController))]
public class LightControllerEditor : Editor
{
    LightController lightCtrl;
    LightData[] lights;
    // Use this for initialization
    void OnEnable () {
        lightCtrl = ((MonoBehaviour)target).gameObject.GetComponent<LightController>();
        lights = ((MonoBehaviour)target).gameObject.GetComponents<LightData>();
    }

    // Update is called once per frame
    public override void OnInspectorGUI()
    {
        //if (lightCtrl != null)
        //{
        //    foreach (LightData light in lights)
        //    {
        //        if (lightCtrl.HidePools)
        //        {
        //            light.hideFlags = HideFlags.HideInInspector;
        //        }
        //        else
        //        {
        //            light.hideFlags = HideFlags.None;
        //        }
        //    }
        //}
        base.OnInspectorGUI();
    }
}

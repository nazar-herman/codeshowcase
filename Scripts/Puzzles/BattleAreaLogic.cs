﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleAreaLogic : PuzzleLogic {
    public IsTrigger platform;
    public List<IsDoor> doors;
    public List<GameObject> enemies;
    bool stop;
    bool exectuted;
    // Use this for initialization
    void Start()
    {
        foreach (GameObject enemy in enemies)
        {
            EnemyBehaviourBase behBase = enemy.GetComponent<EnemyBehaviourBase>();
            if(behBase != null)
            {
                behBase.areaLogic = this;
            }
        }

    }

    public override void Update()
    {
        base.Update();
        if(stop)
        {
            return;
        }
        if(platform != null && platform.triggered && !exectuted)
        {
            Execute();
        }
        if(enemies.Count == 0)
        {
            foreach(IsDoor door in doors)
            {
                door.Open();
            }
            stop = true;
        }
    }

    public override void Execute()
    {
        base.Execute();
        foreach (IsDoor door in doors)
        {
            door.Close();
        }
        foreach (GameObject enemy in enemies)
        {
            enemy.SetActive(true);
        }
        exectuted = true;
    }

    public void RemoveEnemy(GameObject obj)
    {
        enemies.Remove(obj);
    }
}

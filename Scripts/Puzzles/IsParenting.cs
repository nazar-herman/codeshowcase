﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsParenting : MonoBehaviour {
    private Transform player;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player = other.transform;
            player.SetParent(transform);
        }
    }

    private void Update()
    {
        if (player.GetComponent<HealthCtrlBase>() != null && player.GetComponent<HealthCtrlBase>().dead)
        {
            player.SetParent(null);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            other.transform.SetParent(null);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeMaterialCtrl : MonoBehaviour {
    public Material notChargedMaterial;
    public Material chargeMaterial;
    public IsChargeable chargeable;
    public float lerpSpeed;
    public float timer = 2.0f;
    bool stop;
    Material mat;
	// Use this for initialization
	void Start () {
        if(chargeable == null)
        {
            chargeable = GetComponent<IsChargeable>();
        }
        if (chargeable == null)
        {
            chargeable = GetComponentInParent<IsChargeable>();
        }
        if(GetComponent<Renderer>() != null)
        {
            mat = GetComponent<Renderer>().material;
        }
    }

    // Update is called once per frame
    void Update () {
		if(chargeable != null && chargeable.triggered && mat != null && !stop)
        {
            mat.Lerp(mat, chargeMaterial, lerpSpeed*Time.deltaTime);
            if(timer >=0.0f)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                GetComponent<Renderer>().material = chargeMaterial;
                stop = true;
            }
        }
	}
}

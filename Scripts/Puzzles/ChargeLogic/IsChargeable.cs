﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class IsChargeable : IsTrigger
{
    public int chargeAmount;
    public int chargeLossPerSecond;
    public int charge;
    public LightColor type;
    Targetable targetable;
    public UnityEvent doEvent;
    public AudioClip ChargeUpSound;

    private AudioSource a_source;
	void Start () {
        charge = 0;
        targetable = GetComponent<Targetable>();
        a_source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if(charge >= chargeAmount)
        {
           triggered = true;
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        //.Log("HIT");
        switch (type)
        {
            case LightColor.Yellow:
                {
                    IsStun isStun = collision.gameObject.GetComponentInChildren<IsStun>();
                    if (isStun != null)
                    {
                        IsDamaging isDam = collision.gameObject.GetComponentInChildren<IsDamaging>();
                        Charge(isDam.damage);
                        Destroy(collision.gameObject);
                    }
                    break;
                }
            case LightColor.Red:
                {
                    //isDashTrigger isDashTrigger = collision.gameObject.GetComponent<isDashTrigger>();
                    //if(isDashTrigger != null)
                    //{
                    //    IsDamaging isDamaging = collision.gameObject.GetComponentInChildren<IsDamaging>();
                    //    charge += isDamaging.damage;
                    //    if (charge >= chargeAmount && targetable != null)
                    //    {
                    //        targetable.DeathCleanup();
                    //        Destroy(targetable);
                    //    }
                    //}
                    break;
                }
        }


    }
    public void Charge(int amount)
    {
        charge += amount;
        if (charge >= chargeAmount && targetable != null)
        {
            a_source.PlayOneShot(ChargeUpSound);

            if (doEvent != null)
            {
                doEvent.Invoke();
            }
            targetable.DeathCleanup();
            Destroy(targetable);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedLogic : PuzzleLogic {
    public List<IsChargeable> chargeables;
    public bool done = false;
    public bool stopChecks = false;
	// Use this for initialization
	public virtual void Start () {
        chargeables = new List<IsChargeable>(GetComponentsInChildren<IsChargeable>());
	}

    public override void Update()
    {
        if(!stopChecks)
        {
            done = true;
            foreach (IsChargeable chargeable in chargeables)
            {
                if (!chargeable.triggered)
                {
                    done = false;
                    break;
                }
            }
            if (done)
            {
                Execute();
            }
        }
    }
    // Update is called once per frame
    public override void Execute () {
        stopChecks = true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum TurnAxis
{
    X,
    Y,
    Z
}

public class TurnDoor : IsDoor {
    public float openedAngle;
    public float closedAngle;
    public TurnAxis turnAxis = TurnAxis.X;
    public float openTurnSpeed;
    public float closeTurnSpeed;
    Vector3 turnVector;
    Vector3 openedVector;
    Vector3 closedVector;
    Quaternion endRotation;
	// Use this for initialization
	public override void Start () {
        base.Start();
        openedVector = openedAngle * Vector3.one;
        openedVector += transform.rotation.eulerAngles;
        closedVector = closedAngle * Vector3.one;
        closedVector += transform.rotation.eulerAngles;

    }

    public override void Update()
    {
        base.Update();
        if (delaying)
        {
            return;
        }
        if(isOpening && !isOpen)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, endRotation, openTurnSpeed * Time.deltaTime);
            if(transform.rotation == endRotation)
            {
                isOpen = true;
                isClosed = false;
                isClosing = false;
                isOpening = false;
            }
        }

        if (isClosing && !isClosed)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, endRotation, closeTurnSpeed * Time.deltaTime);
            if (transform.rotation == endRotation)
            {
                isOpen = false;
                isClosed = true;
                isClosing = false;
                isOpening = false;
            }
        }
    }
    // Update is called once per frame
    public override void Open () {
        if(!isOpening)
        {
            base.Open();
            switch (turnAxis)
            {
                case TurnAxis.X:
                    {
                        turnVector = transform.right;
                        break;
                    }
                case TurnAxis.Y:
                    {
                        turnVector = transform.up;
                        break;
                    }
                case TurnAxis.Z:
                    {
                        turnVector = transform.forward;
                        break;
                    }
            }
            endRotation = Quaternion.Euler(Vector3.Scale(openedVector, turnVector));
        }
    }

    public override void Close()
    {
        if (!isClosing)
        {
            base.Close();
            switch (turnAxis)
            {
                case TurnAxis.X:
                    {
                        turnVector = transform.right;
                        break;
                    }
                case TurnAxis.Y:
                    {
                        turnVector = transform.up;
                        break;
                    }
                case TurnAxis.Z:
                    {
                        turnVector = transform.forward;
                        break;
                    }
            }

            endRotation = Quaternion.Euler(Vector3.Scale(closedVector, turnVector));
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisolveDoor : IsDoor
{
    public float disolveTime;
    float disolveTimer;
    Material mat;
    Collider col;
    float disolveSpeed;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        mat = GetComponent<Renderer>().material;
        disolveSpeed = 1 / disolveTime;
        col = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isOpening && !isOpen)
        {
            disolveTimer += Time.deltaTime;
            if(disolveTimer < disolveTime)
            {
                mat.SetFloat("_DissolveAmount", Mathf.Lerp(0, 1, disolveTimer/disolveTime));
            }
            else
            {
                isOpen = true;
            }
            if(disolveTimer >= (disolveTime - 1) && col != null && col.enabled)
            {
                col.enabled = false;
            }
        }
        if(isOpen)
        {
            Destroy(gameObject);
        }
    }
    public override void Open()
    {
        base.Open();

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDoor : IsDoor {
    public Transform openWaypoint;
    public Transform closedWaypoint;
    public float openSpeed;
    public float closeSpeed;
    Vector3 openPoint;
    Vector3 closedPoint;

    // Use this for initialization
    public override void Start ()
    {
        base.Start();
        openPoint = openWaypoint.position;
        closedPoint = closedWaypoint.position;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (delaying)
        {
            return;
        }
        if (isOpening && !isOpen)
        {
            transform.position = Vector3.Lerp(transform.position, openPoint, openSpeed * Time.deltaTime);
            if(Vector3.Distance(transform.position, openPoint) < 0.1f)
            {
                transform.position = openPoint;
                isOpen = true;
            }
        }
        if (isClosing && !isClosed)
        {
            transform.position = Vector3.Lerp(transform.position, closedPoint, closeSpeed * Time.deltaTime);
            if (Vector3.Distance(transform.position, closedPoint) < 0.1f)
            {
                transform.position = closedPoint;
                isClosed = true;
            }
        }

    }
    public override void Open()
    {
        if (!isOpening && !isOpen)
        {
            base.Open();
        }
    }

    public override void Close()
    {
        if (!isClosing && !isClosed)
        {
            base.Close();
        }
    }
}

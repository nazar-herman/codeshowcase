﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentDoor : IsDoor
{
    List<IsDoor> childDoors;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        childDoors = new List<IsDoor>();
        IsDoor[] doors = GetComponentsInChildren<IsDoor>();
        foreach (IsDoor door in doors)
        {
            if (door != this)
            {
                childDoors.Add(door);
            }
        }
    }
    // Update is called once per frame
    public override void Update()
    {
        //base.Update();
        isOpen = true;
        foreach(IsDoor door in childDoors)
        {
            if(!door.isOpen)
            {
                isOpen = false;
                break;
            }
        }
        isClosed = true;
        foreach (IsDoor door in childDoors)
        {
            if (!door.isClosed)
            {
                isClosed = false;
                break;
            }
        }
    }

    public override void Open()
    {
        //base.Open();
        foreach (IsDoor door in childDoors)
        {
            door.Open();
        }
    }

    public override void Close()
    {
        //base.Close();
        foreach(IsDoor door in childDoors)
        {
            door.Close();
        }
    }
}


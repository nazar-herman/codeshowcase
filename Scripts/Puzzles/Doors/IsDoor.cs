﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsDoor : MonoBehaviour {

    AudioSource a_source;
    [Header("Sound Effects")]
    public AudioClip OpenOneShot;
    public AudioClip CloseOneShot;
    public AudioClip OpeningContinuous;
    public AudioClip ClosingContinuous;
    [Space(10)]
    public bool isOpen;
    public bool isClosed;
    public bool isOpening;
    public bool isClosing;
    public bool delaying;
    public float openDelay;
    public float closeDelay;
    float timer;
	// Use this for initialization
	public virtual void Start ()
    {
        a_source = GetComponent<AudioSource>();
	}
	
    public virtual void Update()
    {
        if(delaying)
        {
            if (timer > 0.0f)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                delaying = false;
            }
        }
        else
        {
            if(isOpening)
            {
                if(OpeningContinuous != null)
                {
                    if (!a_source.isPlaying)
                    {
                        a_source.clip = OpeningContinuous;
                        a_source.Play();
                    }
                }
            }

            if(isClosing)
            {
                if(ClosingContinuous != null)
                {
                    if (!a_source.isPlaying)
                    {
                        a_source.clip = ClosingContinuous;
                        a_source.Play();
                    }
                }
            }
        }

        if(isClosed || isOpen)
        {
            if (a_source.clip != null)
            {
                a_source.Stop();
                a_source.clip = null;
            }
        }
    }

	// Update is called once per frame
	public virtual void Open () {
        if(isClosed || isClosing)
        {
            timer = openDelay;
            delaying = true;
        }

        if (!isOpening)
        {
            if (OpenOneShot != null)
            {
                a_source.PlayOneShot(OpenOneShot);
            }
        }

        isOpening = true;
        isClosing = false;
        isClosed = false;
	}

    public virtual void Close()
    {
        if (isOpen || isOpening)
        {
            timer = closeDelay;
            delaying = true;
        }

        if (!isClosing)
        {
            if (CloseOneShot != null)
            {
                a_source.PlayOneShot(CloseOneShot);
            }
        }

        isClosing = true;
        isOpening = false;
        isOpen = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fence_IsTriggerOpener : MonoBehaviour
{
    public GameObject[] fences;

    void OnTriggerEnter(Collider other)
    {
        foreach(GameObject fence in fences)
        {
            fence.GetComponent<Collider>().isTrigger = true;
        }
    }
}

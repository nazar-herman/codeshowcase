﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeHoleGate_Trigger : MonoBehaviour
{
    public GameObject guards;
    public GameObject objectToDestory1;
    public GameObject objectToDestory2;

    private bool isTriggered = false;

    private void Awake()
    {
        if (objectToDestory1 == null || objectToDestory2 == null)
        {
            Debug.LogError("ObjectToDestroy not found!");
        }
        
    }

    void Update()
    {
        if (guards)
        {
            if (GetComponent<TriggerToOpenObject>().triggerON == true)
            {
                guards.SetActive(true);
            }
        }
        else
        {
            Debug.Log("No guards to be activated!");
        }

        if (objectToDestory1 == null && objectToDestory2 == null && !isTriggered)
        {
            GetComponent<TriggerToOpenObject>().triggerON = true;
            isTriggered = true;
        }

    }
}

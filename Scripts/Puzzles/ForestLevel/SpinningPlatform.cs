﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningPlatform : MonoBehaviour
{
    public float speed = 20;

    private void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrangeCreature : MonoBehaviour
{
    public float scaleChangeRate = 1f;

    private Transform strangeCreater;
    private Vector3 changeOffset;
    public bool isScared = false;
    public bool shrink = false;
    private float maxScale;
    private float minScale = 0.1f;

    private void Start()
    {
        strangeCreater = transform.GetChild(0);
        changeOffset = new Vector3(scaleChangeRate, scaleChangeRate, scaleChangeRate);
        maxScale = transform.localScale.x;
    }

    private void Update()
    {
        if (shrink)
        {
            strangeCreater.localScale -= changeOffset * Time.deltaTime;

            if (strangeCreater.localScale.x <= minScale)
            {
                shrink = false;
            }
        }

        if (!shrink && !isScared && strangeCreater.localScale.x < maxScale)
        {
            strangeCreater.localScale += changeOffset * Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        isScared = true;
        shrink = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isScared = false;
    }

   
}

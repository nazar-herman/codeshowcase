﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashCrystalToOpenDoor : MonoBehaviour
{
    public GameObject crystalToSlash;

    private void Update()
    {
        if (crystalToSlash == null)
        {
            GetComponent<MoveToPoint>().triggerON = true;
        }
    }
}

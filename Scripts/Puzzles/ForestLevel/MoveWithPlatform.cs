﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithPlatform : MonoBehaviour
{
    private Transform player;

    private void Update()
    {
        if (player.GetComponent<HealthCtrlBase>() != null && player.GetComponent<HealthCtrlBase>().dead)
        {
            player.SetParent(null);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        other.transform.SetParent(transform);

        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player = other.transform;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        other.transform.SetParent(null);
    }
}

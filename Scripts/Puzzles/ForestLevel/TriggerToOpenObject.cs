﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Used to move an object between its original position and a target waypoint
public class TriggerToOpenObject : MonoBehaviour
{
    public float speed = 7.0f;
    public bool triggerON = false;

    private Vector3 originalPos;
    private Vector3 targetPos;
    private bool targetGot = false;

    void Awake()
    {
        originalPos = transform.position;

        //Get the first waypoint child as target and then detach it, so the object can move without moving this waypoint
        foreach (Transform child in transform)
        {
            if (!targetGot && child.tag == "Waypoint")
            {
                targetPos = child.position;
                child.SetParent(null);
                targetGot = true;
            }
        }

        if (targetPos == null)
        {
            Debug.LogError("No waypoint found!");
        }
    }

    void Update()
    {
        if (triggerON)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * speed);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, originalPos, Time.deltaTime * speed);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        foreach (Transform child in transform)
        {
            if (child.tag == "Waypoint")
            {
                Gizmos.DrawSphere(child.position, .7f);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveToPoint_Multiple : MonoBehaviour
{
    public float speed = 4.0f;
    public float delay = 0.0f;
    public enum type { PlayOnce, Loop, PingPong }
    public type movementType;
    public bool triggerON = false;

    private int currentWp;
    private float arrivalTime;
    private bool forward = true, arrived = false;
    private List<Transform> waypoints = new List<Transform>();

    void Awake()
    {
        //get child waypoints, then detach them (so object can move without moving waypoints)
        foreach (Transform child in transform)
        {
            //if (child.tag == "Waypoint")
            //{
                waypoints.Add(child);
            //}
        }

        foreach (Transform waypoint in waypoints)
        {
            waypoint.parent = null;
        }

        if (waypoints.Count == 0)
        {
            Debug.LogError("No waypoints found!");
        }
    }

    void Update()
    {
        //when arrived at waypoint, get the next one
        if (waypoints.Count > 0)
        {
            if (!arrived)
            {
                if (Vector3.Distance(transform.position, waypoints[currentWp].position) < 0.3f)
                {
                    arrivalTime = Time.time;
                    arrived = true;
                }
            }
            else
            {
                if (Time.time > arrivalTime + delay)
                {
                    GetNextWP();
                    arrived = false;
                }
            }
        }

        if (triggerON && !arrived && waypoints.Count > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, waypoints[currentWp].position, Time.deltaTime * speed);
            //Vector3 direction = waypoints[currentWp].position - transform.position;
            //rigid.MovePosition(transform.position + (direction.normalized * speed * Time.fixedDeltaTime));
        }
    }


    private void GetNextWP()
    {
        if (movementType == type.PlayOnce)
        {
            currentWp++;
            if (currentWp == waypoints.Count)
                enabled = false;
        }

        if (movementType == type.Loop)
            currentWp = (currentWp == waypoints.Count - 1) ? 0 : currentWp += 1;

        if (movementType == type.PingPong)
        {
            if (currentWp == waypoints.Count - 1)
                forward = false;
            else if (currentWp == 0)
                forward = true;
            currentWp = (forward) ? currentWp += 1 : currentWp -= 1;
        }
    }

    //draw gizmo spheres for waypoints
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        foreach (Transform child in transform)
        {
            //if (child.tag == "Waypoint")
            //{
                Gizmos.DrawSphere(child.position, .7f);
            //}
        }
    }
}

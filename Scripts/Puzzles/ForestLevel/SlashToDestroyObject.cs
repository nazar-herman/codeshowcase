﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashToDestroyObject : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<IsDamaging>() != null && other.GetComponent<IsDamaging>().color == LightColor.Blue)
        {
            Destroy(gameObject);
        }
    }
}

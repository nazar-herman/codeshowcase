﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : MonoBehaviour
{
    public Transform doors;
    public Transform enemies;
    //public Transform thorns;
    //public GameObject crystalToSlash;

    MoveToPoint doorMoveToPoint;
    MoveToPoint thornMoveToPoint;

    private void Update()
    {
        /*if (enemies.childCount == 0 && crystalToSlash &&!crystalToSlash.activeSelf)
        {
            crystalToSlash.SetActive(true);
            
            if (thorns != null)
            {
                thorns.gameObject.SetActive(false);
            }
        }

        if (crystalToSlash == null)
        {
            foreach (Transform door in doors)
            {
                door.gameObject.SetActive(false);
            }
        }*/

        if (enemies.childCount == 0)
        {
            foreach (Transform door in doors)
            {
                door.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>() != null)
        {
            foreach (Transform door in doors)
            {
                if (doorMoveToPoint = door.GetComponent<MoveToPoint>())
                    doorMoveToPoint.triggerON = true;
            }

            foreach (Transform enemy in enemies)
            {
                enemy.gameObject.SetActive(true);
            }

            /*if (thorns != null)
            {
                foreach (Transform thorn in thorns)
                {
                    if (thornMoveToPoint = thorn.GetComponent<MoveToPoint>())
                        thornMoveToPoint.triggerON = true;
                }
            }*/
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBallLogic : MonoBehaviour
{
    public GameObject rope;
    public float initialForce = 10;

    public GameObject fence_IsTriggerOpener;

    private Rigidbody rb;
    private bool isTriggered = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (!rope.activeSelf && !isTriggered)
        {
            transform.parent = null;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            rb.AddForce(Vector3.right * initialForce);
            fence_IsTriggerOpener.SetActive(true);

            isTriggered = true;
        }

    }
}

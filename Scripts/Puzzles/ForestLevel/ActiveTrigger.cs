﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveTrigger : MonoBehaviour
{
    public Transform targetObjects;
    public GameObject trigger;

    TriggerToOpenObject triggerMoveToPoint;

    private void Start()
    {
        triggerMoveToPoint = trigger.GetComponent<TriggerToOpenObject>();
    }

    private void Update()
    {
        if (targetObjects.childCount == 0)
        {
            trigger.SetActive(true);
            triggerMoveToPoint.enabled = true;
            triggerMoveToPoint.triggerON = true;
        }
    }
}

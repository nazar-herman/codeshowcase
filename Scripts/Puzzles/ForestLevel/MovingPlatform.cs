﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public bool autoTrigger = false; //Whether the platform automatically move or not

    private MoveToPoint moveToPoint;

    private bool isTriggered = false;
    private Transform player;

    void Start()
    {
        moveToPoint = GetComponent<MoveToPoint>();

        if (autoTrigger && moveToPoint.triggerON == false)
        {
            moveToPoint.triggerON = true;
        }
    }

    private void Update()
    {
        if (player.GetComponent<HealthCtrlBase>() != null && player.GetComponent<HealthCtrlBase>().dead)
        {
            player.SetParent(null);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player = other.transform;

            if (!autoTrigger && !isTriggered)
            {
                moveToPoint.triggerON = true;
                isTriggered = true;
            }

            other.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            other.transform.SetParent(null);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public GameObject[] targets;

    public bool autoClose = true;

    TriggerToOpenObject[] triggerToOpenObjectScripts;
    GameObject weight;
    bool statueON;

    private void Start()
    {
        triggerToOpenObjectScripts = new TriggerToOpenObject[targets.Length];

        for (int i = 0; i < targets.Length; i++)
        {
            triggerToOpenObjectScripts[i] = targets[i].GetComponent<TriggerToOpenObject>();
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (!statueON)
        {
            weight = other.gameObject;
        }

        if (other.gameObject.GetComponent<CharacterControl>() == null)
        {
            statueON = true;
        }

        foreach (TriggerToOpenObject targetToOpen in triggerToOpenObjectScripts)
        {
            targetToOpen.triggerON = true;
        }
    }

    void Update()
    {
        if(weight == null && autoClose)
        {
            foreach (TriggerToOpenObject targetMoveToPoint in triggerToOpenObjectScripts)
            {
                targetMoveToPoint.triggerON = false;
            }

            statueON = false;
        }
    }
    private void OnCollisionExit(Collision other)
    {
        if (!statueON)
        {
            weight = null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thorn : MonoBehaviour
{
    public int damage = 1;
    public float rehitTime = 2f;

    private GameObject player;
    private bool takingHit = false;
    private bool isLoaded = false;

    private void Update()
    {
        if (takingHit && !isLoaded)
        {
            StartCoroutine("TakingDamage");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            player = other.gameObject;
            takingHit = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>())
        {
            takingHit = false;
        }
    }

    IEnumerator TakingDamage()
    {
        isLoaded = true;

        player.gameObject.GetComponent<HealthCtrlBase>().TakeHit(damage, LightColor.White);

        yield return new WaitForSeconds(rehitTime);
        isLoaded = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FenceDestroy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
    }
}

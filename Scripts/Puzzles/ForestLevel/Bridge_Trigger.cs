﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge_Trigger : MonoBehaviour
{
    public GameObject objectToDestroy;

    bool isTriggered = false;
    
    private void Update()
    {
        //Destroy the object to put down the bridge
        if (objectToDestroy)
        {
            if (!objectToDestroy.activeSelf && !isTriggered)
            {
                gameObject.GetComponent<Rigidbody>().isKinematic = false;
                isTriggered = true;
            }
        }
        else
        {
            Debug.LogError("ObjectToDestory not found!");
        }
    }
}

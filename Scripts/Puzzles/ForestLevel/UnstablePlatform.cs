﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnstablePlatform : MonoBehaviour
{
    public float disappearDelay = 1f;
    public float respawnDelay = 4f;

    private Collider col;
    private MeshRenderer mesh;
    private bool playerON = false;
    private bool respawn = false;

    private void Start()
    {
        col = GetComponent<Collider>();
        mesh = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        StartCoroutine("ChangeStatus");
    }

    IEnumerator ChangeStatus()
    {
        if (playerON)
        {
            yield return new WaitForSeconds(disappearDelay);

            mesh.enabled = false;
            col.enabled = false;
            playerON = false;
            respawn = true;
        }
        else if (respawn)
        {
            yield return new WaitForSeconds(respawnDelay);

            col.enabled = true;
            mesh.enabled = true;
            respawn = false;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        playerON = true;
    }
}

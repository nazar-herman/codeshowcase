﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsPlatform : IsTrigger
{
    AudioSource a_source;

    public AudioClip pressedSFX;
    public AudioClip unpressedSFX;

    public GameObject pressedByObj;
	// Use this for initialization
	void Start () {
        a_source = GetComponent<AudioSource>();
	}
    private void LateUpdate()
    {
        if(pressedByObj == null)
        {
            triggered = false;
        }
    }

    // Update is called once per frame

    
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<CharacterControl>() != null)
        {
            if(pressedSFX != null)
            {
                a_source.PlayOneShot(pressedSFX);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(!triggered && (other.GetComponent<CharacterControl>() != null || other.GetComponent<StatueControl>() != null))
        {
            triggered = true;
            pressedByObj = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(triggered && pressedByObj == other.gameObject)
        {
            if (unpressedSFX != null)
            {
                a_source.PlayOneShot(unpressedSFX);
            }
            triggered = false;
            pressedByObj = null;
        }
    }

}

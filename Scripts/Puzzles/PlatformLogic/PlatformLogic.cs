﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum PlatformMode
{
    OpenAndClose,
    PermanentOpen,
    PermanentClose,
    Toogle
}

public class PlatformLogic : PuzzleLogic {
    public PlatformMode mode;
    public List<IsTrigger> platforms;
    public List<IsDoor> doors;
    public bool pressed;
    bool allPressed;
    bool stop;
    // Use this for initialization
    void Start () {
		if(mode == PlatformMode.Toogle)
        {
            platforms.RemoveRange(1, platforms.Count - 1);
        }
	}

    public override void Update()
    {
        if(stop)
        {
            return;
        }
        allPressed = true;
        foreach(IsTrigger platform in platforms)
        {
            if(!platform.triggered)
            {
                allPressed = false;
                break;
            }
        }
        if(!pressed && allPressed)
        {
            Execute();
        }
        if(pressed && !allPressed)
        {
            Execute();
        }
    }

    public override void Execute()
    {
        base.Execute();
        pressed = allPressed;
        switch(mode)
        {
            case PlatformMode.OpenAndClose:
                {
                    Open();
                    break;
                }
            case PlatformMode.PermanentOpen:
                {
                    PermanentOpen();
                    break;
                }
            case PlatformMode.PermanentClose:
                {
                    PermanentClose();
                    break;
                }
            case PlatformMode.Toogle:
                {
                    Toogle();
                    break;
                }
        }
    }

    void Open()
    {
        if(pressed)
        {
            foreach(IsDoor door in doors)
            {
                door.Open();
            }
        }
        else
        {
            foreach (IsDoor door in doors)
            {
                door.Close();
            }
        }
    }

    void PermanentOpen()
    {
        if (pressed)
        {
            foreach (IsDoor door in doors)
            {
                door.Open();
            }
            stop = true;
        }
    }
    void PermanentClose()
    {
        if (pressed)
        {
            foreach (IsDoor door in doors)
            {
                door.Close();
            }
            stop = true;
        }
    }

    void Toogle()
    {
        if (pressed)
        {
            foreach (IsDoor door in doors)
            {
                if(door.isOpen)
                {
                    door.Close();
                }
                else
                {
                    door.Open();
                }
            }
        }
    }
}

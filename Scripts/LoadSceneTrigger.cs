﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneTrigger : MonoBehaviour
{
    public string sceneToLoad;
    public Vector3 newPosition;

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<CharacterControl>() != null)
        {
            Scene scene = SceneManager.GetSceneByName("GLOBAL");
            SceneManager.MoveGameObjectToScene(other.gameObject, scene);
            Managers.Instance.Level.BeginSceneTranstition(sceneToLoad, newPosition);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public GameObject shieldParticle;
    public GameObject breakParticle;
    public LightColor color;
    public int health;
    public bool active;

    HealthCtrlBase healthCtrl;

    void Awake()
    {
        healthCtrl = GetComponent<HealthCtrlBase>();
    }

    void Update()
    {
        if(active)
        {
            if(shieldParticle != null)
            {
                shieldParticle.SetActive(true);
            }
            healthCtrl.invincible = true;

            if(health <=0)
            {
                Break();
            }
        }
    }

    public void Break()
    {
        healthCtrl.invincible = false;
        shieldParticle.SetActive(false);
        GameObject brPr = Instantiate(breakParticle,transform.position,transform.rotation,transform);
        Destroy(this);
    }

    void OnTriggerEnter(Collider other)
    {
        IsDamaging sword = other.GetComponent<IsDamaging>();
        if(sword != null)
        {
            if(sword.color == color)
            {
                health -= sword.damage;
            }
        }
    }
}

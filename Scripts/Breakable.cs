﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    public LightColor colorRequired;
    public int damageRequired;
    public int currentDamage;

    public GameObject[] containedObjects;

    void Start()
    {
        if (containedObjects.Length > 0)
        {
            LockContained();
        }
    }

    public void ApplyDamage(int damage)
    {
        currentDamage += damage;
        if(currentDamage >= damageRequired)
        {
            Break();
        }
    }

    void Break()
    {
        if (containedObjects.Length > 0)
        {
            UnlockContained();
        }

        Destroy(this.gameObject);
    }

    void LockContained()
    {
        foreach(GameObject  obj in containedObjects)
        {
            Collider[] colliders = obj.GetComponentsInChildren<Collider>();
            foreach(Collider col in colliders)
            {
                col.enabled = false;
            }
        }
    }

    void UnlockContained()
    {
        foreach (GameObject obj in containedObjects)
        {
            Collider[] colliders = obj.GetComponentsInChildren<Collider>();
            foreach (Collider col in colliders)
            {
                col.enabled = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Buttons
{
    public enum Button
    {
        A,
        B,
        X,
        Y,
        DPadX,
        DPadY,
        Start,
        RT,
        LT,
        RB,
        LB,
        RS
    }

    public static string A = "Xbox-A";
    public static string B = "Xbox-B";
    public static string X = "Xbox-X";
    public static string Y = "Xbox-Y";
    public static string DPadX = "DPadX";
    public static string DPadY = "DPadY";
    public static string Start = "Xbox-ST";
    public static string RT = "Xbox-RT";
    public static string LT = "Xbox-LT";
    public static string RB = "Xbox-RB";
    public static string LB = "Xbox-LB";
    public static string RS = "Xbox-RS";

    public static string[] buttons = new string[] { A, B, X, Y, DPadX, DPadY, Start, RT, LT, RB, LB, RS };

    public static string[] buttonImgs = new string[] { "Xbox A" };
}

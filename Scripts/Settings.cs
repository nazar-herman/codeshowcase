﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Settings
{
    public static float masterVol = 85;
    public static float musicVol = 100;
    public static float sfxVol = 100;
    public static float ambientVol = 100;

    public static bool fullscreen = true;
    public static float brightness = 1;
}

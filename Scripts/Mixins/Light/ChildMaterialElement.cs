﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildMaterialElement : MonoBehaviour {
    public Material defaultMat;
    public List<Material> materials;
    LightController lightController;
    Renderer renderer;
	// Use this for initialization
	void Start () {
        lightController = GetComponentInParent<LightController>();
        renderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(lightController != null)
        {
            renderer.material = materials[(int)lightController.lightColor];
        }
        else
        {
            renderer.material = defaultMat;
        }
	}
}

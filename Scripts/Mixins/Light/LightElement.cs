﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightElement : MonoBehaviour {
    public Material defaultMat;
    public List<Material> materials;
    LightData lightData;
    Renderer renderer;
    // Use this for initialization
    void Start()
    {
        lightData = GetComponentInParent<LightData>();
        renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (lightData != null)
        {
            renderer.material = materials[(int)lightData.color];
        }
        else
        {
            renderer.material = defaultMat;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildLightElement : MonoBehaviour {
    public List<Color> colors;
    LightController lightCtrl;
    Light light;
	// Use this for initialization
	void Start () {
        lightCtrl = GetComponentInParent<LightController>();
        light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        if (colors.Count > (int)lightCtrl.lightColor)
        {
            light.color = colors[(int)lightCtrl.lightColor];
        }
	}
}

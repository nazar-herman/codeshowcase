﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum LightColor
{
    White,
    Red,
    Yellow,
    Blue
}

public class LightController : MonoBehaviour
{
    [HideInInspector()]
    public IntData lightPool;
    public LightColor lightColor;
    public int maxLight;
    public bool HidePools;
    int currentIndex;
    float timer;
    public bool[] unlocks = { true, false, false, false };

    public float LightPercent
    {
        get
        {
            return (float)lightPool.Data / maxLight;
        }
    }

    void Start()
    {
        IntData[] datas = GetComponents<IntData>();
        foreach (IntData data in datas)
        {
            if (data.Name == "Light")
            {
                lightPool = data;
            }
        }
        if (lightPool != null)
        {
            lightColor = LightColor.White;
        }
    }

    void Update()
    {
        if (lightPool.Data > maxLight)
        {
            lightPool.Data = maxLight;
        }

        if(lightPool.Data < maxLight)
        {
            Regenerate();
        }
    }

    public void SelectNextLight()
    {
        LightColor tempColor = lightColor;
        tempColor = NextColor(tempColor);
        while (!unlocks[(int)tempColor])
        {
            tempColor = NextColor(tempColor);
            if(tempColor == lightColor)
            {
                break;
            }
        }
        if(unlocks[(int)tempColor])
        {
            lightColor = tempColor;
        }
    }

    public void SelectPreviousLight()
    {
        LightColor tempColor = lightColor;
        tempColor = PreviousColor(tempColor);
        while (!unlocks[(int)tempColor])
        {
            tempColor = PreviousColor(tempColor);
            if (tempColor == lightColor)
            {
                break;
            }
        }
        if (unlocks[(int)tempColor])
        {
            lightColor = tempColor;
        }
    }

    LightColor NextColor(LightColor color)
    {
        switch(color)
        {
            case LightColor.White:
                return LightColor.Blue;
            case LightColor.Blue:
                return LightColor.Red;
            case LightColor.Red:
                return LightColor.Yellow;
            case LightColor.Yellow:
                return LightColor.White;
        }

        return 0;
    }

    LightColor PreviousColor(LightColor color)
    {
        switch (color)
        {
            case LightColor.White:
                return LightColor.Yellow;
            case LightColor.Yellow:
                return LightColor.Red;
            case LightColor.Red:
                return LightColor.Blue;
            case LightColor.Blue:
                return LightColor.White;
        }

        return 0;
    }

    public void SelectLight(int index)
    {
        if (index < 0 || index > (int)LightColor.Blue)
        {
            return;
        }
        lightColor = (LightColor)index;
    }

    public void Drain(int amount)
    {
        lightPool.Data -= amount;
        if (lightPool.Data < 0)
        {
            lightPool.Data = 0;
        }
    }

    public void Regenerate()
    {
        if(timer > 1.0f)
        {
            lightPool.Data += 5;
            timer = 0;
        }
        timer += Time.deltaTime;
    }
}

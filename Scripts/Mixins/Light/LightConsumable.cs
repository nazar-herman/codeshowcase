﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LightData))]
public class LightConsumable : IsConsumable {

    LightData lightData;

    public void Start()
    {
        lightData = GetComponent<LightData>();
    }
    public override void Consume()
    {
        if(recipient != null)
        {
            IntData[] light = recipient.GetComponents<IntData>();
            foreach(IntData data in light)
            {
                if(data.Name == "Light")
                {
                    data.Data += lightData.Data;
                    Destroy(this.gameObject);
                    break;
                }
            }
        }
    }
}

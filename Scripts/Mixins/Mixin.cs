﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mixin : MonoBehaviour {

    public string Name;
    protected GameObject recipient;

    public void SetRecipient(GameObject go)
    {
        recipient = go;
    }
}

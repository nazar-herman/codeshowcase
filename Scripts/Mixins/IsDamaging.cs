﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsDamaging : Mixin {
    public LightColor color;
    public int damage;
    public float knockbackForce;
    GameObject owner;
	// Use this for initialization
	void Start () {
        CharacterControl chCtrl = GetComponentInParent<CharacterControl>();
        if(chCtrl != null)
        {
            owner = chCtrl.gameObject;
        }
        EnemyBehaviourBase enemy = GetComponentInParent<EnemyBehaviourBase>();
        if (enemy != null)
        {
            owner = enemy.gameObject;
        }
    }
	
	// Update is called once per frame
	public void Damage ()
    {
        if(!gameObject.activeSelf)
        {
            return;
        }
		if(recipient != null)
        {
            HealthCtrlBase healthCtrl = recipient.GetComponent<HealthCtrlBase>();
            if(healthCtrl != null)
            {
                healthCtrl.TakeHit(damage, color);
                Vector3 dir;
                if (owner != null)
                {
                    dir = healthCtrl.transform.position - owner.transform.position;
                }
                else
                {
                    dir = -healthCtrl.transform.forward; 
                }
                healthCtrl.AddKnockback(knockbackForce, dir);
            }

            Breakable breakable = recipient.GetComponent<Breakable>();
            if(breakable != null)
            {
                if (color == breakable.colorRequired)
                {
                    breakable.ApplyDamage(damage);
                }
            }

            IsChargeable chargeable = recipient.GetComponent<IsChargeable>();
            if(chargeable != null && chargeable.type == color)
            {
                chargeable.Charge(damage);
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTouchable : MonoBehaviour {
    public string onTouch;
    private void OnTriggerEnter(Collider other)
    {
        IsConsumable cons = GetComponent<IsConsumable>();
        if(cons != null)
        {
            cons.SetRecipient(other.gameObject);
        }

        IsDamaging dm = GetComponent<IsDamaging>();
        if(dm != null)
        {
            dm.SetRecipient(other.gameObject);
        }

        SendMessage(onTouch);
    }

    private void OnCollisionEnter(Collision other)
    {
        IsConsumable cons = GetComponent<IsConsumable>();
        if (cons != null)
        {
            cons.SetRecipient(other.gameObject);
        }
        
        IsDamaging dm = GetComponent<IsDamaging>();
        if (dm != null)
        {
            dm.SetRecipient(other.gameObject);
        }

        SendMessage(onTouch);
    }
}

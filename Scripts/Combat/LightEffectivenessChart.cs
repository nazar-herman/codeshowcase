﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuple<T1, T2>
{
    public T1 First { get; private set; }
    public T2 Second { get; private set; }
    internal Tuple(T1 first, T2 second)
    {
        First = first;
        Second = second;
    }
}


public static class LightEffectivenessChart {

    public static Dictionary<KeyValuePair<LightColor, LightColor>, float> lightChart = new Dictionary<KeyValuePair<LightColor, LightColor>, float>()
    {
        {new KeyValuePair<LightColor, LightColor>(LightColor.Blue, LightColor.Red), 1.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Red, LightColor.Yellow), 1.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Yellow, LightColor.Blue), 1.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Red, LightColor.Blue), 0.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Yellow, LightColor.Red), 0.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Blue, LightColor.Yellow), 0.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Blue, LightColor.Blue), 1.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Red, LightColor.Red), 1.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Yellow, LightColor.Yellow), 1.0f },
    };


    public static Dictionary<KeyValuePair<LightColor, LightColor>, float> crystalChart = new Dictionary<KeyValuePair<LightColor, LightColor>, float>()
    {
        {new KeyValuePair<LightColor, LightColor>(LightColor.Blue, LightColor.Red), 1.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Red, LightColor.Yellow), 1.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Yellow, LightColor.Blue), 1.5f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Blue, LightColor.Blue), 1.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Red, LightColor.Red), 1.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Yellow, LightColor.Yellow), 1.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Red, LightColor.Blue), 0.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Yellow, LightColor.Red), 0.0f },
        {new KeyValuePair<LightColor, LightColor>(LightColor.Blue, LightColor.Yellow), 0.0f },
    };

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targetable : MonoBehaviour
{
    public enum TargetType
    {
        Enemy,
        Object
    }

    public void DeathCleanup()
    {
        if(targetedBy != null)
        {
            if (targetedBy.LockedTarget != null)
            {
                targetedBy.LockedTarget = null;
            }
            else if (targetedBy.TargetFacing != null)
            {
                targetedBy.TargetFacing = null;
            }
        }
    }

    public TargetControl targetedBy;
    public bool targeted = false;
}

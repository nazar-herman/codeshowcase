﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TargetControl : MonoBehaviour
{
    public float visionAngle;
    public float radius;
    public LayerMask layerMask;


    [SerializeField]
    private Targetable _targetFacing;
    [SerializeField]
    private Targetable _lockedTarget;
    private TargetIcon targetIcon;
    private LightController lightController;
    private List<Collider> targetsInRange;

    public Targetable TargetFacing
    {
        get { return _targetFacing; }
        set
        {

            if (value == null && !_lockedTarget)
            {
                if (_targetFacing)
                {
                    _targetFacing.targeted = false;
                    _targetFacing.targetedBy = null;
                }
                if (targetIcon.gameObject.activeInHierarchy)
                {
                    targetIcon.target = null;
                    targetIcon.head = null;
                    targetIcon.gameObject.SetActive(false);
                }
            }

            _targetFacing = value;

            if (_targetFacing != null)
            {
                _targetFacing.targeted = true;
                _targetFacing.targetedBy = this;
            }
        }
    }
    public Targetable LockedTarget
    {
        get { return _lockedTarget; }
        set
        {

            if (value == null)
            {
                if (_lockedTarget)
                {
                    _lockedTarget.targeted = false;
                    _lockedTarget.targetedBy = null;
                }

                targetIcon.targetImage.color = Color.white;
                targetIcon.gameObject.SetActive(false);
            }

            _lockedTarget = value;

            if (_lockedTarget != null)
            {
                _lockedTarget.targeted = true;
                _lockedTarget.targetedBy = this;
            }
        }
    }

    public void Awake()
    {
        targetIcon = FindObjectOfType<TargetIcon>();
        targetIcon.gameObject.SetActive(false);
    }

    public void Start()
    {
        lightController = GetComponent<LightController>();
    }

    public void LateUpdate()
    {

        targetsInRange = Physics.OverlapSphere(transform.position, radius, layerMask).ToList();
        

        if (!LockedTarget)
        {
            GetTargetFacing();
        }

        HandleTargetIcon();

        if (Input.GetButtonDown(Buttons.RS))
        {
            Lock();
        }
    }

    public void GetTargetFacing()
    {
        TargetFacing = null;

        List<Collider> coneTargets = new List<Collider>();

        foreach (Collider target in targetsInRange)
        {
            Vector3 targetDir = target.transform.position - transform.position;
            float angle = Vector3.Angle(targetDir,transform.forward);

            if(angle < visionAngle)
            {
                coneTargets.Add(target);
            }
        }

        float closest = 0.0f;

        foreach (Collider target in coneTargets)
        {
            if(TargetFacing == null)
            {
                TargetFacing = target.GetComponent<Targetable>();
                closest = Vector3.Distance(transform.position, target.transform.position);
            }
            else
            {
                float dist = Vector3.Distance(transform.position, target.transform.position);

                if(dist < closest)
                {
                    closest = dist;
                    TargetFacing = target.GetComponent<Targetable>();
                }
            }
        }
    }
    
    public void HandleTargetIcon()
    {
        if(!LockedTarget)
        {
            if(TargetFacing)
            {
                if(!targetIcon.gameObject.activeInHierarchy)
                {
                    targetIcon.gameObject.SetActive(true);
                    targetIcon.SetTarget(TargetFacing.gameObject);
                    if (targetIcon.head != null)
                    {
                        targetIcon.transform.position = targetIcon.head.transform.position + Vector3.up;
                    }
                    else
                    {
                        BoxCollider col = targetIcon.target.GetComponent<BoxCollider>();
                        targetIcon.transform.position = new Vector3(col.transform.position.x, col.bounds.center.y + col.bounds.extents.y, col.transform.position.z) + (Vector3.up* 0.5f);
                    }
                }
            }
        }

        if (LockedTarget)
        {
            if (!targetsInRange.Contains(LockedTarget.GetComponent<Collider>()))
            {
                LockedTarget = null;
            }
        }
    }

    public void Lock()
    {
        if (TargetFacing != null && !LockedTarget)
        {
            LockedTarget = TargetFacing;
            targetIcon.targetImage.color = Color.red;
            
            TargetFacing = null;
        }
        else if(LockedTarget)
        {
            targetIcon.targetImage.color = Color.white;
            LockedTarget = null;
        }
    }


    public void SwitchTargets()
    {
    
    }

}

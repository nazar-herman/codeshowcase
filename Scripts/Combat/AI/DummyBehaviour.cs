﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyBehaviour : EnemyBehaviourBase {
    public int maxHits = 4;
    [HideInInspector]
    public int hits;
    bool removed;
	// Use this for initialization
	void Start () {
		
	}

    public override void Update()
    {
        if(hits >= maxHits && !removed && areaLogic != null)
        {
            removed = true;
            areaLogic.RemoveEnemy(gameObject);
        }
    }
}

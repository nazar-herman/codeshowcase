﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
[NodeInfo(category = "Mushroom/Action/")]
public class HideBehaviour : ActionNode {

    public MushroomBehaviour enemy;

    public override Status Update()
    {
        if (!enemy.hidden)
        {
            enemy.SetHideAnim(true);
            enemy.Stop();
        }
        return Status.Success;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine;
[NodeInfo(category = "Mushroom/Condition/")]
public class IsCloneCondition : BehaviourMachine.ConditionNode
{
    public MushroomBehaviour enemy;

    public override Status Update()
    {
        if (enemy.isClone || enemy.alreadyCloned)
        {
            return Status.Failure;
        }
        return Status.Success;
    }

}

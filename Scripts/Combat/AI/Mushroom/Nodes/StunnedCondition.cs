﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Mushroom/Condition/")]

public class StunnedCondition : ConditionNode
{
    public EnemyBehaviourBase enemy;
    public override Status Update()
    {
        if (enemy._stunned)
        {
            return Status.Success;
        }
        return Status.Failure;
    }
}


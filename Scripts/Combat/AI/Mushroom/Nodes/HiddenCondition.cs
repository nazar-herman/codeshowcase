﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Mushroom/Condition/")]


public class HiddenCondition : ConditionNode {
    public MushroomBehaviour mushroom;
    public bool hiddenValue;
    public override Status Update()
    {
        if(mushroom.hidden == hiddenValue)
        {
            return Status.Success;
        }
        return Status.Failure;
    }
}

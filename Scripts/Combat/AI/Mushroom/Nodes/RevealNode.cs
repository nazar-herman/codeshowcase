﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
[NodeInfo(category = ("Mushroom/Action/"))]
public class RevealNode : ActionNode
{

    public MushroomBehaviour enemy;

    public override Status Update()
    {
        if (enemy.hidden)
        {
            enemy.SetHideAnim(false);
            enemy.Stop();
        }
        return Status.Success;
    }
}


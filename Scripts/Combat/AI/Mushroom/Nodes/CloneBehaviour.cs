﻿using System;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine;
[NodeInfo(category = "Mushroom/Action/")]
public class CloneBehaviour : ActionNode
{
    public MushroomBehaviour enemy;
    public override Status Update()
    {
        if(enemy.alreadyCloned || enemy.isClone)
        {
            return Status.Failure;
        }
        if(enemy.cloning)
        {
            return Status.Running;
        }
        enemy.Clone();
        return Status.Success;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomBehaviour : EnemyBehaviourBase {
    public TargetFinder targetFinder;
    public float rechargeTime;
    public float fleeTime = 2;
    public bool isClone;
    public bool alreadyCloned;
    public bool cloning;
    public bool hidden = false;
    public GameObject clonePrefab;
    float timer;
    private void Start()
    {
        ResetHiddenSMB smb = anim.GetBehaviour<ResetHiddenSMB>();
        smb.enemy = this;
    }
    public override void Attack()
    {
        agent.velocity =  Vector3.zero;
        if (_track)
        {
            Track();
        }

        if (anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", false);
        }

        if (lastAttackTime > 0.0f)
        {
            if (Time.time > (lastAttackTime + attackDelay))
            {
                anim.SetTrigger("Attack");
                lastAttackTime = Time.time;
                stunnedTimer = stunnedDuration;
            }
        }
        else
        {
            anim.SetTrigger("Attack");
            lastAttackTime = Time.time;
            stunnedTimer = stunnedDuration;
        }
    }

    public override void Seek()
    {
        if (!anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", true);
        }
        agent.SetDestination(target.position);
        agent.isStopped = false;
        agent.speed = speed;
        Move();
    }
    public override void Flee()
    {
        if (!anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", true);
        }
        agent.destination =  transform.position + (transform.position - target.position).normalized;
        agent.isStopped = false;
        agent.speed = fleeSpeed;
        Move();
    }
    public override void Idle()
    {
        anim.SetBool("Walk", false);
        anim.SetBool("Hide", false);

        Stop();
    }


    public override void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void Clone()
    {
        if (!alreadyCloned && !isClone)
        {
            GameObject clone = Instantiate(clonePrefab, transform.position + transform.right * 2, transform.rotation, null);
            //areaLogic.enemies.Add(clone);
            MushroomBehaviour mushroom = clone.GetComponent<MushroomBehaviour>();
            mushroom.isClone = true;
            mushroom.anim.Play("Attack");
            alreadyCloned = true;
            isClone = true;
            timer = rechargeTime;
            Reset();
        }
    }

    public override void Stop()
    {
        agent.isStopped = true;
        anim.SetBool("Walk", false);
    }
    public void CloneMessage()
    {
        
        GameObject clone = Instantiate(clonePrefab, transform.position + transform.right * 2, transform.rotation, null);
        MushroomBehaviour mushroom = clone.GetComponent<MushroomBehaviour>();
        mushroom.isClone = true;
        Reset();

    }

    public void Retreat()
    {

    }

    public void Reveal()
    {
        hidden = false;
    }
    public void Hide()
    {
        hidden = true;
    }
    public void SetHideAnim(bool value)
    {
        anim.SetBool("Hide", value);
    }

    public override void Update()
    {
        base.Update();
        if (alreadyCloned && timer <= 0.0f)
        {
            alreadyCloned = false;
        }
        if (timer >= 0.0f)
        {
            timer -= Time.deltaTime;
        }
        if (stunnedTimer >= 0.0f)
        {
            _stunned = true;
            stunnedTimer -= Time.deltaTime;
        }
        else
        {
            _stunned = false;
        }
        if(healthCtrl != null)
        {
            healthCtrl.invincible = hidden;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class StructureBuilder : MonoBehaviour {
    public List<EnemySlot> enemySlots;
    public List<SquadFormation> quadFormations;
    public List<SquadFormation> tripleFormations;
    public List<SquadFormation> doubleFormations;

    List<EnemySlot> availableSlots;
	// Use this for initialization
	void Start () {
        enemySlots = new List<EnemySlot>(GetComponentsInChildren<EnemySlot>());
        List<SquadFormation> formations = new List<SquadFormation>(GetComponentsInChildren<SquadFormation>());
        quadFormations = formations.FindAll(f => f.formation.Count == 4).ToList();
        tripleFormations = formations.FindAll(f => f.formation.Count == 3).ToList();
        doubleFormations = formations.FindAll(f => f.formation.Count == 2).ToList();


    }

    // Update is called once per frame
    void Update () {
		
	}

    public SquadFormation GetFormation(int enemiesAmount)
    {
        enemiesAmount = Mathf.Clamp(enemiesAmount, enemiesAmount, 4);
        if(enemiesAmount == 4)
        {
            List<SquadFormation> freeQuadFormations = quadFormations.FindAll(x => !x.blocked);
            if(freeQuadFormations.Count > 0)
            {
                return freeQuadFormations.Random();
            }
            else
            {
                enemiesAmount--;
            }
        }

        if(enemiesAmount == 3)
        {
            List<SquadFormation> freeTripleFormations = tripleFormations.FindAll(x => !x.blocked);
            if (tripleFormations.Count > 0)
            {
                return freeTripleFormations.Random();
            }
            else
            {
                enemiesAmount--;
            }
        }

        if (enemiesAmount == 2)
        {
            List<SquadFormation> freeDoubleFormations = doubleFormations.FindAll(x => !x.blocked);
            if (tripleFormations.Count > 0)
            {
                return freeDoubleFormations.Random();
            }
        }
        return null;
    }


}

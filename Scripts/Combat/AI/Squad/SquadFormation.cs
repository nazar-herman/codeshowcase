﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class SquadFormation : MonoBehaviour {
    public List<EnemySlot> formation;
    public bool blocked;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(formation.Any(x=> x.blocked))
        {
            blocked = true;
        }
        else
        {
            blocked = false;
        }
	}
}

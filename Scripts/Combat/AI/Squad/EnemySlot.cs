﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlot : MonoBehaviour
{
    public EnemySlot parentNode;
    public EnemySlot childNode;
    public EnemySlot Next;
    public EnemySlot Previous;
    public GameObject owner;
    public float radius;
    public float outerRadius;
    public bool taken = false;
    public bool blocked = false;
    GameObject blockedBy;
    RaycastHit hit;
    // Use this for initialization
    void Start()
    {
        EnemySlot[] slots = GetComponentsInChildren<EnemySlot>();
        if(slots.Length > 1)
        {
            childNode = slots[1];
            childNode.owner = owner;
            childNode.parentNode = this;
            Vector3 direction = (transform.position - Vector3.up - owner.transform.position).normalized;
            float innerRadius = Vector3.Distance(transform.position, owner.transform.position);
            transform.position = transform.position + direction * (radius - innerRadius);
        }
    }
    private void Update()
    {
        CheckBlock();
        if(childNode != null)
        {

            PlaceChildNode();

        }
    }
    void PlaceChildNode()
    {
        Vector3 direction = (transform.position - Vector3.up - owner.transform.position).normalized;
        float innerRadius = Vector3.Distance(transform.position, owner.transform.position);
        Debug.DrawRay(transform.position, direction * (outerRadius - innerRadius), Color.red);
        if(!Physics.Raycast(transform.position, direction* (outerRadius - innerRadius), out hit, (outerRadius - innerRadius)) || hit.collider.gameObject.GetComponent<isWall>() == null)
        {
            childNode.transform.position = transform.position + direction * (outerRadius - innerRadius);
        }
        else
        {
            Vector3 position = hit.point;
            position.y = transform.position.y;
            position -= direction/2;
            childNode.transform.position = position;
        }
    }
    void CheckBlock()
    {
        if (blockedBy == null)
        {
            if (Physics.Raycast(transform.position, owner.transform.position - transform.position, out hit))
            {
                if (hit.collider != null && hit.collider.gameObject.GetComponent<isWall>() != null)
                {
                    blocked = true;
                    if(childNode != null)
                    {
                        childNode.enabled = false;
                    }

                }
                else
                {
                    blocked = false;
                    if (childNode != null)
                    {
                        childNode.enabled = true;
                    }
                }
            }
        }

    }

    // Update is called once per frame
    public void OnTriggerEnter(Collider other)
    {
        isWall isW = other.GetComponent<isWall>();
        if (isW != null)
        {
            blocked = true;
            blockedBy = other.gameObject;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        isWall isW = other.GetComponent<isWall>();
        if (blockedBy != null && other.gameObject == blockedBy)
        {
            blocked = false;
            blockedBy = null;
        }
    }
}

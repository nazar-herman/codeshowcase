﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSlotTarget : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        EnemyBehaviourBase enemy = other.GetComponent<EnemyBehaviourBase>();
        if (enemy != null && enemy.enemySlotTarget != null)
        {
            enemy.chaseSlot = false;
            EnemySlot slot = enemy.enemySlotTarget.GetComponent<EnemySlot>();
            if (slot != null)
            {
                slot.taken = false;
            }
            enemy.enemySlotTarget = null;
        }
    }
}

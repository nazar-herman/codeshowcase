﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class SquadController : MonoBehaviour
{
    public static Random rnd = new Random();
    public List<EnemyBehaviourBase> members;
    public List<EnemyBehaviourBase> nearTargetMembers;
    public List<EnemySlot> outerSlots;
    public StructureBuilder formationBuilder;
    public int simultaniousAttackers;
    public int attackingNow;
    public SquadFormation currentFormation;
    public GameObject currentTarget;
    public float outerCircleRadius;
    bool squadAlerted;
    List<TargetFinder> targetFinders = new List<TargetFinder>();
    // Use this for initialization
    void Start()
    {
        foreach (EnemyBehaviourBase member in members)
        {
            member.sqCtrl = this;
            targetFinders.Add(member.GetComponentInChildren<TargetFinder>());
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(currentFormation != null && currentFormation.blocked)
        {
            GetNewFormation(currentTarget);
        }
    }

    public void AlertSquad(GameObject target)
    {
        CharacterControl chCtrl = target.GetComponent<CharacterControl>();
        if (chCtrl == null)
        {
            return;
        }
        if(currentFormation != null)
        {
            return;
        }
        members.Sort((a, b) => Vector3.Distance(a.transform.position, target.transform.position).CompareTo(Vector3.Distance(b.transform.position, target.transform.position)));
        currentTarget = target;
        GetNewFormation(currentTarget);
    }

    public void GetNewFormation(GameObject target)
    {
        currentFormation = formationBuilder.GetFormation(members.Count);

        foreach (EnemyBehaviourBase member in members)
        {
            List<EnemySlot> freeSlots = currentFormation.formation.FindAll(x => !x.blocked && !x.taken).ToList();
            if (freeSlots.Count != 0)
            {
                EnemySlot closestSlot = freeSlots[0];
                float smallestDistance = Vector3.Distance(member.transform.position, closestSlot.transform.position);
                foreach (EnemySlot slot in freeSlots)
                {
                    if (Vector3.Distance(member.transform.position, slot.transform.position) < smallestDistance)
                    {
                        closestSlot = slot;
                        smallestDistance = Vector3.Distance(member.transform.position, closestSlot.transform.position);
                    }
                }

                member.SetTarget(target.transform);
                member.chaseSlot = true;
                member.SetEnemySlotTarget(closestSlot);
                member.outerRingSlot = closestSlot.childNode;
                closestSlot.taken = true;
            }
        }
    }
    public void LostTarget(GameObject target)
    {
        foreach (TargetFinder finder in targetFinders)
        {
            if (finder.target == null && finder.enemyCtrl.target != null)
            {
                finder.enemyCtrl.SetTarget(null);
            }
        }
        if(members.All(x=>x.target == null))
        {
            currentFormation = null;
        }
    }

}

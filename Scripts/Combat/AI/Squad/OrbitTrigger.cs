﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitTrigger : MonoBehaviour
{
    public List<EnemySlot> outerRingSlots;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public void OnTriggerEnter(Collider other)
    {
        EnemyBehaviourBase enemy = other.GetComponent<EnemyBehaviourBase>();
        if (enemy != null && enemy.enemySlotTarget != null)
        {
            EnemySlot closestSlot = outerRingSlots[0];
            float smallestDistance = Vector3.Distance(enemy.transform.position, closestSlot.transform.position);
            foreach (EnemySlot slot in outerRingSlots)
            {
                if (Vector3.Distance(enemy.transform.position, slot.transform.position) < smallestDistance)
                {
                    closestSlot = slot;
                    smallestDistance = Vector3.Distance(enemy.transform.position, closestSlot.transform.position);
                }
            }
            int clockWiseSteps = 0;
            int counterClockwiseSteps = 0;
            EnemySlot tempSlot = closestSlot;
            if(closestSlot.parentNode == enemy.enemySlotTarget)
            {
                return;
            }
            while (tempSlot.parentNode != enemy.enemySlotTarget && clockWiseSteps < 10)
            {
                Debug.Log(clockWiseSteps);
                clockWiseSteps++;
                tempSlot = tempSlot.Next;
            }
            tempSlot = closestSlot;
            while (tempSlot.parentNode != enemy.enemySlotTarget && counterClockwiseSteps < 10)
            {
                Debug.Log(counterClockwiseSteps);

                counterClockwiseSteps++;
                tempSlot = tempSlot.Previous;
            }
            enemy._orbiting = true;
            enemy._clockwise = clockWiseSteps <= counterClockwiseSteps;
            enemy.outerRingSlot = closestSlot;
            if (enemy._clockwise && Vector3.Distance(closestSlot.transform.position, enemy.transform.position) < Vector3.Distance(closestSlot.Next.transform.position, enemy.transform.position))
            {
                enemy.outerRingSlot = closestSlot.Next;
            }
            if (!enemy._clockwise && Vector3.Distance(closestSlot.transform.position, enemy.transform.position) < Vector3.Distance(closestSlot.Previous.transform.position, enemy.transform.position))
            {
                enemy.outerRingSlot = closestSlot.Previous;
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        EnemyBehaviourBase enemy = other.GetComponent<EnemyBehaviourBase>();
        if (enemy != null)
        {
            enemy._clockwise = false;
            enemy._orbiting = false;
            enemy.outerRingSlot = enemy.enemySlotTarget.childNode;
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunArea : MonoBehaviour {
    public float stunDuration;
    public float minStunRadius;
    public float maxStunRadius;
    public float growthSpeed;
    public GameObject particle;
    SphereCollider stunCollider;

    float timer;
    public float growthTime;
	// Use this for initialization
	void Start () {
        IsStun stunSphere = GetComponentInChildren<IsStun>();
        if (stunSphere != null)
        {
            stunSphere.stunDuration = stunDuration;
            stunCollider = stunSphere.GetComponent<SphereCollider>();
            if (stunCollider != null)
            {
                stunCollider.radius = minStunRadius;
            }
        }
        if(particle != null)
        {
            Instantiate(particle, transform.position, transform.rotation);
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(stunCollider != null)
        {
            if((maxStunRadius - stunCollider.radius) <= 0.05f)
            {
                Destroy(gameObject);
            }
            else
            {
                timer += Time.deltaTime;

                stunCollider.radius = Mathf.Lerp(minStunRadius, maxStunRadius, timer/growthTime);
            }
        }
	}
}

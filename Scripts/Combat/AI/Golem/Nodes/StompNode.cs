﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Golem/Action/")]
public class StompNode : ActionNode {
    public GolemBehaviour golem;
    public override Status Update()
    {
        golem.Stomp();
        return Status.Success;
    }

}

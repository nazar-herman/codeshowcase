﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Golem/Condition/")]

public class ShieldedNode : ConditionNode
{
    public GolemBehaviour golemBehaviour;

    public override Status Update()
    {
        if(golemBehaviour.shielded)
        {
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }
}

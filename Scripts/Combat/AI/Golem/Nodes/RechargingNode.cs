﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Golem/Condition/")]
public class RechargingNode : ConditionNode {
    public GolemBehaviour golem;

    public override Status Update()
    {
        if (golem.recharging)
        {
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }
}

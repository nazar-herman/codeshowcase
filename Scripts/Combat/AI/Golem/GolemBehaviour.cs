﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemBehaviour : EnemyBehaviourBase
{
    [Header("Shield Settings")]
    public LightColor shieldColor;
    public int shieldHealth;
    public GameObject shieldParticle;
    public GameObject destoyedParticle;

    [Header("Stun Settings")]
    public float rechargeTime;
    public float minStunRadius;
    public float maxStunRadius;
    public float growthSpeed;
    public float growthTime;
    public float stunDuration;
    [Range(0.0f, 1.0f)]
    public float stompChance;

    public GameObject areaPrefab;

    [HideInInspector]
    public bool attackLeft = true;
    public bool shielded = false;
    [HideInInspector]
    public bool recharging = false;
    float timer;
    SphereCollider stunCollider;


    public override void Attack()
    {
        agent.velocity = Vector3.zero;
        if (_track)
        {
            Track();
        }

        if (anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", false);
        }

        if (lastAttackTime > 0.0f)
        {
            if (Time.time > (lastAttackTime + attackDelay))
            {
                if (attackLeft)
                {
                    anim.SetTrigger("Attack1");
                    attackLeft = false;
                }
                else
                {
                    anim.SetTrigger("Attack2");
                    attackLeft = true;
                }
                lastAttackTime = Time.time;
                stunnedTimer = stunnedDuration;
            }
        }
        else
        {
            if (attackLeft)
            {
                anim.SetTrigger("Attack1");
                attackLeft = false;
            }
            else
            {
                anim.SetTrigger("Attack2");
                attackLeft = true;
            }
            lastAttackTime = Time.time;
            stunnedTimer = stunnedDuration;
        }
    }

    public override void Seek()
    {
        if (!anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", true);
        }
        agent.SetDestination(target.position);
        agent.isStopped = false;
        agent.speed = speed;
        Move();
    }

    public override void Flee()
    {
        if (!anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", true);
        }
        agent.destination = transform.position + (transform.position - target.position).normalized;
        agent.isStopped = false;
        agent.speed = fleeSpeed;
        Move();
    }

    public override void Idle()
    {
        anim.SetBool("Walk", false);
        anim.SetBool("Hide", false);

        Stop();
    }

    public override void SetTarget(Transform target)
    {
        this.target = target;
    }

    public override void Stop()
    {
        agent.isStopped = true;
        anim.SetBool("Walk", false);
    }

    public override void Update()
    {
        base.Update();
        if (timer >= 0.0f)
        {
            recharging = true;
            timer -= Time.deltaTime;
        }
        else
        {
            recharging = false;
        }

        if (stunnedTimer >= 0.0f)
        {
            _stunned = true;
            stunnedTimer -= Time.deltaTime;
        }
        else
        {
            _stunned = false;
        }

    }

    public void Stomp()
    {
        anim.SetTrigger("Stomp");
        timer = rechargeTime;
    }

    public void DoStomp()
    {

        GameObject stun = Instantiate(areaPrefab, transform.position, transform.rotation);
        StunArea area = stun.GetComponent<StunArea>();
        if (area != null)
        {
            area.minStunRadius = minStunRadius;
            area.maxStunRadius = maxStunRadius;
            area.growthSpeed = growthSpeed;
            area.stunDuration = stunDuration;
            area.growthTime = growthTime;
        }

    }

    public void Shield()
    {
        if(!shielded)
        {
            Shield shield = gameObject.AddComponent<Shield>();
            shield.color = shieldColor;
            shield.health = shieldHealth;
            GameObject shieldPart = Instantiate(shieldParticle, transform.position + transform.up, shieldParticle.transform.rotation, transform);
            shield.shieldParticle = shieldPart;
            shield.breakParticle = destoyedParticle;
            shield.active = true;
            shielded = true;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine;

public enum Comparison
{
    Less,
    EqualTo,
    More
}
[NodeInfo(category = "Common/Condition/")]
public class HealthAmountCondition : BehaviourMachine.ConditionNode
{
    public HealthCtrlBase healthCtrl;
    public Comparison comparisonType;
    public int healthAmount;
    bool check;
    public override Status Update()
    {
        switch(comparisonType)
        {
            case Comparison.More:
                {
                    check = healthAmount < healthCtrl.healthData.Data;
                    break;
                }
            case Comparison.Less:
                {
                    check = healthAmount > healthCtrl.healthData.Data;
                    break;
                }
            case Comparison.EqualTo:
                {
                    check = (healthAmount == healthCtrl.healthData.Data);
                    break;
                }
        }
        if(check)
        {
            return Status.Success;
        }
        return Status.Failure;
    }
}

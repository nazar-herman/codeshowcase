﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Action/")]
public class IdleBehaviour : BehaviourMachine.ActionNode
{
    public EnemyBehaviourBase enemy;
    public override Status Update()
    {
        if(!enemy._attacking)
        {
            enemy.Idle();
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Condition/")]
public class EnemySlotTargeted : BehaviourMachine.ConditionNode
{
    public EnemyBehaviourBase enemy;
    public override Status Update()
    {
        if (enemy.enemySlotTarget != null)
        {
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Common/Condition/")]

public class FledCondition : ConditionNode {
    public EnemyBehaviourBase enemy;

    public override Status Update()
    {
        if(enemy._retreated)
        {
            return Status.Failure;
        }
        return Status.Success;
    }
}

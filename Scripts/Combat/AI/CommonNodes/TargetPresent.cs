﻿using System;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine;
[NodeInfo(category = "Common/Condition/")]
public class TargetPresent : BehaviourMachine.ConditionNode
{
    public TargetFinder targetFinder;
    public EnemyBehaviourBase enemy;
    public override Status Update()
    {
        //if(enemy.chaseSlot)
        //{
        //    return Status.Failure;
        //}
        if (targetFinder.target == null)
        {
            enemy.SetTarget(null);
            return Status.Failure;
        }
        if (enemy.target != null)
        {
            return Status.Success;
        }
        if(targetFinder.target != null)
        {
            enemy.SetTarget(targetFinder.target);
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }
}

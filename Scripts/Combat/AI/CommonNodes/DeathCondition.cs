﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Condition/")]
public class DeathCondition : BehaviourMachine.ConditionNode
{
    public HealthCtrlBase hc;
    public EnemyBehaviourBase enemy;

    public override Status Update()
    {
        if(!hc.dead)
        {
            return Status.Success;
        }
        else
        {
            enemy.Stop();
            return Status.Failure;
        }
    }
}

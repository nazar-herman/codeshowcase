﻿using System;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine;
[NodeInfo(category = "Common/Condition/")]
public class NearTargetCondition : BehaviourMachine.ConditionNode
{
    public EnemyBehaviourBase enemyBehaviour;

    public override Status Update()
    {
        if(enemyBehaviour.getDistance() <= enemyBehaviour.attackDistance)
        {
            return Status.Success;
        }
        else
        {
            enemyBehaviour._attacking = false;
            return Status.Failure;
        }
    }
}

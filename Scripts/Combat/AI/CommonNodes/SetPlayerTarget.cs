﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Action/")]

public class SetPlayerTarget : ActionNode {
    public EnemyBehaviourBase enemyBehaviour;
	// Use this for initialization

    public override Status Update()
    {
        if(enemyBehaviour._orbiting)
        {
            if(enemyBehaviour.outerRingSlot == enemyBehaviour.enemySlotTarget.childNode)
            {
                enemyBehaviour._orbiting = false;
                enemyBehaviour._clockwise = false;
            }
            else
            {
                enemyBehaviour.outerRingSlot = enemyBehaviour._clockwise ? enemyBehaviour.outerRingSlot.Next : enemyBehaviour.outerRingSlot.Previous;
            }
            return Status.Success;
        }
        enemyBehaviour.chaseSlot = false;
        if(enemyBehaviour.enemySlotTarget != null)
        {
            enemyBehaviour.enemySlotTarget.taken = false;        
        }
        enemyBehaviour.enemySlotTarget = null;
        return Status.Success;
    }

}

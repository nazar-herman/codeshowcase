﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Action/")]
public class AttackBehaviour : BehaviourMachine.ActionNode
{
    public EnemyBehaviourBase enemy;

    public override Status Update()
    {
        if(enemy._staggered)
        {
            return Status.Failure;
        }

        if (enemy.getDistance() <= enemy.attackDistance)
        {
            enemy.Attack();
            return Status.Success;
        }
        else if(enemy._attacking)
        {
            enemy.Attack();
            return Status.Running;
        }
        else
        {
            return Status.Failure;
        }
    }
}

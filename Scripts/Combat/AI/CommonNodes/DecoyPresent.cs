﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Condition/")]

public class DecoyPresent : ConditionNode {

    public TargetFinder finder;
    public EnemyBehaviourBase enemy;

    public override Status Update()
    {
        if(finder.decoyTarget != null)
        {
            enemy.SetTarget(finder.decoyTarget);
            return Status.Success;
        }
        else
        {
            enemy.SetTarget(finder.decoyTarget);
            return Status.Failure;
        }
    }
}

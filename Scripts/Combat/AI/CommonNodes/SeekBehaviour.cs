﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;
[NodeInfo(category = "Common/Action/")]

public class SeekBehaviour : BehaviourMachine.ActionNode {

    public EnemyBehaviourBase enemy;
    public TargetFinder targetFinder;
    public override Status Update()
    {
        //enemy.SetTarget(targetFinder.target);
        if(enemy._staggered)
        {
            return Status.Failure;
        }

        enemy.Seek();

        return Status.Success;
    }
}

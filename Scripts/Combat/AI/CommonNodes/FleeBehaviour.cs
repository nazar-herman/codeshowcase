﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

public class FleeBehaviour : ActionNode {
    public EnemyBehaviourBase enemy;
    public override Status Update()
    {
        enemy.Flee();
        return Status.Success;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Common/Condition/")]

public class DecoyRevealed : ConditionNode {

    public TargetFinder finder;
    public EnemyBehaviourBase enemy;
    public override Status Update()
    {
        if(finder.decoyTarget == null)
        {
            enemy.SetTarget(null);
            return Status.Failure;
        }

        StatueControl statue = finder.decoyTarget.GetComponent<StatueControl>();
        if(statue != null && !statue.revealed)
        {
            return Status.Success;
        }
        enemy.SetTarget(null);
        return Status.Failure;
    }
}

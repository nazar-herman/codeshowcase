﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class EnemyBehaviourBase : MonoBehaviour
{
    protected NavMeshAgent agent;
    protected Animator anim;
    [HideInInspector]
    public SquadController sqCtrl;
    [HideInInspector]
    public BattleAreaLogic areaLogic;
    public EnemySlot enemySlotTarget;
    public EnemySlot outerRingSlot;
    public float deadZone = 10.0f;
    public float angularSpeedDampTime = 5f;
    public bool chaseSlot = false;
    public Transform target;
    public float speed = 2f;
    public float fleeSpeed = 4f;
    public float attackDistance;
    public float attackDelay;
    protected float lastAttackTime;
    [HideInInspector]
    public bool waitingToAttack;
    public float stunnedDuration;
    protected float stunnedTimer;
    [HideInInspector]
    public bool _track = false;
    [HideInInspector]
    public bool _attacking = false;
    [HideInInspector]
    public bool _staggered = false;
    [HideInInspector]
    public bool _retreating = false;
    [HideInInspector]
    public bool _alreadyAttacked = false;
    [HideInInspector]
    public bool _inSquadList = false;
    [HideInInspector]
    public bool _orbiting = false;
    [HideInInspector]
    public bool _clockwise = false;
    [HideInInspector]
    public bool _stunned = false;
    public bool _retreated = false;
    public Collider hitBox;
    public Collider hitBoxRight;

    protected HealthCtrlBase healthCtrl;
    protected Rigidbody rb;
    bool removed;

    void Awake()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updatePosition = true;
        //agent.SetDestination(target.position);
        deadZone *= Mathf.Deg2Rad;
        healthCtrl = GetComponent<HealthCtrlBase>();
        rb = GetComponent<Rigidbody>();
    }

    public virtual void Update()
    {
        //agent.SetDestination(target.position);
        Vector3 horzVel = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        //if (horzVel.magnitude > 1)
        //{
        //    agent.enabled = false;
        //}
        //else
        //{
        //    agent.enabled = true;
        //}
        if (lastAttackTime > 0.0f)
        {
            if (Time.time < lastAttackTime + attackDelay)
            {
                waitingToAttack = true;
            }
            else
            {
                waitingToAttack = false;
            }
        }
        if(healthCtrl.dead && !removed && areaLogic != null)
        {
            areaLogic.RemoveEnemy(gameObject);
            removed = true;
        }
    }

    //void OnAnimatorMove()
    //{
    //    agent.velocity = anim.deltaPosition / Time.deltaTime;
    //}

    protected void Move()
    {
        // Speed is a projection of desired velocity on to the forward vector...
        float velMag = Vector3.Project(agent.desiredVelocity, transform.forward).magnitude * agent.speed;

        //anim.SetFloat("Speed", velMag);

        Track();
    }

    protected void Track()
    {
        float angle = FindAngle(transform.forward, agent.desiredVelocity, transform.up);

        //		 Make sure you get the absolute value for the angle you received above
        if (Mathf.Abs(angle) < deadZone)
        {
            transform.LookAt(transform.position + agent.desiredVelocity);
        }

        //		 the agents desiredVelocity, for the time we want to use deltaTime * angularSpeedDampTime
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(agent.desiredVelocity), Time.deltaTime * angularSpeedDampTime);
    }

    float FindAngle(Vector3 fromVector, Vector3 toVector, Vector3 upVector)
    {
        // If the vector the angle is being calculated to is 0 then we have no angle
        if (toVector == Vector3.zero)
        {
            return 0f;
        }

        // Create a float to store the angle between the facing of the enemy and the direction it's travelling.
        float _angle = Vector3.Angle(fromVector, toVector);

        // Find the cross product of the two vectors 
        // - Up will mean we need to go right
        // - Down will mean we need to go left
        Vector3 normal = Vector3.Cross(fromVector, toVector);

        // Convert from deg to rad for our animator
        _angle *= Mathf.Deg2Rad;

        return _angle;
    }

    public float getDistance()
    {
        return Mathf.Max(Vector3.Distance(transform.position, target.position), agent.remainingDistance);
    }

    public virtual void SetTarget(Transform target)
    {
        this.target = target;
        //agent.ResetPath();
        if(target == null)
        {
            if(agent != null)
            {
                agent.isStopped = true;
            }
            if(enemySlotTarget != null)
            {
                EnemySlot slot = enemySlotTarget.GetComponent<EnemySlot>();
                if(slot != null)
                {
                    slot.taken = false;
                }
                enemySlotTarget = null;
            }
        }
        else
        {
            agent.isStopped = false;
            agent.SetDestination(target.position);
        }
    }

    public void SetEnemySlotTarget(EnemySlot target)
    {
        this.enemySlotTarget = target;
        agent.SetDestination(enemySlotTarget.transform.position);
    }
    virtual public void Seek()
    {

    }
    virtual public void Attack()
    {

    }
    virtual public void Idle()
    {

    }

    virtual public void Flee()
    {

    }
    virtual public void Stop()
    {

    }

    virtual public void Knockback()
    {
        agent.isStopped = true;

    }

    public void EnemyAnimEvent(string name)
    {
        switch(name)
        {
            case "Attack Start":
                _attacking = true;
                break;
            case "Attack End":
                _attacking = false;
                break;
            case "Track Start":
                _track = true;
                break;
            case "Track End":
                _track = false;
                break;
            case "Stagger End":
                _staggered = false;
                break;
            case "HitBox Start":
                if(hitBox != null)
                    hitBox.enabled = true;
                break;
            case "HitBox End":
                if (hitBox != null)
                    hitBox.enabled = false;
                break;
            case "HitBoxRight Start":
                if (hitBoxRight != null)
                    hitBoxRight.enabled = true;
                break;
            case "HitBoxRight End":
                if (hitBoxRight != null)
                    hitBoxRight.enabled = false;
                break;
        }
    }

    public void Stagger()
    {
        if (!_staggered)
        {
            Reset();
            _staggered = true;
            anim.SetTrigger("Stagger");
        }
    }

    protected void Reset()
    {
        _track = false;
        _attacking = false;
        _staggered = false;
        waitingToAttack = false;
        lastAttackTime = 0.0f;
    }

    public void AddToSquadList()
    {
        if(sqCtrl != null && !_inSquadList)
        {
            sqCtrl.nearTargetMembers.Add(this);
            _inSquadList = true;
        }
    }

    public void RemoveFromSquadList()
    {
        if (sqCtrl != null && _inSquadList)
        {
            sqCtrl.nearTargetMembers.Remove(this);
            _inSquadList = false;
        }
    }

    public virtual void OnDestroy()
    {
        if(areaLogic != null)
        {
            areaLogic.RemoveEnemy(gameObject);
        }
    }
}

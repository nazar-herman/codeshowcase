﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFinder : MonoBehaviour {
    public Transform target;
    public Transform decoyTarget;
    [SerializeField]
    private float radius = 5.0f;
    SphereCollider col;
    public EnemyBehaviourBase enemyCtrl;
	// Use this for initialization
	void Start () {
        col = GetComponent<SphereCollider>();
        if(col != null)
        {
            col.radius = radius;
        }
        enemyCtrl = GetComponentInParent<EnemyBehaviourBase>();
	}
	


    public void SetRadius(float rad)
    {
        radius = rad;
        if (col != null)
        {
            col.radius = radius;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        CharacterControl chCtrl = other.GetComponent<CharacterControl>();
        if(chCtrl != null)
        {
            target = other.transform;
            return;
        }
        StatueControl statueCtrl = other.GetComponent<StatueControl>();
        if(statueCtrl != null)
        {
            decoyTarget = statueCtrl.transform;
            return;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (target != null && other.gameObject == target.gameObject)
        {
            target = null;

        }
        if (decoyTarget != null && other.gameObject == decoyTarget.gameObject)
        {
            decoyTarget = null;
        }
    }
}

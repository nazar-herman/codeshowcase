﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour {
    public Transform obj;
    public Vector3 offset;
	// Use this for initialization
	void Start () {
        transform.position = obj.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = obj.position + offset;
	}
}

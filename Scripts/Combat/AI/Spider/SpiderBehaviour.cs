﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderBehaviour : EnemyBehaviourBase
{
    public TargetFinder targetFinder;
    public float dashDistance;
    public float maxDashRadius;
    public float minDashRadius;
    public float dashRechargeTime;
    [HideInInspector]
    public bool canDash = true;
    public LayerMask dashMask;
    public bool isDashing;
    public Vector3 dashTarget;
    public GameObject webPrefab;
    float rechargeTimer;
    private void Start()
    {
        EndAttackSMB smb = anim.GetBehaviour<EndAttackSMB>();
        OnDashEnterSMB odsmb = anim.GetBehaviour<OnDashEnterSMB>();
        odsmb.enemy = this;
        smb.enemy = this;
    }
    public override void Attack()
    {
        agent.velocity = Vector3.zero;
        agent.isStopped = false;
        agent.SetDestination(target.position);

        Track();
        if (anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", false);
        }

        if (lastAttackTime > 0.0f)
        {
            if (Time.time > (lastAttackTime + attackDelay))
            {
                anim.SetTrigger("Attack");
                lastAttackTime = Time.time;
                stunnedTimer = stunnedDuration;
            }
        }
        else
        {
            anim.SetTrigger("Attack");
            lastAttackTime = Time.time;
            stunnedTimer = stunnedDuration;
        }
    }

    public override void Seek()
    {
        if (isDashing)
        {
            return;
        }
        if (!anim.GetBool("Walk"))
        {
            anim.SetBool("Walk", true);
        }
        agent.SetDestination(target.position);
        dashTarget = target.position;
        agent.isStopped = false;
        agent.speed = speed;
        Move();
    }
    public override void Flee()
    {
        agent.destination = transform.position + (transform.position - target.position).normalized;
        dashTarget = transform.position + (transform.position - target.position).normalized * dashDistance;
        //agent.isStopped = false;
        agent.speed = fleeSpeed;
        Dash();
        _retreated = true;
    }
    public override void Idle()
    {
        anim.SetBool("Walk", false);
        Stop();
    }


    public override void SetTarget(Transform target)
    {
        this.target = target;
    }

    public override void Stop()
    {
        agent.isStopped = true;
        anim.SetBool("Walk", false);
    }

    public override void Update()
    {
        base.Update();
        if (stunnedTimer >= 0.0f)
        {
            _stunned = true;
            stunnedTimer -= Time.deltaTime;
        }
        else
        {
            _stunned = false;
        }
        if (rechargeTimer >= 0.0f)
        {
            rechargeTimer -= Time.deltaTime;
        }
        else
        {
            canDash = true;
        }
    }

    public void Dash()
    {
        anim.Play("Dash");
        anim.SetBool("Dashing", true);
        Stop();
        isDashing = true;
        rechargeTimer = dashRechargeTime;
        canDash = false;
        healthCtrl.invincible = true;
        transform.LookAt(dashTarget);
    }

    public void DoDash()
    {
        Vector3 direction = transform.forward;
        if (dashTarget != null)
        {
            direction = dashTarget - transform.position;
            direction.y = 0;
            direction = direction.normalized;
        }
        rb = GetComponent<Rigidbody>();
        Vector3 p1 = transform.position + 0.1f * Vector3.up;
        RaycastHit[] hits = Physics.RaycastAll(p1, direction * (dashDistance + 1), dashDistance + 1, dashMask);
        Debug.DrawLine(p1, p1 + direction * (dashDistance + 1), Color.red, 10);
        float actualDistance = dashDistance;
        foreach (RaycastHit hit in hits)
        {
            float distanceToHit = Vector3.Distance(hit.point - 0.1f * Vector3.up, transform.position);
            actualDistance = Mathf.Clamp(actualDistance, 0.0f, distanceToHit - 1);
        }
        Debug.Log("Dash " + actualDistance);
        rb.MovePosition(transform.position + actualDistance * direction);
    }

    public void EndDash()
    {
        Debug.Log("End Dash");
        isDashing = false;
        anim.SetBool("Dashing", false);
        healthCtrl.invincible = false;
    }

    public void DropWeb()
    {
        if(webPrefab != null)
        {
            Instantiate(webPrefab, transform.position, transform.rotation);
        }
    }
}

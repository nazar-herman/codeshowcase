﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Spider/Condition/")]
public class CanDashCondition : ConditionNode  {
    public SpiderBehaviour enemy;
    public override Status Update()
    {
        if(enemy.canDash)
        {
            return Status.Success;
        }
        return Status.Failure;
    }

}

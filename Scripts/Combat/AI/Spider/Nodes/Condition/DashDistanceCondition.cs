﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Spider/Condition/")]
public class DashDistanceCondition : ConditionNode {
    public SpiderBehaviour enemy;

    public override Status Update()
    {
        if(enemy.target == null)
        {
            return Status.Failure;
        }

        if(enemy.isDashing)
        {
            return Status.Failure;
        }
        float distance = Vector3.Distance(enemy.transform.position, enemy.dashTarget);
        if(enemy.minDashRadius <= distance && distance <= enemy.maxDashRadius)
        {
            return Status.Success;
        }
        return Status.Failure;
    }
}

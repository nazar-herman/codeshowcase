﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Spider/Condition/")]
public class IsDashingCondition : ConditionNode {
    public SpiderBehaviour spiderBehaviour;

    public override Status Update()
    {
        if(spiderBehaviour.isDashing)
        {
            return Status.Success;
        }
        return Status.Failure;
    }
}

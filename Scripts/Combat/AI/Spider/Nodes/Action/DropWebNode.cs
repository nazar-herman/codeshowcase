﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Spider/Action/")]
public class DropWebNode : ActionNode {
    public SpiderBehaviour enemy;

    public override Status Update()
    {
        enemy.DropWeb();
        return Status.Success;
    }
}

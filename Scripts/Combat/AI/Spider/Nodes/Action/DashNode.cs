﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using System;

[NodeInfo(category = "Spider/Action/")]
public class DashNode : ActionNode {
    public SpiderBehaviour enemy;
    Rigidbody rb;
    public override Status Update()
    {
        if(enemy.isDashing)
        {
            return Status.Failure;
        }
        enemy.Dash();
        return Status.Success;
    }


}

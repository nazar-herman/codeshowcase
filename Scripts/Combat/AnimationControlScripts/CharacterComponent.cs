﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterComponent : MonoBehaviour
{
    protected Animator anim;
    public CombatStateControl combatCtrl;
    public HealthCtrlBase healthCtrl;
    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        combatCtrl = GetComponent<CombatStateControl>();
        healthCtrl = GetComponent<HealthCtrlBase>();
        BaseSMB[] smbs = anim.GetBehaviours<BaseSMB>();

        foreach(BaseSMB smb in smbs)
        {
            smb.component = this;
        }
    }

    public virtual void Reset()
    {

    }

    public virtual void SetMovement(bool val)
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatStateControl : CharacterComponent
{
    public GameObject currentSword;
    public GameObject[] swords;
    public GameObject[] slashEffects;
    [HideInInspector]
    public bool CombatActive = false;
    [HideInInspector]

    private TargetControl targetControl;
    private LightController lightControl;
    private CharacterControl characterControl;

    private bool _hitBoxActive = false;
    private bool _trailActive = false;
    private bool _canAttack = true;
    private bool _canTransition = false;
    private bool _canRoll = true;
    private bool _canDoMovement = true;
    private bool _staggered = false;
    private bool _rolling = false;

    public bool Staggered
    {
        get { return _staggered; }

        set
        {
            _staggered = value;

            anim.SetBool("staggered", value);        
        }
    }
    public bool HitBoxActive
    {
        get { return _hitBoxActive; }

        set
        {
            _hitBoxActive = value;
            currentSword.GetComponent<Collider>().enabled = value;
        }
    }
    //public bool TrailActive
    //{
    //    get { return _trailActive; }
    //    set
    //    {
    //        _trailActive = value;
    //        if (value)
    //        {
    //            Vector3 position = new Vector3(transform.position.x, 0.9f, transform.position.z);
    //            GameObject sl = Instantiate(slashEffects[(int)lightControl.lightColor], position + transform.forward, Quaternion.identity);
    //            sl.transform.parent = sword.transform;
    //            sl.transform.localEulerAngles = -sword.transform.localEulerAngles;
    //            sl.transform.parent = null;
    //            Destroy(sl, 0.10f);
    //        }
    //    }
    //}
    public bool CanAttack
    {
        get { return _canAttack; }
        set
        {
            _canAttack = value;

            anim.SetBool("canAttack", value);
        }
    }
    public bool CanTransition
    {
        get { return _canTransition; }
        set
        {
            _canTransition = value;
            anim.SetBool("CanTransition", value);
        }
    }
    public bool CanRoll
    {
        get { return _canRoll; }
        set
        {
            _canRoll = value;
            anim.SetBool("canRoll", value);
        }
    }
    public bool CanDoMovement
    {
        get { return _canDoMovement; }
        set
        {
            _canDoMovement = value;
            anim.SetBool("canDoMovement", value);
        }
    }

    protected override void Awake()
    {
        base.Awake();
        targetControl = GetComponent<TargetControl>();
        lightControl = GetComponent<LightController>();
        characterControl = GetComponent<CharacterControl>();
        currentSword = swords[(int)lightControl.lightColor];
        anim.SetInteger("Color", (int)lightControl.lightColor);
        //currentSword.SetActive(true);
    }

    public override void Reset()
    {
        CanTransition
            = HitBoxActive 
            = false;

        this.gameObject.layer = 8;
    }

    public override void SetMovement(bool val)
    {
        CanDoMovement = val;
    }

    public void ResetAll()
    {
        CanAttack = true;
        CanTransition = false;
        CanRoll = true;
        CanDoMovement = true;
        HitBoxActive = false;
    }

    public void HandleCombatInput()
    {
        if(!currentSword.activeSelf)
        {
            return;
        }
        if (CanAttack)
        {
            if (!CombatActive)
            {
                CombatActive = true;
                CanDoMovement = false;
                anim.doSetInteger(AnimCombatParams.AnimParam.Attack, 10);

            }
            else
            {
                anim.doSetInteger(AnimCombatParams.AnimParam.Attack, (int)AnimCombatParams.AnimTransitionState.TransitionState_Pending);
            }

            if (targetControl.LockedTarget != null)
            {
                transform.LookAt(new Vector3(targetControl.LockedTarget.transform.position.x, transform.position.y, targetControl.LockedTarget.transform.position.z));
            }

            if(targetControl.TargetFacing != null)
            {
                transform.LookAt(new Vector3(targetControl.TargetFacing.transform.position.x, transform.position.y, targetControl.TargetFacing.transform.position.z));
            }

        }
    }

    public void HandleRoll(Vector2 rollDir)
    {
        if(CombatActive)
        {
            anim.doSetInteger(AnimCombatParams.AnimParam.Attack, (int)AnimCombatParams.AnimTransitionState.TransitionState_None);
        }

        if(_canRoll)
        {
            if (anim.GetFloat("inputMag") > 0.25)
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(rollDir.x, rollDir.y) * Mathf.Rad2Deg + 45.0f, transform.eulerAngles.z);
            }
            anim.doSetInteger(AnimCombatParams.AnimParam.Roll, 10);
        }
    }

    public void OnCombatEvent(string eventName)
    {
        switch(eventName)
        {
            case "Attack Start":
                CanRoll = false;
                CanDoMovement = false;
                characterControl.switchingAllowed = false;
                break;
            case "Attack End":
                if (!_rolling)
                {
                    CanDoMovement = true;
                }
                CombatActive = false;
                characterControl.switchingAllowed = true;
                break;
            case "Can Transition":
                if (!Staggered)
                {
                    CanRoll = true;
                    CanTransition = true;
                }
                break;
            case "Roll Start":
                HitBoxActive = false;
                CanAttack = false;
                CanDoMovement = false;
                CombatActive = false;
                CanRoll = false;
                _rolling = true;
                break;
            case "Roll End":
                CanAttack = true;
                CanDoMovement = true;
                CanRoll = true;
                characterControl.switchingAllowed = true;
                _rolling = false;
                break;
            case "Layer Swap Start":
                this.gameObject.layer = 20;
                break;
            case "Layer Swap End":
                this.gameObject.layer = 8;
                break;
            case "Hit Box Start":
                if (!Staggered)
                {
                    HitBoxActive = true;
                }
                break;
            case "Hit Box End":
                HitBoxActive = false;
                break;
            case "Trail Start":
                //TrailActive = true;
                break;
            case "Trail End":
                //TrailActive = false;
                break;
            case "Stagger Start":
                Reset();
                CanDoMovement = false;
                CanRoll = false;
                CanAttack = false;
                CombatActive = false;
                Staggered = true;
                break;
            case "Stagger End":
                CanDoMovement = true;
                CanRoll = true;
                CanAttack = true;
                Staggered = false;
                break;
        }
    }

    public void OnLightChanged()
    {
        if(!lightControl.unlocks[(int)lightControl.lightColor])
        {
            return;
        }
        currentSword.SetActive(false);
        currentSword = swords[(int)lightControl.lightColor];
        anim.SetInteger("Color", (int)lightControl.lightColor);
        currentSword.SetActive(true);
    }
}

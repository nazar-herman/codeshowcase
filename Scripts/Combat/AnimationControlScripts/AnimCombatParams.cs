﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnimCombatParams
{
    public static int paramNone = 0;
    public static int paramAttack = Animator.StringToHash("Attack");
    public static int paramAbility = Animator.StringToHash("Ability");
    public static int paramRoll = Animator.StringToHash("Roll");
    public static int paramRed = Animator.StringToHash("Red");
    public static int paramBlue = Animator.StringToHash("Blue");
    public static int paramYellow = Animator.StringToHash("Yellow");
    public static int paramGrab = Animator.StringToHash("doGrab");
    public enum AnimParam
    {
        None, Attack, Ability, Roll, doGrab
    }

    private static int[] colorParamHashes = { paramRed, paramYellow, paramBlue };
    private static int[] paramHashes = { paramNone, paramAttack, paramAbility, paramRoll, paramGrab };

    public enum AnimTransitionState
    {
        TransitionState_None = 0,
        TransitionState_Pending = 1,
        TransitionState_Triggered = 2
    }

    public static int doSetInteger(this Animator animator, AnimParam param, int? value=null)
    {
        int retVal = -1;

        if(param != AnimParam.None)
        {
            animator.SetInteger(paramHashes[(int)param], (int)value);
            retVal = 1;
        }
        else
        {
            retVal = 0;
        }

        return retVal;
    }

    public static bool hasParameter(this Animator animator, string name)
    {
        foreach(AnimatorControllerParameter parameter in animator.parameters)
        {
            if(parameter.name == name)
            {
                return true;
            }
        }

        return false;
    }
}

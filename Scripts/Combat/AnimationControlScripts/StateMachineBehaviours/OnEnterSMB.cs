﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnterSMB : StateMachineBehaviour{

    public AnimCombatParams.AnimParam animParam;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        animator.doSetInteger(animParam, (int)AnimCombatParams.AnimTransitionState.TransitionState_Triggered);
    }
}

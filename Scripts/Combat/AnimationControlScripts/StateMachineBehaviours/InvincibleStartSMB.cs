﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleStartSMB : BaseSMB {


    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        component.healthCtrl.invincible = true;
    }
}

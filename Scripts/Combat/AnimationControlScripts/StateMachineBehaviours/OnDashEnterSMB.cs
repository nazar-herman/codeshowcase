﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDashEnterSMB: StateMachineBehaviour{
    public EnemyBehaviourBase enemy;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.EnemyAnimEvent("HitBox End");
    }
}

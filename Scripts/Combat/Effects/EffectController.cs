﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class EffectController : MonoBehaviour
{
    HealthCtrlBase healthCtrl;
    public GameObject hitParticle;
    public GameObject stunEffect;
    List<Material> mats;
    float deathEffectTimer = 0.0f;
    void Start()
    {
        mats = new List<Material>();
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach(Renderer renderer in renderers)
        {
            mats.Add(renderer.material);
        }
        healthCtrl = GetComponent<HealthCtrlBase>();
    }
    public void spawnHitParticle()
    {
        GameObject hp = Instantiate(hitParticle, transform.position + new Vector3(0, 0.7f, 0), transform.rotation);
        Destroy(hp, 0.5f);
    }

    void Update()
    {
        if (healthCtrl.dead)
        {
            deathEffectTimer += Time.deltaTime;
            foreach(Material mat in mats)
            {
                if (deathEffectTimer < 400.0f && mat.HasProperty("_DissolveAmount"))
                {
                    mat.SetFloat("_DissolveAmount", Mathf.Lerp(mat.GetFloat("_DissolveAmount"), 1, deathEffectTimer * 0.004f));
                }
            }
        }
    }
}

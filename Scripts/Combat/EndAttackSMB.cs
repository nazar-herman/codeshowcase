﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndAttackSMB : StateMachineBehaviour {
    public EnemyBehaviourBase enemy;
    // Use this for initialization
    private bool beginExit = false;
    private bool waitingToBegin = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (animator.IsInTransition(layerIndex))
        {
            waitingToBegin = true;
        }
        else
        {
            waitingToBegin = false;
        }

        beginExit = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);

        if (!beginExit)
        {
            if (animator.IsInTransition(layerIndex))
            {

                if (!waitingToBegin)
                {
                    enemy.EnemyAnimEvent("HitBox End");
                    beginExit = true;
                }
            }
            else if (waitingToBegin)
            {
                waitingToBegin = false;
            }
        }
    }
}

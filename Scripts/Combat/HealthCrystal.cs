﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCrystal : MonoBehaviour
{
    HealthCtrlBase parentHealthCtrl;

    public float destroyAtPercent;
    [HideInInspector]
    public float healthPercent;

    void Awake()
    {
        parentHealthCtrl = GetComponentInParent<HealthCtrlBase>();
    }
    void Update()
    {
        healthPercent = parentHealthCtrl.HealthPercent;

        if(healthPercent <= destroyAtPercent)
        {
            SendMessageUpwards("Stagger");
            DestroyCrystal();
        }
    }

    public void DestroyCrystal()
    {
        //Spawn Some Effect
        Destroy(this.gameObject);
    }
}

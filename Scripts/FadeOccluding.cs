﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOccluding : MonoBehaviour
{
    public Transform target;
    public LayerMask layerMask;

    void Update()
    {
        float distance = Vector3.Distance(transform.position, target.position);
        Vector3 dirVec = target.position - transform.position;
        RaycastHit[] hits = Physics.RaycastAll(transform.position, dirVec,distance - 2.0f,layerMask);

        foreach(RaycastHit hit in hits)
        {
            //Renderer renderer = hit.collider.GetComponent<Renderer>();
            //if(renderer == null)
            //{
            //    continue;
            //}

            MakeTransparent MT = hit.collider.GetComponent<MakeTransparent>();

            if(MT == null)
            {
                hit.collider.gameObject.AddComponent<MakeTransparent>();
            }

            if (MT != null)
            {
                MT.DoTransparency();
            }
        }
    }
}

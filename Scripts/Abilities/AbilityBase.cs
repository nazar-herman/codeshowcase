﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum AbilityType
{
    Primary,
    Secondary
}

public enum ChargeStage
{
    First,
    Second,
    Third,
    Four
}


public class AbilityBase : MonoBehaviour {
    [Header("Ability settings")]
    public LightColor lightType;
    public AbilityType abilityType;
    //[HideInInspector]
    public int minEnergyAmount;
    [HideInInspector]
    public int energyAmount;
    //public int maxEnergyAmount;
   
    public float cooldown;
    [HideInInspector]
    public bool OnCooldown;
    public string triggerName;
    protected CombatStateControl combatCtrl;
    public bool startCooldown;
    protected Animator anim;
    protected CharacterControl chCtrl;
    Vector3 oldChrPos;
    float chargeTimer;
    float timer;
    ParticleSystem currentParticle;

    [Space(10)]
    [Header("Sounds")]
    public AudioClip abilitySFX;
    protected CharacterSFX characterSFX;

    public float CooldownTimer
    {
        get { return timer; }
    }

    public virtual void Start()
    {
        anim = GetComponent<Animator>();
        combatCtrl = GetComponent<CombatStateControl>();
        chCtrl = GetComponent<CharacterControl>();
        characterSFX = GetComponent<CharacterSFX>();
    }

    public virtual void Prepare()
    {

    }
    public virtual void Charge()
    {
    }
    public virtual void Execute()
    {

    }
    public virtual void Update()
    {
        if(startCooldown)
        {
            timer = cooldown;
            OnCooldown = true;
            startCooldown = false;
        }
        if(OnCooldown)
        {
            if(timer <= 0.0f)
            {
                OnCooldown = false;
            }
            timer -= Time.deltaTime;
        }
    }

    public virtual void End()
    {
        anim.SetBool("canStop", false);
        anim.SetBool(triggerName, false);
        combatCtrl.CanAttack = true;
        combatCtrl.CombatActive = false;
        chCtrl.switchingAllowed = true;
        chCtrl.paused = false;
    }

    public virtual void Use()
    {

        anim.SetBool(triggerName, true);
        anim.SetInteger("Ability", 10);
        combatCtrl.CanAttack = false;
        combatCtrl.CombatActive = true;
        startCooldown = true;
    }
}

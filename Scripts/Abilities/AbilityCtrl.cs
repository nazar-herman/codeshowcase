﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityCtrl : MonoBehaviour {
    public Dictionary<LightColor, AbilityBase> abilities = new Dictionary<LightColor, AbilityBase>();
    public bool HideAbilities;
    bool preparation;
    bool doAbility;
    bool chargeAbility;
    bool prepareAbility;
    bool executeAbility;
    Animator anim;
    LightController lightCtrl;
    TargetControl targetCtrl;
    CombatStateControl combatCtrl;
    CharacterControl chCtrl;
	// Use this for initialization
	void Start () {
        AbilityBase[] abilitiesList = GetComponents<AbilityBase>();
        foreach(AbilityBase ability in abilitiesList)
        {
            abilities.Add(ability.lightType, ability);
        }
        lightCtrl = GetComponent<LightController>();
        anim = GetComponent<Animator>();
        targetCtrl = GetComponent<TargetControl>();
        combatCtrl = GetComponent<CombatStateControl>();
        chCtrl = GetComponent<CharacterControl>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!chCtrl.stunned)
        {
            if (Input.GetButtonDown(Buttons.Y))
            {
                doAbility = true;
            }
        }
        //float triggerValue = Input.GetAxis(Buttons.RT);
        //if(triggerValue == 1.0f && !chargeAbility)
        //{
        //    prepareAbility = true;
        //}
        //if(triggerValue == 0.0f && chargeAbility)
        //{
        //    executeAbility = true;
        //    chargeAbility = false;
        //}

        if (doAbility && combatCtrl.CanDoMovement)
        {
            doAbility = false;
            if (abilities.ContainsKey(lightCtrl.lightColor))
            {
                AbilityBase ability = abilities[lightCtrl.lightColor];
                if(ability.minEnergyAmount <= lightCtrl.lightPool.Data && !ability.OnCooldown)
                { 
                    if (targetCtrl.TargetFacing != null)
                    {
                        transform.LookAt(new Vector3(targetCtrl.TargetFacing.transform.position.x, transform.position.y, targetCtrl.TargetFacing.transform.position.z));
                    }
                    if (targetCtrl.LockedTarget != null)
                    {
                        transform.LookAt(new Vector3(targetCtrl.LockedTarget.transform.position.x, transform.position.y, targetCtrl.LockedTarget.transform.position.z));
                    }
                    ability.Use();
                    lightCtrl.Drain(ability.energyAmount);
                }
            }
        }

        //if(prepareAbility && combatCtrl.CanDoMovement && !chargeAbility)
        //{
        //    if (abilities.ContainsKey(lightCtrl.lightColor))
        //    {
        //        AbilityBase ability = abilities[lightCtrl.lightColor];
        //        if (ability.minEnergyAmount <= lightCtrl.lightPool.Data && !ability.OnCooldown)
        //        {
        //            prepareAbility = false;
        //            //if (targetCtrl.LockedTarget != null)
        //            //{
        //            //    transform.LookAt(new Vector3(targetCtrl.LockedTarget.transform.position.x, transform.position.y, targetCtrl.LockedTarget.transform.position.z));
        //            //}
        //            ability.Prepare();
        //            chargeAbility = true;
        //        }
        //    }
        //}
        //if (executeAbility && combatCtrl.CanDoMovement)
        //{
        //    executeAbility = false;
        //    AbilityBase ability = abilities[lightCtrl.lightColor];
        //    if (ability.startCharging)
        //    {
        //        ability.Execute();
        //        lightCtrl.Drain(ability.energyAmount);
        //    }
        //}
    }
}

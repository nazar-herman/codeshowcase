﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityPointer : MonoBehaviour
{
    public GameObject pointer;
    public LayerMask mask;
    public float showHeight;
    public Vector3 showPointField;
    public Vector3 hitNormal;
    public float hitHeight;
    public RaycastHit hit;
    Ray ray;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public void ShowPointer(Vector3 showPoint)
    {
        showPointField = showPoint;
        //showPointField.y += 2;
        //Gizmos.DrawLine(showPoint, showPoint + 20 * Vector3.down);
        Debug.DrawRay(showPointField, Vector3.down*10, Color.red, 10);
        if (Physics.Raycast(showPointField, Vector3.down,out hit, 10, mask))
        {
            //Debug.Log("HIT");
            Vector3 properShowPoint = hit.point;
            hitHeight = hit.point.y;
            hitNormal = hit.normal;
            properShowPoint.y += showHeight;
            pointer.transform.position = properShowPoint;
            pointer.SetActive(true);
        }
    }

    public void HidePointer()
    {
        pointer.SetActive(false);
    }
}

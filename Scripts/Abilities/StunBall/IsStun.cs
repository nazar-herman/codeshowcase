﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsStun : MonoBehaviour {
    public float stunDuration;
    
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        CharacterControl chCtrl = other.GetComponent<CharacterControl>();
        if(chCtrl != null)
        {
            chCtrl.Stun(stunDuration);
        }
    }
}

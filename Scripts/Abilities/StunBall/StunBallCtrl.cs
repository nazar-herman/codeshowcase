﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunBallCtrl : MonoBehaviour {
    public Vector3 moveDirection;
    public float moveSpeed;
    public float timer;
    public int hits;
    public int maxHits;
    public ParticleSystem particleSys;
    public List<GameObject> dropItems;
    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

    private void Update()
    {
        if(timer <= 0.0f)
        {
            if(particleSys != null)
            {
                particleSys.Play();
            }

            Destroy(gameObject);

        }
        timer -= Time.deltaTime;
    }
    // Update is called once per frame
    void FixedUpdate () {
        rb.MovePosition(transform.position + moveDirection * moveSpeed * Time.deltaTime);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 22)
        {
            Destroy(this.gameObject);
            return;
        }

        moveDirection = Vector3.Reflect(moveDirection, collision.contacts[0].normal);

    }
}

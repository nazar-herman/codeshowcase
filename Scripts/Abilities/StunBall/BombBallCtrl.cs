﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBallCtrl : MonoBehaviour
{
    public Vector3 moveDirection;
    public float moveSpeed;
    public float timer;
    public float explosionRadius;
    public int damage;
    public ParticleSystem particleSys;
    public List<GameObject> dropItems;

    public bool exploding;
    public bool blowedUp;
    Rigidbody rb;
    Collider col;
    Ray ray;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (timer <= 0.0f)
        {
            if (!exploding)
            {
                BlowUp();
            }
           
        }
        if (blowedUp && (particleSys == null || !particleSys.IsAlive()))
        {
            Destroy(gameObject);
        }
        timer -= Time.deltaTime;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        rb.MovePosition(transform.position + moveDirection * moveSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        BlowUp();
    }

    public void BlowUp()
    {
        exploding = true;
        if (particleSys != null)
        {
            particleSys.Play();
        }
        RaycastHit[] hits = Physics.SphereCastAll(ray, explosionRadius);
        foreach (RaycastHit hit in hits)
        {
            HealthCtrlBase healthCtrl = hit.collider.gameObject.GetComponent<HealthCtrlBase>();
            if (healthCtrl != null)
            {
                //healthCtrl.TakeHit(damage);
            }
        }
        blowedUp = true;
    }
}

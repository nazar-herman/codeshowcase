﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunBallAbility : AbilityBase {
    [Header("Stun Settings")]
    public GameObject stunBall;
    public GameObject bombBall;
    public float stunDuration;
    public float ballSpeed;
    public float existTime;
	// Use this for initialization
	public override void Start () {
        base.Start();
        energyAmount = minEnergyAmount;
    }
    public override void Execute()
    {
        base.Execute();
        chCtrl.paused = true;
    }
    public void ThrowBall()
    {
        GameObject ball = Instantiate(stunBall, transform.position + Vector3.up + transform.forward, transform.rotation);
        StunBallCtrl stunCtrl = ball.GetComponent<StunBallCtrl>();
        stunCtrl.moveDirection = transform.forward;
        stunCtrl.moveSpeed = ballSpeed;
        stunCtrl.timer = existTime;
        End(); 
    }

    public override void Use()
    {
        base.Use();
        chCtrl.paused = true;
    }
    public override void End()
    {
        base.End();
        chCtrl.paused = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isDashTrigger : MonoBehaviour {
    IsDamaging damaging;
	// Use this for initialization
	void Start () {
        damaging = GetComponent<IsDamaging>();
        
	}

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        IsChargeable chargeable = other.GetComponent<IsChargeable>();
        if(chargeable != null && chargeable.type == LightColor.Red)
        {
            if(damaging != null)
            {
                chargeable.Charge(damaging.damage);
            }
        }
    }
}

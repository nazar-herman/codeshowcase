﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAbility : AbilityBase {
    [Header("Dash settings")]
    public bool isDashing;
    public float dashDistance;
    public float dashSpeed;
    public GameObject dashTrigger;
    [Header("Charge settings")]
    public float dashDistancePerCharge;
    public float dashSpeedPerCharge;
    public float maxDashDistance;
    public float maxDashSpeed;
    public LayerMask layerMask;
    [HideInInspector]
    public bool startDash;
    public ParticleSystem dashParticle;
    Rigidbody rb;
    float dashTime;
    float finalDashDistance;
    float finalDashSpeed;
    float dashDistanceSpeed;
    float dashSpeedSpeed;
    Vector3 oldPosition;
    // Use this for initialization
    public override void Start () {
        base.Start();
        rb = GetComponent<Rigidbody>();
        energyAmount = minEnergyAmount;
    }

    // Update is called once per frame
    public override void Use()
    {
        //base.Use();

        if(isDashing)
        {
            return;
        }
        combatCtrl.CanAttack = false;
        combatCtrl.CombatActive = true;
        chCtrl.paused = true;
        startCooldown = true;

        characterSFX.SetClip("RedAbility");
        characterSFX.Play();

        anim.Play("Dash");
        Vector3 p1 = transform.position + 0.1f * Vector3.up - transform.forward;

        RaycastHit[] hits = Physics.RaycastAll(p1, transform.forward * (dashDistance + 1), dashDistance + 1, layerMask);
        Debug.DrawLine(p1, p1 + transform.forward * (dashDistance + 1), Color.red, 10);
        float actualDistance = dashDistance;
        foreach (RaycastHit hit in hits)
        {
            float distanceToHit = Vector3.Distance(hit.point - 0.1f * Vector3.up, transform.position);
            actualDistance = Mathf.Clamp(actualDistance, 0.0f, distanceToHit - 1);
        }
        rb.MovePosition(transform.position + actualDistance * transform.forward);
        isDashing = true;
        startDash = false;
        gameObject.layer = 11;
        dashTrigger.SetActive(true);
        oldPosition = transform.position;
        dashParticle.Play();
    }

    public override void Prepare()
    {
        base.Prepare();
    }


    public override void Charge()
    {
        
    }
    public override void Update()
    {
        base.Update();

    }
    void FixedUpdate()
    {
        DoDash();
    }

    void DoDash()
    {
        if (isDashing && Vector3.Distance(transform.position, oldPosition) < finalDashDistance)
        {
            rb.MovePosition(transform.position + transform.forward * finalDashSpeed * Time.deltaTime);
        }
        
    }

    public override void End()
    {
        base.End();
        isDashing = false;
        rb.useGravity = true;
        chCtrl.paused = false;
        gameObject.layer = 8;
        dashTrigger.SetActive(false);
        rb.velocity = Vector3.zero;
        chCtrl.atk = true;
    }

    public void EndDash()
    {
        End();
    }

}

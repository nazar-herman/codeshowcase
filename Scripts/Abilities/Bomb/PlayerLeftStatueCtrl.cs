﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLeftStatueCtrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.GetComponent<CharacterControl>() != null)
        {
            transform.parent.gameObject.layer = 0;
        }

    }
}

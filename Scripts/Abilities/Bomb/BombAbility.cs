﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombAbility : AbilityBase {
    [Header("Bomb settings")]
    public GameObject statuePrefab;
    public float baseSpawnDistance;
    public float baseSpawnHeight;
    public float distancePerCharge;
    public float maxDistance;
    public List<Renderer> renderers;
    public StatueControl currentStatue;
    Vector3 spawnPosition;
    AbilityPointer abilityPointer;
    HealthCtrlBase healthCtrl;
    float finalSpawnDistance;
    //float finalDashSpeed;
    float statueDistanceSpeed;
    float finalSpawnHeight;
    bool executed;
    bool preparing;
    bool hide;
    int realMinEnergyAmount;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        abilityPointer = GetComponent<AbilityPointer>();
        realMinEnergyAmount = minEnergyAmount;
        healthCtrl = GetComponent<HealthCtrlBase>();
    }



    public override void End()
    {
        base.End();
    }

    public override void Update()
    {
        base.Update();
        Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (hide && stickInput.magnitude > 0.25)
        {

            ShowCharachter();
            anim.SetFloat("inputMag", stickInput.magnitude);
            combatCtrl.HandleRoll(stickInput);            
        }
        if(!hide && executed)
        {
            executed = false;
            End();
        }
    }

    public override void Charge()
    {
        base.Charge();
    }

    public void SpawnBomb(Vector3 position)
    {
            GameObject statue = Instantiate(statuePrefab, position, transform.rotation, null);
            StatueControl statueCtrl = statue.GetComponent<StatueControl>();
            statueCtrl.SetBonesPosition(anim);
            currentStatue = statueCtrl;
            minEnergyAmount = 0;
            statueCtrl.bombCtrl = this;
    }

    public void ShowCharachter()
    {
        chCtrl.paused = false;
        minEnergyAmount = realMinEnergyAmount;
        anim.speed = 1;
        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = true;
        }
        healthCtrl.invincible = false;
        hide = false;
    }

    public override void Use()
    {
        base.Use();
        startCooldown = false;
        if (currentStatue != null && !currentStatue.exploding)
        {
            currentStatue.BlowUp();
            energyAmount = 0;
            minEnergyAmount = realMinEnergyAmount;
            base.End();
            return;
        }
        minEnergyAmount = 0;
        energyAmount = realMinEnergyAmount;
        spawnPosition = transform.position + Vector3.up*0.1f;
        SpawnBomb(spawnPosition);

        characterSFX.SetClip("BlueAbility");
        characterSFX.Play();

        chCtrl.paused = true;
        anim.speed = 0;
        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = false;
        }
        healthCtrl.invincible = true;
        hide = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class StatueControl : MonoBehaviour {
    public BombAbility bombCtrl;
    public float explosionRadius;
    public float explodeTime;
    public ParticleSystem explosionParticles;
    public LayerMask hitMask;
    public int explosionDamage;
    public List<Renderer> renderers;
    public bool revealed;
    float timer;
    Animator anim;
    public bool exploding = false;
    public bool blowedUp = false;
    Ray ray;
    Collider col;
    Rigidbody rb;
    DeathCtrlBase deathCtrl;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        deathCtrl = GetComponent<DeathCtrlBase>();
    }
	
	// Update is called once per frame
	public void SetBonesPosition (Animator parentAnim) {
        anim = GetComponent<Animator>();
        foreach (HumanBodyBones bone in Enum.GetValues(typeof(HumanBodyBones)))
        {
            Transform boneTransform = parentAnim.GetBoneTransform(bone);
            Transform statueBoneTransform = anim.GetBoneTransform(bone);
            if(boneTransform != null && statueBoneTransform != null)
            {
                statueBoneTransform.localPosition = boneTransform.localPosition;
                statueBoneTransform.localRotation = boneTransform.localRotation;
            }
        }
	}

    private void Update()
    {
        explodeTime -= Time.deltaTime;
        if(explodeTime <= 0.0f && !exploding)
        {
            BlowUp();
        }
    }

    public void BlowUp()
    {
        exploding = true;
        col.enabled = false;
        rb.useGravity = false;
        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = false;
        }
        bombCtrl.ShowCharachter();
        bombCtrl.startCooldown = true;
        blowedUp = true;
        deathCtrl.Die();
    }

    public void OnDestroy()
    {
        if (!exploding)
        {
            exploding = true;
            col.enabled = false;
            rb.useGravity = false;
            foreach (Renderer renderer in renderers)
            {
                renderer.enabled = false;
            }
            bombCtrl.ShowCharachter();
            bombCtrl.startCooldown = true;
            blowedUp = true;
        }
    }
}

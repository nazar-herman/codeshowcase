﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public AudioSource musicSource;
    public AudioSource sfxSource;
    public AudioSource ambientSource;

    public AudioClip mainMenuMusic;
    public AudioClip forestMusic;
    public AudioClip blendLandsMusic;
    public AudioClip cityMusic;
    public AudioClip thanksMusic;

    private Dictionary<string, AudioClip> music;

    private static AudioManager _ins;

    public static AudioManager Instance
    {
        get
        {
            return _ins;
        }
    }

    void Awake()
    {
        if(_ins == null)
        {
            _ins = this;
        }

        music = new Dictionary<string, AudioClip>();

        music["TitleScene"] = mainMenuMusic;
        music["Tutorial New"] = forestMusic;
        music["Forest"] = forestMusic;
        music["BlendLandLevel"] = blendLandsMusic;
        music["City"] = cityMusic;
        music["Thanks"] = thanksMusic;

        AudioListener.pause = false;
        sfxSource.ignoreListenerPause = true;
        DontDestroyOnLoad(this);
    }

    public void playMusic(AudioClip clip)
    {
        musicSource.Stop();
        musicSource.clip = clip;
        musicSource.Play();
    }

    public void playSFX(AudioClip clip)
    {
        sfxSource.PlayOneShot(clip);
    }

    public void AudioListenerToggle()
    {
        AudioListener listener = GetComponent<AudioListener>();
        listener.enabled = !listener.enabled;

        GameObject cameraTarget = GameObject.Find("CameraTarget");

        cameraTarget.GetComponent<AudioListener>().enabled = !cameraTarget.GetComponent<AudioListener>().enabled;
    }

    public void CrossfadeMusic(string clipName)
    {
        if (music[clipName] != null && musicSource.clip != music[clipName])
        {
            StartCoroutine(CrossFadeMusic(clipName));
        }
    }

    public IEnumerator CrossFadeMusic(string clipName)
    {
        while(musicSource.volume > 0)
        {
            musicSource.volume -= 0.02f;
            yield return null;
        }

        musicSource.Stop();
        musicSource.clip = music[clipName];
        musicSource.Play();

        while(musicSource.volume < 0.25)
        {
            musicSource.volume += 0.02f;
            yield return null;
        }
    }
}

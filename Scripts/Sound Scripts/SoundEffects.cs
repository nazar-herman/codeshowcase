﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    protected AudioSource a_source;

    protected Dictionary<string, AudioClip> sounds;

    public List<AudioClip> leftFoot;
    public List<AudioClip> rightFoot;

    public List<AudioClip> screams;

    protected bool scream = false;

    protected float timer;
    public float timeBetweenScreams = 2.0f;

    protected virtual void Awake()
    {
        a_source = GetComponent<AudioSource>();
        sounds = new Dictionary<string, AudioClip>();
    }

    public virtual void Update()
    {
        if(scream)
        {
            timer += Time.deltaTime;

            if(timer >= timeBetweenScreams)
            {
                scream = false;
                timer = 0.0f;
            }
        }
    }

    public virtual void PlayScreams()
    {
        if (screams.Count > 0)
        {
            if (!scream)
            {
                //a_source.Stop();
                Random.InitState((int)System.DateTime.Now.Ticks);
                int rand = Random.Range(0, screams.Count);
                scream = true;

                PlayClip(screams[rand]);
            }
        }
    }

    public virtual void PlayFootstep(string foot)
    {
        if (leftFoot.Count > 0)
        {
            if (foot == "Left")
            {
                Random.InitState((int)System.DateTime.Now.Ticks);
                int rand = Random.Range(0, leftFoot.Count);

                a_source.PlayOneShot(leftFoot[rand]);
            }
        }

        if (rightFoot.Count > 0)
        {
            if (foot == "Right")
            {
                Random.InitState((int)System.DateTime.Now.Ticks);
                int rand = Random.Range(0, rightFoot.Count);

                a_source.PlayOneShot(rightFoot[rand]);
            }
        }
    }

    public virtual void PlayClip(string clipName)
    {
        if (sounds[clipName] != null)
        {
            a_source.clip = sounds[clipName];
            a_source.Play();
        }
    }

    public virtual void PlayClip(AudioClip clip)
    {
        if (clip != null)
        {
            a_source.clip = clip;
            a_source.Play();
        }
    }

    public virtual void Play()
    {
        a_source.Play();
    }

    public virtual void PlayClipLooping(string clipName)
    {
        if (sounds[clipName] != null)
        {
            a_source.loop = true;
            a_source.clip = sounds[clipName];
            a_source.Play();
        }
    }

    public virtual void PlayClipLooping()
    {
        if(a_source.clip != null)
        {
            a_source.loop = true;
            a_source.Play();
        }
    }

    public virtual void PlayClipOneShot(string clipName)
    {
        if (sounds[clipName] != null)
        {
            a_source.PlayOneShot(sounds[clipName]);
        }
    }

    public virtual bool IsPlaying()
    {
        return a_source.isPlaying;
    }

    public virtual void SetClip(string clipName)
    {
        a_source.clip = sounds[clipName];
    }

    public virtual void StopClip()
    {
        a_source.loop = false;
        a_source.Stop();
    }
}

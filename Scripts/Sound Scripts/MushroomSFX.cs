﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomSFX : SoundEffects
{
    public AudioClip windUpSFX;
    public AudioClip attackSFX;
    public AudioClip abilitySFX;
    public AudioClip deathSFX;

    public AudioClip swordHit;

    protected override void Awake()
    {
        base.Awake();
        sounds.Add("WindUp", windUpSFX);
        sounds.Add("Attack", attackSFX);
        sounds.Add("Ability", abilitySFX);
        sounds.Add("Death", deathSFX);
        sounds.Add("SwordHit", swordHit);
    }
}

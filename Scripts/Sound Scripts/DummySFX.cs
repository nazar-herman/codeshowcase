﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummySFX : SoundEffects {

    [Space(10)]
    [Header("Other Sounds")]
    public AudioClip moveSFX;
    public AudioClip swordHit;

    protected override void Awake()
    {
        base.Awake();
        sounds.Add("Move", moveSFX);
        sounds.Add("SwordHit", swordHit);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSFX : SoundEffects {

    public AudioClip redSwing1SFX;
    public AudioClip redSwing2SFX;
    public AudioClip redSwing3SFX;
    public AudioClip redSwing4SFX;
    [Space(10)]
    public AudioClip blueSwing1SFX;
    public AudioClip blueSwing2SFX;
    [Space(10)]
    public AudioClip whiteSwing1SFX;
    public AudioClip whiteSwing2SFX;
    public AudioClip whiteSwing3SFX;
    [Space(10)]
    public AudioClip yellowSwing1SFX;
    public AudioClip yellowSwing2SFX;
    public AudioClip yellowSwing3SFX;
    [Space(10)]
    public AudioClip redAbilitySFX;
    public AudioClip blueAbilitySFX;
    public AudioClip yellowAbilitySFX;
    [Space(10)]
    public AudioClip rollSFX;
    [Space(10)]
    public AudioClip hitSFX;
    public AudioClip deathSFX;

    protected override void Awake()
    {
        base.Awake();
        sounds.Add("RedSwing1", redSwing1SFX);
        sounds.Add("RedSwing2", redSwing2SFX);
        sounds.Add("RedSwing3", redSwing3SFX);
        sounds.Add("RedSwing4", redSwing4SFX);

        sounds.Add("BlueSwing1", blueSwing1SFX);
        sounds.Add("BlueSwing2", blueSwing2SFX);

        sounds.Add("WhiteSwing1", whiteSwing1SFX);
        sounds.Add("WhiteSwing2", whiteSwing2SFX);
        sounds.Add("WhiteSwing3", whiteSwing3SFX);

        sounds.Add("YellowSwing1", yellowSwing1SFX);
        sounds.Add("YellowSwing2", yellowSwing2SFX);
        sounds.Add("YellowSwing3", yellowSwing3SFX);

        sounds.Add("RedAbility", redAbilitySFX);
        sounds.Add("BlueAbility", blueAbilitySFX);
        sounds.Add("YellowAbility", yellowAbilitySFX);

        sounds.Add("Roll", rollSFX);

        sounds.Add("Hit", hitSFX);

        sounds.Add("Death", deathSFX);
    }
}

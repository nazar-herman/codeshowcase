﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    public GameObject tutorialObject;

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<CharacterControl>() != null)
        {
            tutorialObject.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
}

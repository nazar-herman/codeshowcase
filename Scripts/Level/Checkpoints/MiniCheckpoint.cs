﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniCheckpoint : Interactable
{
    public float lightThreshold;

    CharacterControl player;
    LightController lc;
    HealthCtrlBase hc;

    Checkpoint parent;

    protected override void Awake()
    {
        base.Awake();
        parent = GetComponentInParent<Checkpoint>();
        usable = true;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        player = other.GetComponent<CharacterControl>();
        if (player != null)
        {
            lc = player.GetComponent<LightController>();
            hc = player.GetComponent<HealthCtrlBase>();

            if (lc.lightPool.Data < lightThreshold && usable)
            {
                base.OnTriggerEnter(other);
            }
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharacterControl>() != null)
        {
            player = null;
            lc = null;
            hc = null;
            base.OnTriggerExit(other);
        }
    }
    protected override void LateUpdate()
    {
        if (lc != null)
        {
            if (lc.lightPool.Data < lightThreshold && usable)
            {
                prompt = true;
            }
        }

        base.LateUpdate();
    }
    protected override void Use()
    {
        Restore();
        prompt = false;
        usable = false;
        promptObject.gameObject.SetActive(false);
        Managers.Instance.UI.inContext = false;
    }

    void Restore()
    {
        hc.healthData.Data += (int)(hc.maxhealth.Data * parent.healPercent);
        lc.lightPool.Data += (int)(lc.maxLight * parent.resourcePercent);
    }
}

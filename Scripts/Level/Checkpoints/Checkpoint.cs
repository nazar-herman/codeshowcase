﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public GameObject activeParticle;
    public AudioClip activeSound;
    public Color defaultColor;
    public Color activeColor;
    public float healPercent;
    public float resourcePercent;
    public Transform spawnPoint;
    private bool _active;

    MeshRenderer[] rends;
    Collider col;
    public bool Active
    {
        get { return _active; }
        set
        {
            _active = value;
            activeParticle.SetActive(value);

            if(value == true)
            {
                foreach (MeshRenderer m_ren in rends)
                {
                    m_ren.material.SetColor("_Color", activeColor);
                }
            }
            else
            {
                foreach (MeshRenderer m_ren in rends)
                {
                    m_ren.material.SetColor("_Color", defaultColor);
                }
            }
        }
    }

    void Awake()
    {
        rends = GetComponentsInChildren<MeshRenderer>();
        col = GetComponent<Collider>();

        foreach (MeshRenderer m_ren in rends)
        {
            m_ren.material.SetColor("_Color", defaultColor);
        }
    }

    protected virtual void Update()
    {

    }

    protected virtual void OnTriggerEnter(Collider other)
    {

        if (Managers.Instance.Checkpoint.currentCheckpoint != this)
        {
            CharacterControl player = other.GetComponent<CharacterControl>();

            if (player != null)
            {
                Managers.Instance.Checkpoint.SetNextCheckpoint(this);
                AudioManager.Instance.playSFX(activeSound);
            }
        }
    }

}

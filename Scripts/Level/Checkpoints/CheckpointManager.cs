﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    public Checkpoint currentCheckpoint;

    public void SetNextCheckpoint(Checkpoint checkpoint)
    {
        if (currentCheckpoint != null)
        {
            currentCheckpoint.Active = false;
            currentCheckpoint = null;
            currentCheckpoint = checkpoint;
            currentCheckpoint.Active = true;
        }
        else
        {
            currentCheckpoint = checkpoint;
            currentCheckpoint.Active = true;
        }
    }
}

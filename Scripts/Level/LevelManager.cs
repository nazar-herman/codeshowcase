﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    CharacterControl characterControl;
    PlayerHealthCtrl playerHealth;
    public GameObject camera;
    public CameraFade cameraFade;
    public float transitionTime;
    public bool loading;

    void Awake()
    {
        characterControl = FindObjectOfType<CharacterControl>();
        playerHealth = characterControl.GetComponent<PlayerHealthCtrl>();
    }


    public void BeginSceneTranstition(string sceneName, Vector3 newPos)
    {
        loading = true;
        characterControl.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        characterControl.paused = true;
        StartCoroutine(LoadScene(sceneName, newPos));
    }

    public void RestartAtCheckpoint()
    {
        Managers.Instance.UI.DisplayDeathPrompt(false);
        characterControl.paused = true;
        cameraFade.FadeToBlack();
        StartCoroutine(RestartCheckpoint());
    }

    IEnumerator RestartCheckpoint()
    {
        yield return new WaitForSeconds(transitionTime);
        playerHealth.Respawn();
        camera.transform.position = playerHealth.transform.position;
        cameraFade.FadeFromBlack();
        characterControl.paused = false;
        characterControl.switchingAllowed = true;
    }

    public void EndGame()
    {
        characterControl.paused = true;
        cameraFade.FadeToBlack();
        StartCoroutine(EndTheGame());
    }

    IEnumerator EndTheGame()
    {
        AudioManager.Instance.CrossfadeMusic("Thanks");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Thanks");
    }

    IEnumerator LoadScene(string sceneName, Vector3 newPos)
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;
        AsyncOperation sync;

        sync = SceneManager.LoadSceneAsync("LoadingScene", LoadSceneMode.Additive);
        while (!sync.isDone) { yield return null; }

        SceneLoad sceneLoad = FindObjectOfType<SceneLoad>();

        sceneLoad.BeginSceneTranstition(sceneName, newPos);
    }


}

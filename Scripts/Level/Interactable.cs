﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour {
    [Header("Events")]
    public List<EventData> events;
    [Space(10)]

    public Buttons.Button button;
    public GameObject promptObject;
    public float yOffset;
    public float cooldown;
    [HideInInspector]
    public bool usable;

    protected float cooldownTimer;
    protected bool prompt;
    protected Camera camera;

    protected virtual void Awake()
    {
        camera = Camera.main;

        if (promptObject != null)
        {
            promptObject.SetActive(false);
        }
    }

    protected virtual void LateUpdate()
    {
        if (!usable)
        {
            cooldownTimer += Time.deltaTime;
            if (cooldownTimer >= cooldown)
            {
                cooldownTimer = 0.0f;
                usable = true;
            }
        }
        else if (prompt && usable)
        {
            Managers.Instance.UI.inContext = true;
            if (promptObject != null)
            {
                if (!promptObject.activeInHierarchy)
                {
                    promptObject.transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);
                    promptObject.gameObject.SetActive(true);
                }
            }

            if (Input.GetButtonDown(Buttons.buttons[(int)button]))
            {
                Use();
            }
        }
    }

    //protected virtual void FixedUpdate()
    //{
    //    if(prompt && usable)
    //    {
    //        UpdatePosition();
    //    }
    //}

    //protected virtual void UpdatePosition()
    //{
    //    RectTransform rect = promptObject.GetComponent<RectTransform>();
    //    Vector3 pos = camera.WorldToScreenPoint(this.transform.position);
    //    rect.position = new Vector3(pos.x, pos.y + Screen.height * yOffsetPercent);
    //    if (!promptObject.activeInHierarchy)
    //    {
    //        promptObject.gameObject.SetActive(true);
    //        Managers.Instance.UI.inContext = true;
    //    }
    //}

    protected virtual void OnTriggerEnter(Collider other)
    {
        prompt = true;
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        
        prompt = false;
        if (promptObject != null)
        {
            promptObject.gameObject.SetActive(false);
        }
        Managers.Instance.UI.inContext = false;
    }

    protected virtual void Use()
    {
        foreach(EventData ev in events)
        {
            ev.thisEvent.Invoke();
        }
    }
}

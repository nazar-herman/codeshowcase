﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teleport : Interactable
{
    public LightUnlock doorToActivate;
    public Transform teleportPosition;
    LightController lc;

    protected override void Awake()
    {
        base.Awake();
        usable = true;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<LightController>() != null)
        {
            lc = other.GetComponent<LightController>();
            base.OnTriggerEnter(other);
        }
    }

    protected override void OnTriggerExit(Collider other)
    {

        if (other.GetComponent<LightController>() != null)
        {
            lc = null;
            base.OnTriggerExit(other);
        }
    }

    protected override void LateUpdate()
    {
        if (prompt && usable)
        {
            if (!promptObject.activeInHierarchy)
            {
                //promptObject.transform.localPosition = new Vector3(transform.position.x, transform.position.y + yOffset);
                promptObject.gameObject.SetActive(true);
                Managers.Instance.UI.inContext = true;
            }

            if (Input.GetButtonDown(Buttons.buttons[(int)button]))
            {
                Use();
            }
        }

    }

    protected override void Use()
    {
        prompt = false;
        usable = false;
        promptObject.gameObject.SetActive(false);
        lc.gameObject.transform.position = teleportPosition.position;
        Managers.Instance.UI.inContext = false;
        doorToActivate.gameObject.SetActive(true);
        doorToActivate.unlocked = false;
        doorToActivate.usable = true;
        Managers.Instance.Light.Reset();
    }
}

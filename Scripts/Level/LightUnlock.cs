﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightUnlock : Interactable
{

    public LightColor lightColor;
    public AudioClip lightUnlockSFX;
    [HideInInspector]
    public bool unlocked;

    LightController lc;

    protected override void Awake()
    {
        base.Awake();
        usable = true;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (!unlocked)
        {
            if (other.GetComponent<LightController>() != null)
            {
                lc = other.GetComponent<LightController>();
                base.OnTriggerEnter(other);
            }
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        if (!unlocked)
        {
            if (other.GetComponent<LightController>() != null)
            {
                lc = null;
                base.OnTriggerExit(other);
            }
        }
    }

    protected override void LateUpdate()
    {
        if (prompt && usable)
        {
            Managers.Instance.UI.inContext = true;

            if (promptObject != null)
            {
                if (!promptObject.activeInHierarchy)
                {
                    //promptObject.transform.localPosition = new Vector3(transform.position.x, transform.position.y + yOffset);
                    promptObject.gameObject.SetActive(true);
                }
            }

            if (Input.GetButtonDown(Buttons.buttons[(int)button]))
            {
                Use();
            }
        }

    }

    protected override void Use()
    {
        Managers.Instance.Light.Unlock(lightColor);
        prompt = false;
        usable = false;

        if (promptObject != null)
        {
            promptObject.gameObject.SetActive(false);
        }

        unlocked = true;
        Managers.Instance.UI.inContext = false;
        lc.lightColor = lightColor;
        lc.GetComponent<CombatStateControl>().OnLightChanged();
        base.Use();
        this.gameObject.SetActive(false);
        AudioManager.Instance.playSFX(lightUnlockSFX);
        Managers.Instance.UI.TutorialFadeOut();
    }
}

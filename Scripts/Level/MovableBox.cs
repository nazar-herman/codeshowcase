﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableBox : Interactable
{
    public float pushSpeed;
    public Transform snapPosFront;
    public Transform snapPosBack;
    public Transform snapPosLeft;
    public Transform snapPosRight;

    private Transform currentSnap;

    CharacterControl player;

    public bool attached;

    Rigidbody rb;

    protected override void Awake()
    {
        base.Awake();
        usable = true;
        rb = GetComponent<Rigidbody>();
    }

    protected override void LateUpdate()
    {
        if (prompt && usable)
        {
            if (!promptObject.activeInHierarchy)
            {
                promptObject.transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);
                promptObject.gameObject.SetActive(true);
                Managers.Instance.UI.inContext = true;
            }

            if (Input.GetButtonDown(Buttons.buttons[(int)button]))
            {
                Use();
            }
        }

        if(attached)
        {
            if(Input.GetButtonDown(Buttons.B))
            {
                attached = false;
                usable = true;
                promptObject.gameObject.SetActive(true);
                //player.Grab = false;
                Destroy(player.GetComponent<SpringJoint>());
            }

            if(player.anim.GetFloat("inputY") >= 0.5f)
            {
                rb.MovePosition(transform.position + currentSnap.forward * pushSpeed);
            }

            if(player.anim.GetFloat("inputY") <= -0.5f)
            {
                rb.MovePosition(transform.position + -currentSnap.forward * pushSpeed);
            }
        }
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<CharacterControl>() != null)
        {
            player = other.gameObject.GetComponent<CharacterControl>();

            base.OnTriggerEnter(other);
        }
    }

    protected override void OnTriggerExit(Collider other)
    {

        if (other.GetComponent<CharacterControl>() != null)
        {
            player = null;
            base.OnTriggerExit(other);
        }

    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<CharacterControl>() != null)
        {
            Vector3 point = transform.InverseTransformPoint(other.transform.position);
            Vector3 localDir = point.normalized;
            float fwdDot = Vector3.Dot(localDir, Vector3.forward);
            float rightDot = Vector3.Dot(localDir, Vector3.right);
            float leftDot = Vector3.Dot(localDir, -Vector3.right);
            float backDot = Vector3.Dot(localDir, -Vector3.forward);

            if (Mathf.Max(fwdDot, rightDot, leftDot, backDot) == fwdDot)
            {
                currentSnap = snapPosFront;
            }

            if (Mathf.Max(fwdDot, rightDot, leftDot, backDot) == leftDot)
            {
                currentSnap = snapPosLeft;
            }

            if (Mathf.Max(fwdDot, rightDot, leftDot, backDot) == rightDot)
            {
                currentSnap = snapPosRight;
            }

            if (Mathf.Max(fwdDot, rightDot, leftDot, backDot) == backDot)
            {
                currentSnap = snapPosBack;
            }
        }
    }

    protected override void Use()
    {
        attached = true;
        usable = false;
        player.transform.position = new Vector3(currentSnap.position.x, player.transform.position.y, currentSnap.position.z);
        player.transform.rotation = currentSnap.rotation;
        player.anim.SetInteger("doGrab", 10);
        //player.Grab = true;
        player.gameObject.AddComponent<SpringJoint>();
        SpringJoint joint = player.GetComponent<SpringJoint>();
        joint.connectedBody = rb;
        joint.spring = 2000;
        joint.damper = 100;
        joint.minDistance = 0.0f;
        joint.maxDistance = 0.0f;
        promptObject.gameObject.SetActive(false);
    }
}
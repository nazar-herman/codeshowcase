﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    CharacterControl characterControl;
    PlayerHealthCtrl playerHealth;
    public GameObject camera;
    public CameraFade cameraFade;
    public GameObject loadingIcon;
    public float transitionTime;
    float timer;

    bool doneLoad;
    [HideInInspector]
    public bool loading;

    float loadTimer = 0.0f;

    void Awake()
    {
        characterControl = FindObjectOfType<CharacterControl>();
        if (characterControl != null)
        {
            playerHealth = characterControl.GetComponent<PlayerHealthCtrl>();
        }
        camera = GameObject.Find("CameraTarget");
    }

    void Update()
    {
        if (doneLoad)
        {
            cameraFade.FadeFromBlack();
            doneLoad = false;
        }

        if (loading)
        {
            loadTimer += Time.deltaTime;
        }
    }

    public void BeginSceneTranstition(string sceneName, Vector3 newPos)
    {
        loading = true;
        cameraFade.FadeToBlack();
        StartCoroutine(LoadScene(sceneName, newPos));
    }

    IEnumerator LoadScene(string sceneName, Vector3 newPos)
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;
        AsyncOperation sync;

        yield return new WaitForSeconds(transitionTime);

        loadingIcon.SetActive(true);

        bool doToggle = false;

        if(SceneManager.GetActiveScene().name == "TitleScene")
        {
            doToggle = true;
        }

        sync = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        while (!sync.isDone) { yield return null; }

        sync = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        while (!sync.isDone || loadTimer < 3.0f + transitionTime){ yield return null;}

        Application.backgroundLoadingPriority = ThreadPriority.Normal;

        AudioManager.Instance.CrossfadeMusic(sceneName);

        while(GameObject.Find("CameraTarget") == null) { yield return null; }

        if (doToggle)
        {
            AudioManager.Instance.AudioListenerToggle();
        }

        loading = false;

        if(Managers.Instance != null)
        {
            Managers.Instance.Level.loading = false;
        }

        loadingIcon.SetActive(false);
        loadTimer = 0.0f;

        if (characterControl != null)
        {
            characterControl.transform.position = newPos;
            camera.transform.position = newPos;
            characterControl.paused = false;
            characterControl.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            characterControl.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        }

        Scene scene = SceneManager.GetSceneByName(sceneName);
        SceneManager.SetActiveScene(scene);
        doneLoad = true;

        yield return new WaitForSeconds(transitionTime);
        SceneManager.UnloadSceneAsync("LoadingScene");
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeTransparent : MonoBehaviour
{
    public enum BlendMode
    {
        Opaque = 0,
        Cutout = 1,
        Fade = 2,
        Transparent = 3
    }

    float _transparency = 0.99f;
    float _fadeInTime = 1.0f;

    List<RenderMaterials> renderMaterials;
    Renderer[] renderers;

    public bool doTransparency;
    List<bool> emmisions;
    void Start()
    {
        renderers = GetComponentsInChildren<Renderer>();
        emmisions = new List<bool>();
        renderMaterials = new List<RenderMaterials>();

        for (int i = 0; i < renderers.Length; i++)
        {
            RenderMaterials r_mat = new RenderMaterials();
            r_mat.renderer = renderers[i];
            r_mat.materials = renderers[i].materials;
            r_mat.blendMode = new BlendMode[r_mat.materials.Length];
            renderMaterials.Add(r_mat);
        }
        for (int i = 0; i < renderMaterials.Count; i++)
        {
            for (int j = 0; j < renderMaterials[i].materials.Length; j++)
            {
                renderMaterials[i].blendMode[j] = (BlendMode)((int)renderMaterials[i].materials[j].GetFloat("_Mode"));
                renderMaterials[i].materials[j].SetFloat("_Mode", 2);
                ChangeRenderMode(renderMaterials[i].materials[j], BlendMode.Fade);
                bool emits = renderMaterials[i].materials[j].IsKeywordEnabled("_EMISSION");
                if(emits)
                {
                    renderMaterials[i].materials[j].DisableKeyword("_EMISSION");
                }
                emmisions.Add(emits);
            }
        }


    }

    public void DoTransparency()
    {
        doTransparency = true;
    }

    void Update()
    {
        if (doTransparency)
        {
            _transparency = Mathf.Lerp(_transparency, 0.1f,Time.deltaTime * 3.0f);

            for (int i = 0; i < renderMaterials.Count; i++)
            {
                for (int j = 0; j < renderMaterials[i].materials.Length; j++)
                {
                    Color color = renderMaterials[i].materials[j].color;
                    color.a = _transparency;
                    renderMaterials[i].materials[j].color = color;
                }
            }
        }
        else
        {
            _transparency = Mathf.Lerp(_transparency, 1.0f, Time.deltaTime * 3.0f);
            int k = 0;
            for (int i = 0; i < renderMaterials.Count; i++)
            {
                for (int j = 0; j < renderMaterials[i].materials.Length; j++)
                {
                    Color color = renderMaterials[i].materials[j].color;
                    color.a = _transparency;
                    renderMaterials[i].materials[j].color = color;

                    if (_transparency > 0.99f)
                    {
                        renderMaterials[i].materials[j].SetFloat("_Mode", (int)renderMaterials[i].blendMode[j]);
                        if(emmisions[k])
                        {
                            renderMaterials[i].materials[j].EnableKeyword("_EMISSION");
                        }
                        k++;
                        ChangeRenderMode(renderMaterials[i].materials[j], renderMaterials[i].blendMode[j]);
                    }
                }
            }
        }

        if(_transparency > 0.99f)
        {
            Destroy(this);
        }

        doTransparency = false;
    }

    public static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 2450;
                break;
            case BlendMode.Fade:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 3000;
                break;
            case BlendMode.Transparent:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                standardShaderMaterial.SetInt("_ZWrite", 0);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 3000;
                break;
        }

    }

}

public class RenderMaterials
{
    public Renderer renderer;
    public Material[] materials;
    public MakeTransparent.BlendMode[] blendMode;
}

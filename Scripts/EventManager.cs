﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Linq;

public class EventManager : MonoBehaviour
{
    private List<EventData> events;

    void Awake()
    {
        events = new List<EventData>();
    }

    public void StartListening(string name, UnityAction listener)
    {
        bool exists = false;

        foreach (EventData data in events)
        {
            if (data.eventName == name)
            {
                exists = true;
                data.thisEvent.AddListener(listener);
                break;
            }
        }

        if (!exists)
        {
            EventData newEvent = new EventData(name, new UnityEvent());
            newEvent.thisEvent.AddListener(listener);
            events.Add(newEvent);
        }
    }

    public void StartListening(EventData eventData)
    {
        bool exists = false;

        foreach (EventData data in events)
        {
            if (data.eventName == eventData.eventName)
            {
                exists = true;
                data.thisEvent = eventData.thisEvent;
                break;
            }
        }

        if (!exists)
        {
            EventData newEvent = new EventData(eventData.eventName, new UnityEvent());
            newEvent.thisEvent = eventData.thisEvent;
            events.Add(newEvent);
        }
    }

    public void StopListening(string name)
    {
        bool found = false;
        foreach(EventData data in events)
        {
            if(data.eventName == name)
            {
                found = true;
                break;
            }
        }

        if(found)
        {
            events.RemoveAll(x => x.eventName == name);
        }
    }

    public void TriggerEvent(string name)
    {
        foreach(EventData data in events)
        {
            if(data.eventName == name)
            {
                data.thisEvent.Invoke();
                break;
            }
        }
    }
}

[System.Serializable]
public class EventData
{
    public EventData(string name, UnityEvent listener)
    {
        eventName = name;
        thisEvent = listener;
    }

    public string eventName;
    public UnityEvent thisEvent;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    
    public bool hasTarget;

    private Transform moveTarget;

    public Transform Player;
    public float speed = 2.0f;
    void Awake()
    {
        Player = FindObjectOfType<CharacterControl>().transform;
    }

	void FixedUpdate()
    {
        if (!Player.GetComponent<HealthCtrlBase>().dead && !hasTarget)
        {
            transform.position = Vector3.Lerp(transform.position, Player.position, speed * Time.fixedDeltaTime);
        }
    }
    
    void Update()
    {
        if (hasTarget)
        {
            transform.position = Vector3.Lerp(transform.position, moveTarget.position, 10.0f * 0.002f);

            if (Vector3.Distance(transform.position, moveTarget.position) <= 0.05f)
            {
                hasTarget = false;
                moveTarget = null;
            }
        }
    }

    public void DoCameraMove(Transform target)
    {
        moveTarget = target;
        hasTarget = true;
    }
}

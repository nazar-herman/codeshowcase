﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyHealthCtrl : HealthCtrlBase {

    public GameObject particle;
    public float moveSpeed;
    public List<Transform> waypoints;
    DummyBehaviour dummyBehaviour;
    int index = 0;
    bool move;
    DummySFX dummy_SFX;
    public override void Awake()
    {
        dummy_SFX = GetComponent<DummySFX>();

        if (waypoints != null && waypoints.Count > 0)
        {
            transform.position = waypoints[0].position;
        }

        dummyBehaviour = GetComponent<DummyBehaviour>();
    }

    public override void Update()
    {
        particle.SetActive(move);

        if(move)
        {
            if(Vector3.Distance(waypoints[index].position, transform.position) < 0.1f)
            {
                transform.position = waypoints[index].position;
                move = false;

                if(dummy_SFX.IsPlaying())
                {
                    dummy_SFX.StopClip();
                }
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, waypoints[index].position, moveSpeed * Time.deltaTime);
            }
        }
    }

    public override void TakeHit(int damage, LightColor hitColor)
    {
        dummy_SFX.PlayClipOneShot("SwordHit");
        dummy_SFX.SetClip("Move");

        if (waypoints != null && waypoints.Count > 0)
        {
            index++;
            if (index >= waypoints.Count)
            {
                index = 0;
            }
            move = true;
            dummyBehaviour.hits++;

            dummy_SFX.PlayClipLooping();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHealthCtrl : HealthCtrlBase {
    TargetFinder finder;
    NavMeshAgent agent;
    SoundEffects sfx;
    public Color flashColor;
    public float lerpSpeed;
    public float flashTime;
    List<Renderer> renderers;
    Color defaultColor;
    float lerpTimer;
    bool flashing;

    public override void Awake()
    {
        base.Awake();
        finder = GetComponentInChildren<TargetFinder>();
        agent = GetComponent<NavMeshAgent>();
        sfx = GetComponent<SoundEffects>();

        Renderer[] renderersArr = GetComponentsInChildren<Renderer>();
        renderers = new List<Renderer>(renderersArr);
        defaultColor = renderers[0].material.color;
    }

    public override void Update()
    {
        base.Update();

        if (flashing)
        {
            if (lerpTimer <= 0.0f)
            {
                flashing = false;
                foreach (Renderer rend in renderers)
                {
                    rend.material.color = defaultColor;
                }
            }
            if (lerpTimer <= flashTime / 2.0f)
            {
                foreach (Renderer rend in renderers)
                {
                    foreach (Material mat in rend.materials)
                    {
                        mat.color = Color.Lerp(rend.material.color, defaultColor, Time.deltaTime * lerpSpeed);
                    }
                }
            }
            else
            {
                foreach (Renderer rend in renderers)
                {
                    foreach (Material mat in rend.materials)
                    {
                        mat.color = Color.Lerp(rend.material.color, flashColor, Time.deltaTime * lerpSpeed);
                    }
                }
            }
            lerpTimer -= Time.deltaTime;
        }
    }

    public override void TakeHit(int damage, LightColor hitColor)
    {
        base.TakeHit(damage, hitColor);
        if (invincible)
        {
            return;
        }
        if (!dead)
        {
            if (_hit)
            {
                anim.SetTrigger("Hit");
            }

            if (effectCtrl != null)
            {
                effectCtrl.spawnHitParticle();
            }
            if(finder != null && finder.decoyTarget != null)
            {
                StatueControl statue = finder.decoyTarget.GetComponent<StatueControl>();
                if(statue!=null)
                {
                    statue.revealed = true;
                }
            }
            float multiplier = 1.0f;
            KeyValuePair<LightColor, LightColor> colorPair = new KeyValuePair<LightColor, LightColor>(hitColor, color);
            if (LightEffectivenessChart.lightChart.ContainsKey(colorPair))
            {
                multiplier = LightEffectivenessChart.lightChart[colorPair];
            }

            if ((int)(damage * multiplier) > 0 && !flashing)
            {
                lerpTimer = flashTime;
                flashing = true;
            }

            healthData.Data -= (int)(damage * multiplier);
            if(healthData.Data <= 0)
            {
                if (targetable != null)
                {
                    if (targetable.targeted)
                    {
                        targetable.DeathCleanup();
                    }
                }
            }

            sfx.PlayScreams();
            sfx.PlayClipOneShot("SwordHit");
        }
    }

    public override void Respawn()
    {
        base.Respawn();
    }

    public override void AddKnockback(float force, Vector3 direction)
    {
        base.AddKnockback(force, direction);
        if(invincible)
        {
            return;
        }
        direction = direction.normalized;
        rb.AddForce(force * direction, ForceMode.Impulse);
    }
}

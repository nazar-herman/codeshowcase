﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HealthCtrlBase : MonoBehaviour {

    protected EffectController effectCtrl;
    protected Animator anim;
    protected Rigidbody rb;
    [HideInInspector]
    public IntData healthData;
    [HideInInspector]
    public IntData maxhealth;
    public LightColor color;
    protected DeathCtrlBase deathCtrl;
    protected Targetable targetable;
    [HideInInspector]
    public bool dead;
    protected bool _hit;
    public bool invincible = false;



    public float HealthPercent
    {
        get { return (float)healthData.Data / maxhealth.Data; }
    }
   
	// Use this for initialization
	public virtual void Awake ()
    {
        IntData[] intDatas = GetComponents<IntData>();
        foreach(IntData data in intDatas)
        {
            if(data.Name == "Health")
            {
                healthData = data;
                continue;
            }
            if(data.Name == "MaxHealth")
            {
                maxhealth = data;
                continue;
            }
        }
        deathCtrl = GetComponent<DeathCtrlBase>();
        targetable = GetComponent<Targetable>();
        effectCtrl = GetComponent<EffectController>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

        if(anim != null)
        {
            _hit = anim.hasParameter("Hit");
        }
    }
	
	// Update is called once per frame
	public virtual void Update ()
    {
        if (!dead)
        {
            if(healthData.Data > maxhealth.Data)
            {
                healthData.Data = maxhealth.Data;
            }

            if (healthData.Data <= 0)
            {
                dead = true;
                if(anim != null)
                {
                    anim.SetTrigger("dead");
                }

                if (deathCtrl != null)
                {
                    deathCtrl.Die();
                }
            }
        }

	}

    public virtual void TakeHit(int damage, LightColor hitColor)
    {

    }

    public virtual void Respawn()
    {
       
    }

    public virtual void AddKnockback(float force, Vector3 direction)
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthCtrl : HealthCtrlBase {

    private bool Dead
    {
        get
        {
            return dead;
        }
        set
        {
            dead = value;
            StartCoroutine(Managers.Instance.UI.DeathPromptDelay());
            //do something with the audio manager
        }
    }
    private CharacterControl charCtrl;
    CharacterSFX charSFX;
    // Update is called once per frame
    public override void Awake()
    {
        base.Awake();
        charCtrl = GetComponent<CharacterControl>();
        charSFX = GetComponent<CharacterSFX>();
    }

    public override void Update()
    {
        if (!Dead)
        {
            if (healthData.Data > maxhealth.Data)
            {
                healthData.Data = maxhealth.Data;
            }

            if (healthData.Data <= 0)
            {
                Dead = true;
                if (anim != null)
                {
                    anim.SetTrigger("dead");
                }

                if (deathCtrl != null)
                {
                    deathCtrl.Die();
                }
            }
        }

        if (GetComponent<CharacterControl>() != null)
        {
            if (Dead)
            {
                GetComponent<Rigidbody>().isKinematic = true;
                GetComponent<Collider>().enabled = false;
                GetComponent<CombatStateControl>().ResetAll();
            }
        }
    }

    public override void TakeHit(int damage, LightColor hitColor)
    {

        base.TakeHit(damage, hitColor);
        if (invincible)
        {
            return;
        }
        if (!dead)
        {
            if (effectCtrl != null)
            {
                effectCtrl.spawnHitParticle();
            }
            float multiplier = 1.0f;
            KeyValuePair<LightColor, LightColor> colorPair = new KeyValuePair<LightColor, LightColor>(hitColor, color);
            if (LightEffectivenessChart.lightChart.ContainsKey(colorPair))
            {
                multiplier = LightEffectivenessChart.lightChart[colorPair];
            }
            healthData.Data -= (int)(damage * multiplier);

            if (healthData.Data <= 0)
            {
                charSFX.PlayClipOneShot("Death");
            }
            else
            {
                if (_hit && !charCtrl.stunned)
                {
                    anim.SetTrigger("Hit");
                }
            }

            charSFX.PlayClipOneShot("Hit");
        }
    }

    public override void Respawn()
    {
        base.Respawn();

        dead = false;
        healthData.Data = maxhealth.Data;
        anim.SetTrigger("respawn");
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Collider>().enabled = true;
        transform.position = Managers.Instance.Checkpoint.currentCheckpoint.spawnPoint.position;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalHealthCtrl : HealthCtrlBase {
    public Color flashColor;
    public float lerpSpeed;
    public float flashTime;
    List<Renderer> renderers;
    Color defaultColor;
    float lerpTimer;
    bool flashing;
	// Use this for initialization
	void Start () {
        Renderer[] renderersArr = GetComponentsInChildren<Renderer>();
        renderers = new List<Renderer>(renderersArr);
        defaultColor = renderers[0].material.color;
	}

    // Update is called once per frame
    public override void Awake()
    {
        base.Awake();
    }

    public override void Update()
    {
        base.Update();
        if(flashing)
        {
            if(lerpTimer <= 0.0f)
            {
                flashing = false;
                foreach(Renderer rend in renderers)
                {
                    rend.material.color = defaultColor;
                }
            }
            if(lerpTimer <= flashTime/2.0f)
            {
                foreach (Renderer rend in renderers)
                {
                    foreach (Material mat in rend.materials)
                    {
                        mat.color = Color.Lerp(rend.material.color, defaultColor, Time.deltaTime * lerpSpeed);
                    }
                }
            }
            else
            {
                foreach (Renderer rend in renderers)
                {
                    foreach (Material mat in rend.materials)
                    {
                        mat.color = Color.Lerp(rend.material.color, flashColor, Time.deltaTime * lerpSpeed);
                    }
                }
            }
            lerpTimer -= Time.deltaTime;
        }
    }

    public override void TakeHit(int damage, LightColor hitColor)
    {
        base.TakeHit(damage, hitColor);
        if (invincible)
        {
            return;
        }
        if (!dead)
        {

            if (effectCtrl != null)
            {
                effectCtrl.spawnHitParticle();
            }
            float multiplier = 1.0f;
            KeyValuePair<LightColor, LightColor> colorPair = new KeyValuePair<LightColor, LightColor>(hitColor, color);
            if (LightEffectivenessChart.crystalChart.ContainsKey(colorPair))
            {
                multiplier = LightEffectivenessChart.crystalChart[colorPair];
            }
            if((int)(damage * multiplier) > 0 && !flashing)
            {
                lerpTimer = flashTime;
                flashing = true;
            }
            healthData.Data -= (int)(damage * multiplier);
        }
    }

    public override void Respawn()
    {
        base.Respawn();
    }
}


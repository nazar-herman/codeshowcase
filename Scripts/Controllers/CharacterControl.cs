﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CharacterControl : MonoBehaviour
{

    public float movementSpeed;
    [HideInInspector]
    public float maxMovementSpeed;
    public float rollSpeed;

    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public HealthCtrlBase health;
    [HideInInspector]
    public bool paused = false;
    [HideInInspector]
    public bool switchingAllowed = true;
    [HideInInspector]
    public float stunTimer;
    private Rigidbody rb;
    private CombatStateControl combatControl;
    private TargetControl targetControl;
    private LightController lightController;
    private Vector2 stickInput;
    [HideInInspector]
    public bool stunned;
    private float timer;
    private float stopTime = 0.1f;

    private CharacterSFX characterSFX;

    [HideInInspector]
    public bool atk = false;
    float atkTime = 0.17f;
    float atkTimer;
	void Awake()
    {
        anim = GetComponent<Animator>();
        combatControl = GetComponent<CombatStateControl>();
        targetControl = GetComponent<TargetControl>();
        lightController = GetComponent<LightController>();
        rb = GetComponent<Rigidbody>();
        health = GetComponent<HealthCtrlBase>();
        maxMovementSpeed = movementSpeed;
        characterSFX = GetComponent<CharacterSFX>();
    }

    void Update()
    {

        if(atk)
        {
            atkTimer += Time.deltaTime;
            if(atkTimer >= atkTime)
            {
                atk = false;
                atkTimer = 0.0f;
            }
        }
        
        if(stunTimer >= 0.0f && stunned)
        {
            stunTimer -= Time.deltaTime;
        }
        if(stunTimer < 0.0f && stunned)
        {
            EndStun();
        }
        if (!Managers.Instance.UI.inMenu && !Managers.Instance.UI.inTutorial && !health.dead && !paused && !stunned)
        {
            //DoLockOn();
            DoLightSelection();

            if (!atk)
            {
                DoAttack();
            }
            DoRoll();
        }
        else
        {
            anim.SetFloat("inputMag", 0.0f);
            anim.SetBool("canStop", true);
        }
        anim.SetFloat("MoveSpeed", movementSpeed / maxMovementSpeed);
    }
    void FixedUpdate ()
    {
        if (!Managers.Instance.UI.inMenu && !Managers.Instance.UI.inTutorial && !health.dead && !paused && !stunned)
        {
            DoMovement();
        }
    }
    void DoAttack()
    {
        if (Input.GetButtonDown(Buttons.X))
        {
            combatControl.HandleCombatInput();
        }
    }

    void DoMovement()
    {
        stickInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (stickInput.magnitude < 0.25f)
        {
            if (!anim.GetBool("canStop"))
            {
                timer += Time.deltaTime;
                if (timer >= stopTime)
                {
                    timer = 0.0f;
                    stickInput = Vector2.zero;
                    anim.SetBool("canStop", true);
                }
            }
        }
        else
        {
            anim.SetBool("canStop", false);
            timer = 0.0f;
        }


        anim.SetFloat("inputMag", stickInput.magnitude);


        if (stickInput.magnitude > 0.25f && combatControl.CanDoMovement)
        {
            Vector3 stickDir = new Vector3(stickInput.x, 0.0f, stickInput.y);

            stickDir = Quaternion.Euler(new Vector3(0, 45, 0)) * stickDir;

            float dirToGo = Mathf.Atan2(stickInput.x, stickInput.y) * Mathf.Rad2Deg + 45.0f;
            float t = 0.3f;

            float dot = Vector3.Dot(transform.forward.normalized, stickDir.normalized);

            if (dot < -0.7f)
            {
                t = 0.5f;
            }
            else if(dot > 0 && dot < 0.5)
            {
                t = 0.4f;
            }

            float y = Mathf.LerpAngle(transform.eulerAngles.y, dirToGo, t);

            transform.eulerAngles = new Vector3(transform.eulerAngles.x, y, transform.eulerAngles.z);

            //if (Vector3.Dot(transform.forward.normalized, stickDir.normalized) > 0.5f)
            //{
            //    rb.MovePosition(transform.position + (transform.forward * movementSpeed * Time.deltaTime));
            //}

            rb.MovePosition(transform.position + (stickDir.normalized * movementSpeed * Time.deltaTime));
        }
    }

    void DoRoll()
    {
        if (!Managers.Instance.UI.inContext)
        {
            if (Input.GetButtonDown(Buttons.A))
            {
                combatControl.HandleRoll(stickInput);
            }
        }
    }

    void DoLightSelection()
    {
        if(switchingAllowed)
        {
            if (Input.GetButtonDown(Buttons.LB))
            {
                lightController.SelectPreviousLight();
                combatControl.OnLightChanged();
            }
            if (Input.GetButtonDown(Buttons.RB))
            {
                lightController.SelectNextLight();
                combatControl.OnLightChanged();
            }
            health.color = lightController.lightColor;
        }
    }

    void DoLockOn()
    {
        if(Input.GetButtonDown(Buttons.RS))
        {
            targetControl.Lock();
        }
    }

    void DoGrab()
    {
        float inputY = Input.GetAxis("Vertical");
        anim.SetFloat("inputY", inputY);
    }

    public void Stun(float time)
    {
        if(stunned)
        {
            return;
        }
        paused = true;
        anim.SetTrigger("Stun");
        anim.SetBool("Stunned", true);
        stunTimer = time;
        stunned = true;
    }

    void EndStun()
    {
        paused = false;
        anim.SetBool("Stunned", false);
        stunned = false;
    }
}

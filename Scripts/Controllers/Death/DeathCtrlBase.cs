﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCtrlBase : MonoBehaviour {

    protected Animator anim;
    protected Rigidbody rb;
    protected Collider col;
	// Use this for initialization
	public virtual void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
	}
	
	public virtual void Die()
    {

    }
}

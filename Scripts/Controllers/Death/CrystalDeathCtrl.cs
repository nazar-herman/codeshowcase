﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalDeathCtrl : DeathCtrlBase {

    public List<GameObject> containedObjects;
    public GameObject deathParticle;
    public override void Die()
    {
        base.Die();
        UnlockContained();
        Instantiate(deathParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }
    // Use this for initialization
    void Start () {
        LockContained();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void LockContained()
    {
        foreach (GameObject obj in containedObjects)
        {
            Collider[] colliders = obj.GetComponentsInChildren<Collider>();
            foreach (Collider col in colliders)
            {
                col.enabled = false;
            }
        }
    }

    void UnlockContained()
    {
        foreach (GameObject obj in containedObjects)
        {
            Collider[] colliders = obj.GetComponentsInChildren<Collider>();
            foreach (Collider col in colliders)
            {
                col.enabled = true;
            }
        }
    }
}

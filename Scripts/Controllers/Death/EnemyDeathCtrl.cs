﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathCtrl : DeathCtrlBase {

    public float destroyAfterTime;
    public List<GameObject> dropItems;
    public EnemyBehaviourBase enemy;
    public override void Start()
    {
        base.Start();
        enemy = GetComponent<EnemyBehaviourBase>();
    }
    // Update is called once per frame
    public override void Die()
    {
        rb.isKinematic = true;
        col.enabled = false;
        foreach(GameObject go in dropItems)
        {
            Instantiate(go, transform.position + transform.up, go.transform.rotation);
        }
        if(enemy != null)
        {
            Destroy(enemy.hitBox);
        }
        anim.SetTrigger("die");
        anim.SetBool("dead", true);
        Destroy(gameObject, destroyAfterTime);
    }
}

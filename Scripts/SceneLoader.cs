﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    void Awake()
    {
        Scene scene = SceneManager.GetSceneByName("GLOBAL");

        if (!scene.isLoaded)
        {
            StartCoroutine(LoadGlobal());
        }
    }

    IEnumerator LoadGlobal()
    {

        AsyncOperation sync = SceneManager.LoadSceneAsync("GLOBAL", LoadSceneMode.Additive);
        while (!sync.isDone) { yield return null; }
        Destroy(this.gameObject);
    }
}

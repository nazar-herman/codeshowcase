﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{
    LightController lightControl;
    UIResourceControl uiResourceControl;
    public void Awake()
    {
        lightControl = FindObjectOfType<LightController>();
        uiResourceControl = FindObjectOfType<UIResourceControl>();
    }
    public void Unlock(LightColor color)
    {
        lightControl.unlocks[(int)color] = true;
        if((int)color > 0)
        {
            uiResourceControl.AbilityImages[(int)color - 1].SetActive(true);
        }
    }

    public void Reset()
    {
        for (int i = 0; i < 4; i++)
        {
            lightControl.unlocks[i] = false;
        }

        //lightControl.unlocks[(int)LightColor.White] = true;
        lightControl.lightColor = LightColor.White;
        lightControl.GetComponent<CombatStateControl>().OnLightChanged();
    }
}

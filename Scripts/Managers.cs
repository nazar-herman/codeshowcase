﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour
{
    private static Managers _ins;

    public static Managers Instance
    {
        get
        {
            return _ins;
        }
    }

    public UIManager UI;
    public LevelManager Level;
    public CheckpointManager Checkpoint;
    public LightManager Light;
    public EventManager Event;

    void Awake()
    {
        if(_ins == null)
        {
            _ins = this;
        }

        UI = GetComponent<UIManager>();
        Level = GetComponent<LevelManager>();
        Checkpoint = GetComponent<CheckpointManager>();
        Light = GetComponent<LightManager>();
        Event = GetComponent<EventManager>();
    }
}

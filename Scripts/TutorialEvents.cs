﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TutorialEvents : MonoBehaviour
{
    [Header("Tutorial Events")]
    public List<EventData> events;
    [Space(5)]
    [Header("Tutorial Data")]
    public List<TutorialData> tutorialData;

    private int current;
    private bool tutorialEventStarted;

    private CameraFollow camera;

    void Awake()
    {
        camera = FindObjectOfType<CameraFollow>();
    }

    void OnEnable()
    {
        current = 0;

        if (events.Count > 0)
        {
            foreach (EventData data in events)
            {
                Managers.Instance.Event.StartListening(data);
            }
        }

        Managers.Instance.Event.StartListening("startTutorial", StartTutorialEvent);
        Managers.Instance.Event.StartListening("accept", AcceptButtonEvent);
        Managers.Instance.Event.StartListening("endTutorial", EndTutorialEvent);

        Managers.Instance.UI.ResetTutorialText();

        Managers.Instance.Event.TriggerEvent("startTutorial");
        foreach (string m_event in tutorialData[current].eventsToTrigger)
        {
            Managers.Instance.Event.TriggerEvent(m_event);
        }
    }

    void OnDisable()
    {
        if (events.Count > 0)
        {
            foreach (EventData data in events)
            {
                Managers.Instance.Event.StopListening(data.eventName);
            }
        }

        Managers.Instance.Event.StopListening("startTutorial");
        Managers.Instance.Event.StopListening("accept");
        Managers.Instance.Event.StopListening("endTutorial");
    }

    public void StartTutorialEvent()
    {
        tutorialEventStarted = true;
        Managers.Instance.UI.SetupTutorialCanvas(tutorialData[current].tutorialText, tutorialData[current].acceptText);
        Managers.Instance.UI.DisplayTutorial(true);
    }

    public void AcceptButtonEvent()
    {
        if (current != tutorialData.Count - 1)
        {
            current++;
            Managers.Instance.UI.ResetTutorialText();
            Managers.Instance.UI.SetupTutorialCanvas(tutorialData[current].tutorialText, tutorialData[current].acceptText);

            if (camera.hasTarget)
            {
                camera.hasTarget = false;
            }

            foreach (string m_event in tutorialData[current].eventsToTrigger)
            {
                Managers.Instance.Event.TriggerEvent(m_event);
            }
        }
        else
        {
            Managers.Instance.Event.TriggerEvent("endTutorial");
        }
    }

    public void EndTutorialEvent()
    {
        if (camera.hasTarget)
        {
            camera.hasTarget = false;
        }

        Managers.Instance.UI.DisplayTutorial(false);

        this.gameObject.SetActive(false);
    }

    public void MoveCamera(Transform transform)
    {
        camera.DoCameraMove(transform);
    }

    public void Update()
    {
        if(tutorialEventStarted)
        {
            if (Input.GetButtonDown(Buttons.A))
            {
                Managers.Instance.Event.TriggerEvent("accept");
            }
        }
    }
}

[System.Serializable]
public class TutorialData
{
    [Header("Text")]
    [TextArea(3,10)]
    public string tutorialText;
    public string acceptText;
    //public bool showGIF;
    //public AnimationClip animation;
    //[Space(5)]

    [Header("Events")]
    public List<string> eventsToTrigger;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfTheGame : MonoBehaviour {
    public List<EnemyBehaviourBase> enemies;
    bool stop;

    public void Update()
    {

        if (!stop)
        {
            for(int x = enemies.Count - 1; x >= 0; x--)
            {
                EnemyHealthCtrl healthCtrl = enemies[x].GetComponent<EnemyHealthCtrl>();
                if (healthCtrl.dead)
                {
                    enemies.RemoveAt(x);
                }
            }

            if (enemies.Count == 0)
            {
                stop = true;
                Managers.Instance.Level.EndGame();
            }
        }
    }



    public void RemoveEnemy(EnemyBehaviourBase obj)
    {
        enemies.Remove(obj);
    }
}

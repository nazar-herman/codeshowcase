﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPrompt : MonoBehaviour
{
    public TutorialData tutorialData;

    void FixedUpdate()
    {
        if(Managers.Instance != null)
        {
            Managers.Instance.UI.TutorialFadeOut();
        } 
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharacterControl>() != null)
        {
            Managers.Instance.UI.ResetTutorialText();
            Managers.Instance.UI.DoTutorialText(tutorialData.tutorialText);
            Managers.Instance.UI.TutorialFadeIn();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<CharacterControl>() != null)
        {
            Managers.Instance.UI.TutorialFadeIn();
        }
    }
}
